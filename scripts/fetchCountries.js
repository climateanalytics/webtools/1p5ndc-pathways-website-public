const fs = require("fs");
const path = require("path");
const fetch = require("node-fetch");

/**
This is a workaround to load countries into header without compromising ssg optimisation
 */

const fetchCountries = async () => {
  const request = await fetch(
    `http://dev.climateanalytics.org/rest/v1/npe15C/?token=${process.env.fetchCountriesApiToken}`
  );
  const countries = await request.json();
  fs.writeFileSync(
    path.resolve(__dirname, "../src/data/countries.json"),
    JSON.stringify(countries.data)
  );
  console.log("Wrote countries data to disk");
};

fetchCountries();
