const urls = {
  development: 'https://1p5ndc-pathways-v6.climateanalytics.org',
  local: 'https://1p5ndc-pathways.climateanalytics.org',
  production: 'https://1p5ndc-pathways.climateanalytics.org'
};

module.exports = {
  trailingSlash: true,
  pageExtensions: ['page.js'],
  basePath: process.env.CONTEXT === 'staging' ? '/staging' : '',
  env: {
    screenshotUrl: 'https://1p5ndc-pathways.climateanalytics.org/api/puppeteer/screengrab',
    dataApiUrl: 'https://1p5ndc-pathways.climateanalytics.org/data/v6',
    contentApiUrl: 'http://toolbox.climateanalytics.org/rest/c3/',
    contentApiToken: '***REMOVED: contentApiToken***',
    fetchCountriesApiToken: '***REMOVED: fetchCountriesApiToken***',
    mediaUrl: 'https://toolbox.climateanalytics.org/media/',
    url: urls[process.env.CONTEXT] || urls[process.env.NODE_ENV]
  }
};
