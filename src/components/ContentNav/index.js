import cn from './style.module.scss';
import cx from 'classnames';
import { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { useIsMobile } from 'src/utils/useMedia';
import buttonCn from 'src/style/button.module.scss';
import Link from 'next/link';
import { useIsMounted } from 'src/utils';

const Item = ({
  name,
  slug,
  children,
  active,
  onClick,
  isOpen,
  hashLink = true,
  basePath,
  separator
}) => {
  // Only show children if one of children is the current item
  const childIsActive = useMemo(
    () => children && children.find((child) => child.slug === active),
    [children, active]
  );
  const isActive = childIsActive || slug === active;

  return (
    <li className={cx(cn.item, isActive && cn.current, separator && cn.hasSeparator)}>
      {hashLink ? (
        <a
          className={cx(cn.itemLink)}
          href={`#${slug}`}
          tabIndex={isOpen ? 0 : -1}
          onClick={() => onClick(slug)}>
          {name}
        </a>
      ) : (
        <Link href={`${basePath}${slug}`}>
          <a className={cx(cn.itemLink)}>{name}</a>
        </Link>
      )}
      {children && isActive && (
        <ul className={cn.items}>
          {children.map((item) => (
            <Item key={item.slug} active={active} onClick={onClick} isOpen={isOpen} {...item} />
          ))}
        </ul>
      )}
    </li>
  );
};

export default function ContentNav({
  items,
  contentRef,
  label,
  openCta,
  closeCta,
  basePath,
  current
}) {
  if (!items) return null;
  const [active, setActive] = useState();
  const containerRef = useRef(null);
  const mounted = useIsMounted();
  const [preventUpdate, setPreventUpdate] = useState(false);
  const isMobile = useIsMobile();
  const [isOpen, setIsOpen] = useState(!isMobile);
  const handleScroll = useCallback(() => setPreventUpdate(true));

  const handleToggleClick = useCallback(() => setIsOpen((open) => !open), [setIsOpen]);

  useEffect(() => setActive(current), [current]);

  // close mobile drawer on tap anywhere outside container
  useEffect(() => {
    const closeIfTapOutside = (e) => {
      if (!containerRef.current.contains(e.target) && isOpen) setIsOpen(false);
    };
    document.addEventListener('click', closeIfTapOutside);
    return () => document.removeEventListener('click', closeIfTapOutside);
  }, [isOpen]);

  // Close drawer and set current to clicked. Also prevent updating the current item
  // for 2s so it doesn't automatically change the selected item
  const handleItemClick = useCallback(
    (slug) => {
      window.removeEventListener('scroll', handleScroll);
      setPreventUpdate(true);
      window.setTimeout(() => {
        if (mounted) window.addEventListener('scroll', handleScroll, { once: true });
      }, 2000);
      setIsOpen(false);
      setActive(slug);
    },
    [items]
  );

  useEffect(
    () => () => {
      window.removeEventListener('scroll', handleScroll);
    },
    [items]
  );

  useEffect(() => {
    if (isOpen) {
      document.body.style.setProperty('overflow', 'hidden');
    } else {
      document.body.style.removeProperty('overflow');
    }
  }, [isOpen]);

  useEffect(() => {
    if (!contentRef.current) return;

    const handleIntersection = (entries) => {
      if (preventUpdate) return;
      const intersectingEntries = entries.filter((entry) => entry.isIntersecting);
      const [intersectingEntry] = intersectingEntries;
      if (!intersectingEntry) return;
      setActive(intersectingEntry.target.getAttribute('id'));
    };

    const options = {
      root: null,
      rootMargin: '0px 0px -80% 0px',
      threshold: 0.5
    };

    const observer = new IntersectionObserver(handleIntersection, options);
    const flatItems = items.reduce((acc, item) => [...acc, item, ...(item.children || [])], []);
    flatItems.forEach((item) => {
      if (item.slug) {
        const [target] = contentRef.current.querySelectorAll(`#${item.slug}`);
        if (target) observer.observe(target);
      }
    });

    //const targets contentRef.query

    return () => observer.disconnect();
  }, [contentRef, items, preventUpdate]);

  return (
    <>
      <div ref={containerRef} className={cx(cn.wrapper, isOpen && cn.isOpen)}>
        <div className={cn.innerWrapper}>
          <nav aria-label={label}>
            <ul>
              {items.map((item) => (
                <Item
                  isOpen={isOpen}
                  key={item.slug}
                  {...item}
                  onClick={handleItemClick}
                  active={active}
                  basePath={basePath}
                />
              ))}
            </ul>
          </nav>
        </div>
      </div>
      {isMobile && (
        <button
          className={cx(cn.toggleButton, buttonCn.raised, buttonCn.cta, buttonCn.iconBefore)}
          onClick={handleToggleClick}>
          <span className={isOpen ? 'icon-close' : 'icon-menu'} />
          {isOpen ? closeCta : openCta}
        </button>
      )}
    </>
  );
}
