import Html from 'src/components/content/Html';
import pageCn from 'src/style/page.module.scss';
import cn from './style.module.scss';
import cx from 'classnames';

const References = ({ meta, siteContent }) => {
  return (
    <div className={cx(pageCn.content, cn.wrapper)}>
      <h2 id="references">{siteContent.references_label}</h2>
      <ul className={cx(pageCn.minor, cn.items)}>
        {meta.references.map((ref, i) => (
          <li className={cn.item} key={i} id={ref.id}>
            <b>{ref.name}</b>
            <Html className={cn.itemContent} html={ref.description} tag="div" />
          </li>
        ))}
      </ul>
    </div>
  );
};

export default References;
