import { memo, useEffect, useCallback, useState, useRef } from 'react';
import cn from './style.module.scss';
import cx from 'classnames';
import { debounce } from 'src/utils';

export function useTooltip() {
  const [tooltip, setTooltip] = useState(null);

  const openTooltip = useCallback(
    ({ el, data, top, left }) => {
      if (
        top === tooltip?.top &&
        left === tooltip?.left &&
        el === tooltip?.el &&
        data === tooltip?.data
      )
        return;

      setTooltip({
        el,
        data,
        top,
        left
      });
    },
    [setTooltip, tooltip]
  );

  const closeTooltip = useCallback(() => {
    setTooltip(null);
  }, [setTooltip]);

  // TODO: Check why triggers two updates
  return {
    tooltip,
    handleMouseLeave: closeTooltip,
    handleMouseEnter: openTooltip,
    handleMouseMove: openTooltip
  };
}

const Tooltip = ({
  el,
  id,
  tooltipFor,
  padding = 10,
  top,
  left,
  orientX = 'center',
  orientY = 'top',
  wrapperModifiers = [],
  wrapperClassName = cn.wrapper,
  offset = 7,
  children,
  autoControl
}) => {
  const wrapperRef = useRef(null);
  const [position, setPosition] = useState({ left: 0, top: 0, initialized: false });
  const [isOpen, setIsOpen] = useState(!autoControl);

  const updatePosition = useCallback(() => {
    if (!wrapperRef.current) return;
    let oX = orientX;
    let oY = orientY;
    let x = 0;
    let y = 0;
    const ttBox = wrapperRef.current.getBoundingClientRect();
    const maxLeft = window.innerWidth - ttBox.width - padding;
    const minRight = padding;
    const maxBottom = window.innerHeight - ttBox.height - padding;
    const minTop = padding;

    if (el || tooltipFor) {
      const element = tooltipFor ? document.getElementById(tooltipFor) : el;
      if (!element) console.warn(`No element found with id ${tooltipFor} to attach tooltip to`);
      const elBox = element.getBoundingClientRect();
      // Determine position completely based on passed element
      if (top === undefined && left === undefined) {
        if (oX === 'right') x = elBox.right + offset;
        if (oX === 'left') x = elBox.left - ttBox.width - offset;
        if (oX === 'center') x = elBox.left + elBox.width / 2 - ttBox.width / 2;

        if (oY === 'bottom') y = elBox.bottom + offset;
        if (oY === 'top') y = elBox.top - ttBox.height - offset;
        if (oY === 'center') y = elBox.top + elBox.height / 2 - ttBox.height / 2;

        // Assume left and top are positions within the passed element,
        // us them as base to which left and top is added
      } else {
        if (oX === 'right') x = elBox.left + left + offset;
        if (oX === 'left') x = elBox.left + left - ttBox.width - offset;
        if (oX === 'center') x = elBox.left + left - ttBox.width / 2;

        if (oY === 'bottom') y = elBox.top + top + offset;
        if (oY === 'top') y = elBox.top + top - ttBox.height - offset;
        if (oY === 'center') y = elBox.top + top - ttBox.height / 2;
      }
      // Assuming left/top are already fixed positions so we use them directly
    } else {
      if (left === undefined || top === undefined) return;
      if (oX === 'right') x = left + offset;
      if (oX === 'left') x = left - ttBox.width - offset;
      if (oX === 'center') x = left - ttBox.width / 2;

      if (oY === 'bottom') y = top + offset;
      if (oY === 'top') y = top - ttBox.height - offset;
      if (oY === 'center') y = top - ttBox.height / 2;
    }

    x = Math.max(Math.min(maxLeft, x), minRight);
    y = Math.max(Math.min(maxBottom, y), minTop);

    if (x === position.left && y === position.top) {
      setPosition({ left: x, top: y, initialized: true });
      return;
    }

    // If position is not set directly but derived from passed element, set
    // initialized to false on each update. For some reason, sometimes the position
    // still doesn't get set correctly...TODO: try in very simple example to get
    // position from element and don't render before position is set
    const initialized = top !== undefined && left !== undefined;

    setPosition({ left: x, top: y, initialized });
  }, [
    wrapperRef,
    el,
    padding,
    tooltipFor,
    top,
    left,
    children,
    offset,
    position.initialized,
    position.left,
    position.top,
    isOpen
  ]);

  useEffect(() => {
    updatePosition();
  }, [
    updatePosition,
    wrapperRef,
    el,
    padding,
    tooltipFor,
    top,
    left,
    children,
    offset,
    position.initialized,
    position.left,
    position.top,
    isOpen // not sure which props are actually needed...
  ]);

  const open = useCallback(() => setIsOpen(true), []);
  const close = useCallback(() => setIsOpen(false), []);

  useEffect(() => {
    // Only enable auto control if there is actually an element to attach events to
    if (!autoControl || !(el || tooltipFor)) return;
    const element = tooltipFor ? document.getElementById(tooltipFor) : el;
    if (!element) return;
    element.addEventListener('mouseenter', open);
    element.addEventListener('mouseleave', close);
    element.addEventListener('focus', open);
    element.addEventListener('blur', close);

    return () => {
      element.removeEventListener('mouseenter', open);
      element.removeEventListener('mouseleave', close);
      element.removeEventListener('focus', open);
      element.removeEventListener('blur', close);
    };
  }, [tooltipFor, autoControl, el, tooltipFor]);

  useEffect(() => {
    const debouncedUpdatePosition = debounce(updatePosition, 30);
    window.addEventListener('scroll', debouncedUpdatePosition);
    return () => {
      window.removeEventListener('scroll', debouncedUpdatePosition);
    };
  }, [updatePosition]);

  return (
    <div
      id={id}
      ref={wrapperRef}
      style={{ left: position.left, top: position.top, opacity: position.initialized ? 1 : 0 }}
      className={cx(
        cn.wrapperBase,
        wrapperClassName,
        !isOpen && 'screenreader-only',
        !isOpen && cn.hidden,
        wrapperModifiers.map((m) => cn[m] || m)
      )}>
      {children}
    </div>
  );
};

export default memo(Tooltip);
