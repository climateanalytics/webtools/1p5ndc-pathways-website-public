import { memo, useEffect, useCallback, useState, useRef } from 'react';
import cn from './new.style.module.scss';
import cx from 'classnames';
import { debounce, useDeviceInfo } from 'src/utils';

/*
 * Handles closing and opening of tooltip
 */
export function useTooltip({ easyTrigger } = { easyTrigger: true }) {
  const [triggerEl, setTriggerEl] = useState(null);

  const [focused, setFocused] = useState(false);
  const [cancelled, setCancelled] = useState(false);
  const [moused, setMoused] = useState(false);
  const [visible, setVisible] = useState(false);
  // differentiates focus on click vs focus on tab
  const [clicked, setClicked] = useState(false);

  const triggerRef = useCallback((node) => {
    setTriggerEl(node);
  }, []);

  const handleClick = useCallback(() => {
    if (easyTrigger) {
      setClicked(true);
      return;
    }
    setVisible((visible) => !visible);
    setFocused((focused) => !focused);
  }, [easyTrigger]);

  const handleFocus = useCallback(
    (e) => {
      setFocused(true);
      if (easyTrigger) setVisible(true);
      e.stopPropagation();
    },
    [easyTrigger]
  );

  const handleBlur = useCallback(
    (e) => {
      setFocused(false);
      setCancelled(false);
      if (!moused) setVisible(false);
      e.stopPropagation();
    },
    [moused]
  );

  const handleMouseEnter = useCallback(
    (e) => {
      if (!easyTrigger) return;
      setMoused(true);
      setVisible(true);
      e.stopPropagation();
    },
    [easyTrigger]
  );

  const handleMouseLeave = useCallback(
    (e) => {
      if (!easyTrigger) {
        return;
      }

      setMoused(false);
      if (!focused || clicked) {
        setVisible(false);
        setClicked(false);
        setFocused(false);
      }
      e.stopPropagation();
    },
    [easyTrigger, focused, cancelled, clicked]
  );

  const handleKeyDown = useCallback(
    (e) => {
      if (e.keyCode === 27) {
        setCancelled(true);
        setVisible(false);
        e.stopPropagation();
      }
    },
    [easyTrigger]
  );

  return {
    visible,
    triggerEl,
    triggerProps: {
      ref: triggerRef,
      onClick: handleClick,
      onMouseLeave: handleMouseLeave,
      onMouseEnter: handleMouseEnter,
      onKeyDown: handleKeyDown,
      onFocus: handleFocus,
      onBlur: handleBlur
    }
  };
}

/*
 * Handles positioning of tooltip
 */
const Tooltip = ({
  triggerElement,
  id,
  tooltipFor,
  top = 0,
  left = 0,
  delay = 0,
  offset = 7,
  padding = 10,
  orientation = 'n',
  alignment = 'center',
  children,
  autoWidth = false,
  visible
}) => {
  const ttRef = useRef(null);
  const [position, setPosition] = useState({ x: 0, y: 0, width: null, initialized: false });
  const revealTooltip = visible && position.initialized;
  const { isXS } = useDeviceInfo();

  const updatePosition = useCallback(() => {
    if (!ttRef.current || !visible) return;

    let x = left;
    let y = top;
    const target = ttRef.current.getBoundingClientRect();
    let trggrBbox = {};
    let orient = orientation;

    if (tooltipFor || triggerElement) {
      const trggrEl = tooltipFor ? document.getElementById(tooltipFor) : triggerElement;
      if (!trggrEl) console.warn(`No element found with id ${tooltipFor} to attach tooltip to`);
      trggrBbox = trggrEl.getBoundingClientRect();
      trggrBbox.cx = trggrBbox.x + trggrBbox.width / 2;
      trggrBbox.cy = trggrBbox.y + trggrBbox.height / 2;
    }

    // left and top can be set as parameter or derived
    // automatically from the trigger element
    let origin = {
      left: left || trggrBbox.x,
      top: top || trggrBbox.y,
      cx: trggrBbox.cx || 0,
      cy: trggrBbox.cy || 0,
      width: trggrBbox.width || 0,
      height: trggrBbox.height || 0
    };

    if (autoWidth) target.width = origin.width;

    x = origin.left;
    y = origin.top;

    const dX = origin.width / 2 + offset + target.width + padding;
    const dY = origin.height / 2 + offset + target.height + padding;

    if (isXS) orient = 'n';

    switch (alignment) {
      case 'center':
        x = origin.cx - target.width / 2;
        y = origin.cy - target.height / 2;
        break;
      case 'start':
        x = origin.left;
        y = origin.top;
        break;
      case 'end':
        x = origin.cx + origin.width / 2 - target.width;
        y = origin.cy + origin.height / 2 - target.height;
        break;
    }

    switch (orient) {
      case 'n':
        orient = origin.cy - dY < 0 ? 's' : 'n';
        break;
      case 's':
        orient = origin.cy + dY > window.innerHeight ? 'n' : 's';
        break;
      case 'e':
        orient = origin.cx - dX < 0 ? 'w' : 'e';
        break;
      case 'w':
        orient = origin.cx + dX > window.innerWidth ? 'e' : 'w';
        break;
    }

    switch (orient) {
      case 'n':
        y = origin.top - offset - target.height;
        break;
      case 's':
        y = origin.top + origin.height + offset;
        break;
      case 'e':
        x = origin.left - offset - target.width;
        break;
      case 'w':
        x = origin.left + origin.width + offset;
        break;
    }

    if (orient === 'n' || orient === 's')
      x = Math.max(padding, Math.min(window.innerWidth - target.width - padding, x));
    if (orient === 'e' || orient === 'w')
      y = Math.max(padding, Math.min(window.innerHeight - target.height - padding, y));

    setPosition({ x, y, width: autoWidth && target.width, initialized: true });
  }, [tooltipFor, visible, orientation, alignment, ttRef.current, triggerElement]);

  useEffect(() => {
    const debouncedUpdatePosition = debounce(updatePosition, 100);
    window.addEventListener('resize', debouncedUpdatePosition);
    window.addEventListener('orientationchange', debouncedUpdatePosition);
    window.addEventListener('scroll', debouncedUpdatePosition);

    return () => {
      window.removeEventListener('resize', debouncedUpdatePosition);
      window.removeEventListener('orientationchange', debouncedUpdatePosition);
      window.removeEventListener('scroll', debouncedUpdatePosition);
    };
  }, [updatePosition, visible]);

  useEffect(() => visible && updatePosition(), [visible]);

  return (
    <div
      id={id}
      ref={ttRef}
      style={{
        left: position.x,
        top: position.y,
        width: position.width,
        maxWidth: autoWidth && 'none',
        '--delay': `${delay}s`
      }}
      className={cx(cn.wrapper)}
      aria-hidden={!revealTooltip}>
      {children}
    </div>
  );
};

export default memo(Tooltip);
