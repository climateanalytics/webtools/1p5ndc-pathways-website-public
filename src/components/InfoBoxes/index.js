import { useEffect, useState } from 'react';
import { createPortal } from 'react-dom';
import { StyledHtml } from 'src/components/content/Html';
import Modal from './Modal';
import cn from './style.module.scss';
import cx from 'classnames';
import btnCn from 'src/style/button.module.scss';
import Pathways from './Pathways';
import { useRouter } from 'next/router';

const infoSupplements = {
  pathways: Pathways
};

const TriggerButton = ({ el, children }) => {
  useEffect(() => {});
  return createPortal(children, el);
};

const InfoBoxes = ({ contentRef, meta, siteContent }) => {
  const [infoBoxes, setInfoBoxes] = useState([]);
  const [currentBox, setCurrentBox] = useState(null);
  const router = useRouter(); // Make sure updates happen on each new page load

  useEffect(() => {
    if (!contentRef.current || !siteContent.info) return;

    const anchors = contentRef.current.querySelectorAll('[class^=info-]');
    const items = Array.from(anchors).map((el) => {
      const id = Array.from(el.classList)
        .find((c) => /^info-/.test(c))
        .replace(/^info-/, '');

      const content = siteContent.info[id];
      // Remove html tags, TODO: should be changed in the cms though
      const title = siteContent.info[`${id}_title`].replace(/<[^>]*>/g, '');
      const InfoSupplement = infoSupplements[id];
      if (!content || !title) return;
      return {
        id,
        title,
        content,
        InfoSupplement,
        el: el.parentElement
      };
    });
    setInfoBoxes(items);
  }, [contentRef, setInfoBoxes, siteContent, router.asPath]);

  return (
    <>
      {infoBoxes.map((infoBox, i) => {
        return (
          <div key={i}>
            <TriggerButton el={infoBox.el}>
              <button
                onClick={(e) => {
                  setCurrentBox({ ...infoBox, trigger: document.activeElement });
                  e.stopPropagation();
                }}
                className={cx(btnCn.s, btnCn.bare, btnCn.iconBefore, cn.infoButton)}
                id={`info-trigger-${infoBox.id}`}>
                <span className={cx('icon-info-circled')} />
                <span className={cn.infoButtonInner}>{infoBox.title}</span>
              </button>
            </TriggerButton>
          </div>
        );
      })}
      <Modal
        triggerNode={currentBox?.trigger}
        labelledBy={`info-trigger-${currentBox?.id}`}
        closeLabel={siteContent.close_info_label}
        onClose={() => setCurrentBox(null)}>
        {currentBox && (
          <>
            <h2 className={cn.infoboxTitle}>{currentBox.title}</h2>
            <StyledHtml html={currentBox.content} modifiers={['profile']} />
            {currentBox.InfoSupplement && <currentBox.InfoSupplement meta={meta} />}
          </>
        )}
      </Modal>
    </>
  );
};

export default InfoBoxes;
