import Html from 'src/components/content/Html';
import pageCn from 'src/style/page.module.scss';
import cn from './style.module.scss';
import { useSiteContent } from 'src/data/siteContent';

const Pathways = ({ meta }) => {
  const siteContent = useSiteContent();

  return (
    <div className={pageCn.content}>
      <h5>{siteContent.all_pathways_label}</h5>
      {meta.pathways.map((pathway) => {
        return (
          <div key={pathway.id} className={cn.metaItem}>
            <h4 className={cn.metaItemName}>{pathway.name}</h4>
            <div className={cn.metaItemSecondaryName}>
              {siteContent.iam_name_label}: {pathway.iam_name}
            </div>
            <Html html={pathway.description} />
          </div>
        );
      })}
    </div>
  );
};

export default Pathways;
