import { useEffect, useCallback, useRef } from 'react';
import cn from './style.module.scss';
import cx from 'classnames';
import buttonCn from 'src/style/button.module.scss';

const Modal = ({ children, triggerNode, onClose, labelledBy, closeLabel, wrapperClassName }) => {
  const containerRef = useRef(null);
  const innerRef = useRef(null);
  const closerRef = useRef(null);

  const handleFocus = useCallback(
    (e) => {
      // Only allow to switch focus to elements within the modal or the closer button,
      // otherwise move focus back to inner
      if (
        !containerRef.current ||
        containerRef.current.contains(e.target) ||
        e.target === closerRef.current
      ) {
        return;
      }
      containerRef.current.focus();
    },
    [containerRef, closerRef]
  );

  const handleKeyDown = useCallback((e) => {
    if (e.key === 'Escape') onClose && onClose();
  }, []);

  useEffect(() => {
    // on open, focus the container
    if (children) {
      containerRef.current.focus();
      document.addEventListener('focus', handleFocus, true);
      document.addEventListener('keydown', handleKeyDown);
      document.body.style.setProperty('overflow', 'hidden');
    } else {
      // On close remove handlers
      document.removeEventListener('focus', handleFocus, true);
      document.removeEventListener('keydown', handleKeyDown);
      document.body.style.removeProperty('overflow');
    }

    return () => {
      // On close, re-focus the button that opened the modal
      triggerNode?.focus();
    };
  }, [containerRef, triggerNode, children]);

  useEffect(() => {
    if (!innerRef.current) return;
    innerRef.current.scrollTop = 0;
  }, [children]);

  useEffect(() => {
    const closeIfInside = (e) => {
      !containerRef.current?.contains(e.target) && onClose();
    };
    if (children) document.addEventListener('click', closeIfInside);
    else document.removeEventListener('click', closeIfInside);
    return () => document.removeEventListener('click', closeIfInside);
  }, [children]);

  return (
    <div aria-hidden={!children} className={cn.wrapper}>
      <div ref={innerRef} className={cn.inner}>
        {children && (
          <div
            ref={containerRef}
            aria-labelledby={labelledBy}
            className={wrapperClassName}
            aria-modal="true"
            tabIndex={-1}>
            {children}
          </div>
        )}
      </div>
      <div className={cn.modalCloserWrapper}>
        <button
          ref={closerRef}
          className={cx(buttonCn.cta, buttonCn.pill, buttonCn.iconBefore, cn.modalCloser)}
          onClick={onClose}>
          <span className="icon-west" />
          <span className={buttonCn.buttonText}>{closeLabel}</span>
        </button>
      </div>
    </div>
  );
};

export default Modal;
