import React from 'react';
import cx from 'classnames';

import cn from './style.module.scss';
import ChartText from 'src/components/charts/ChartText';

function hypotenuse(a, b) {
  return Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
}

const alignThreshold = 10;

export const SwoopyTextSvg = ({
  from = null,
  to = null,
  text = null,
  /* textAnchor = 'start',
  verticalAnchor = 'start', */
  width = 100,
  textOffset = [0, 0],
  className,
  id
}) => {
  const dx = to[0] - from[0];
  const dy = to[1] - from[1];
  const alignThreshold = 10;

  const orientX = Math.abs(dx) < alignThreshold ? 0 : (dx / Math.abs(dx)) * -1;
  const orientY = Math.abs(dy) < alignThreshold ? 0 : (dy / Math.abs(dy)) * -1;

  const textTransform = [from[0] + textOffset[0] * orientX, from[1] + textOffset[1] * orientY];
  //const textTransform = from.map((d, i) => d + textOffset[i]);

  let textAnchor = dx < 0 ? 'start' : 'end';
  let verticalAnchor = dy < 0 ? 'start' : 'end';
  textAnchor = Math.abs(dx) < alignThreshold ? 'middle' : textAnchor;
  verticalAnchor = Math.abs(dy) < alignThreshold ? 'middle' : verticalAnchor;

  return (
    <g data-id={id} transform={`translate(${textTransform.join(' ')})`}>
      <ChartText
        width={width}
        verticalAnchor={verticalAnchor}
        textAnchor={textAnchor}
        className={cx(className, cn.swoopyText)}>
        {text}
      </ChartText>
    </g>
  );
};

export const SwoopyText = ({
  from = null,
  to = null,
  text = null,
  value = null,
  width,
  id,
  className,
  textClassName
}) => {
  const dx = to[0] - (from[0] + width / 2);
  const adx = Math.abs(dx);

  const left = from[0];
  const top = from[1];
  let textAlign = dx < width / 2 ? 'left' : 'right';
  textAlign = adx - width / 2 < alignThreshold ? 'center' : textAlign;

  return (
    <div
      className={cx(className, cn.swoopyTextWrapper)}
      data-id={id}
      style={{ transform: `translate(${left}px, ${top}px)`, textAlign }}>
      <div className={cx(textClassName, cn.swoopyText)}>{text}</div>
      {value && <div className={cx(cn.swoopyText, cn.secondary)}>{value}</div>}
    </div>
  );
};

export const SwoopyArrow = ({
  from = null,
  fromWidth,
  fromHeight,
  to = null,
  angle = Math.PI / 6,
  clockwise = true,
  offset = [0, 0]
}) => {
  if (!from || !to) {
    return null;
  }

  // Distance from source center to target
  const dx = to[0] - (from[0] + fromWidth / 2);
  const dy = to[1] - (from[1] + fromHeight / 2);

  // Absolute distance from source center to target
  const adx = Math.abs(dx);
  const ady = Math.abs(dy);

  let fromX = from[0];
  let fromY = from[1];
  let offsetX;
  let offsetY;
  const fromLeft = dx < fromWidth / 2;
  const fromTop = dy < fromHeight / 2;
  let shiftX = adx - fromWidth / 2 > 0;
  let shiftY = ady - fromHeight / 2 > 0;
  // Only draw line from center x if box is at top or at bottom
  const fromCenterX = adx - fromWidth / 2 < alignThreshold && shiftY;
  const fromCenterY = ady - fromHeight / 2 < alignThreshold && shiftX;

  // "from" is always assumed to be top left of source. Here we find out
  // whether we need to draw the line to a different edge/corner of the source

  // If distance (x) from source to target is smaller than width,
  // keep target as top left. Else draw line to top right.
  fromX = fromLeft ? fromX : fromX + fromWidth;

  // If absolute distance from center is smaller than threshold, draw line to center
  fromX = fromCenterX ? from[0] + fromWidth / 2 : fromX;

  // same for y dimension
  fromY = fromTop ? fromY : fromY + fromHeight;
  fromY = fromCenterY ? from[1] + fromHeight / 2 : fromY;

  // Find the exact point where we draw the line
  offsetX = fromLeft ? -1 : 1;
  offsetX = fromCenterX ? 0 : offsetX;
  offsetX *= offset[0];

  offsetY = fromLeft ? -1 : 1;
  offsetY = fromCenterY ? 0 : offsetY;
  offsetY *= offset[1];

  fromX += offsetX;
  fromY += offsetY;

  const angleParsed = Math.min(Math.max(angle, 1e-6), Math.PI - 1e-6);

  // get the chord length ("height" {h}) between points
  var hypo = hypotenuse(to[0] - fromX, to[1] - fromY);

  // get the distance at which chord of height h subtends {angle} radians
  var d = hypo / (2 * Math.tan(angleParsed / 2));

  // get the radius {r} of the circumscribed circle
  var r = hypotenuse(d, hypo / 2);

  /*
    SECOND, compose the corresponding SVG arc.
      read up: http://www.w3.org/TR/SVG/paths.html#PathDataEllipticalArcCommands
      example: <path d = "M 200,50 a 50,50 0 0,1 100,0"/>
                          M 200,50                          Moves pen to (200,50);
                                   a                        draws elliptical arc;
                                     50,50                  following a degenerate ellipse, r1 == r2 == 50;
                                                            i.e. a circle of radius 50;
                                           0                with no x-axis-rotation (irrelevant for circles);
                                             0,1            with large-axis-flag=0 and sweep-flag=1 (clockwise);
                                                 100,0      to a point +100 in x and +0 in y, i.e. (300,50).
    */
  const path =
    'M ' +
    fromX +
    ',' +
    fromY +
    ' a ' +
    r +
    ',' +
    r +
    ' 0 0,' +
    (clockwise ? '1' : '0') +
    ' ' +
    (to[0] - fromX) +
    ',' +
    (to[1] - fromY);

  return (
    <g className={cn.swoopy}>
      <path d={path} className={cx(cn.swoopyPath, cn.swoopyOutline)} />
      <path d={path} className={cn.swoopyPath} />
    </g>
  );
};
