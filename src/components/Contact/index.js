import Html from 'src/components/content/Html';
import { useState } from 'react';
import pageCn from 'src/style/page.module.scss';
import buttonCn from 'src/style/button.module.scss';
import inputCn from 'src/style/input.module.scss';
//import cn from './style.module.scss';
import cx from 'classnames';

const statusTypes = {
  success: 'success',
  error: 'error'
};

export default function Contact({ siteContent }) {
  const [status, setStatus] = useState(null);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const data = new FormData(e.target);
    try {
      const response = await fetch(
        `https://climateanalytics.org/?token=${process.env.contentApiToken}&rex-api-call=tools`,
        {
          method: 'POST',
          redirect: 'follow',
          body: data
        }
      );
      const responseData = await response.json();
      if (responseData.errorcode === 0) setStatus(statusTypes.success);
    } catch (e) {
      console.error(e);
      setStatus(statusTypes.error);
    }
  };

  const handleReset = () => setStatus(null);

  return (
    <div>
      <div className={pageCn.content}>
        <h2 id="contact">{siteContent.contact_label}</h2>
      </div>
      <div className={'margin-bottom-l'}>
        <h4 className="margin-bottom-xs" id="newsletter">
          {siteContent.newsletter_label}
        </h4>
        <p>
          <a
            className={cx(buttonCn.cta, 'margin-right-xxs')}
            href="https://us12.list-manage.com/subscribe?u=9e4cf5c69a12b67305cea7da4&id=85e903aa7a">
            Sign up
          </a>
          {siteContent.signup_text}
        </p>
      </div>
      <div className={'margin-bottom-l'}>
        <h4 className="margin-bottom-xs" id="media-enquiries">
          {siteContent.media_enquiries_label}
        </h4>
        <p>
          {siteContent.media_enquiries_text}
          <br />
          <a href="mailto:press@climateanalytics.org">
            <b>press@climateanalytics.org</b>
          </a>
          <br />
          <a
            href="tel:+49 (0)
        17656734870">
            <b>+49 (0) 17656734870</b>
          </a>
        </p>
      </div>

      <h4 className="margin-bottom-xs" id="get-in-touch">
        {siteContent.get_in_touch_label}
      </h4>
      {!status && (
        <form action="" method="post" onSubmit={handleSubmit}>
          <div className={cx(inputCn.inputWrapper, inputCn.oneLine)}>
            <label htmlFor="name" className={inputCn.label}>
              {siteContent.name_label}
            </label>
            <input
              className={inputCn.textInput}
              type="text"
              name="fromname"
              id="fromname"
              required
            />
          </div>
          <div className={cx(inputCn.inputWrapper, inputCn.oneLine)}>
            <label htmlFor="email" className={inputCn.label}>
              {siteContent.email_label}
            </label>
            <input className={inputCn.textInput} type="email" name="from" id="from" required />
          </div>
          <div className={cx(inputCn.inputWrapper)}>
            <label htmlFor="body" className={inputCn.label}>
              {siteContent.message_label}
            </label>
            <textarea className={inputCn.textInput} rows={6} name="body" id="body" required />
          </div>
          <div className={inputCn.submitWrapper}>
            <input type="submit" value={siteContent.send_label} className={buttonCn.cta} />
          </div>
        </form>
      )}
      {status === statusTypes.success && (
        <div className={inputCn.submitSuccess}>{siteContent.message_success_text}</div>
      )}
      {status === statusTypes.error && (
        <div className={inputCn.submitFailure}>
          {siteContent.message_failure_text}{' '}
          <button className={buttonCn.bare} onClick={handleReset}>
            {siteContent.message_retry_cta}
          </button>
        </div>
      )}
    </div>
  );
}
