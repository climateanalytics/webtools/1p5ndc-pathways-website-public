import cx from 'classnames';
import cn from './style.module.scss';
import gridCn from 'src/style/grid.module.scss';
import useFootnotes from './useFootnotes';

const Footnotes = ({ contentRef, siteContent }) => {
  const [footnotesRef, hasFootnotes] = useFootnotes(contentRef);

  return (
    <section className={cx(cn.wrapper, !hasFootnotes && cn.isEmpty)}>
      <div className={gridCn.grid}>
        <div className={cx(gridCn.col12)}>
          {hasFootnotes && <h3 className={cn.footnotesTitle}>{siteContent.footnotes_label}</h3>}
          <div ref={footnotesRef} className={cn.footnotes}></div>
        </div>
      </div>
    </section>
  );
};

export default Footnotes;
