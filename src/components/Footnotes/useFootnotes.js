import { useEffect, useState, useCallback } from 'react';
import { useRouter } from 'next/router';

const useFootnotes = (containerRef) => {
  const [node, setNode] = useState(null);
  const router = useRouter(); // Make sure updates happen on each new page load

  // needs to be true at the start to make sure footnotes container will be rendered at the beginning
  const [hasFootnotes, setHasFootnotes] = useState(true);
  useEffect(() => {
    if (!node) return;
    // Clear footnote container
    Array.from(node.children).forEach((child) => child.parentNode.removeChild(child));

    // Get all footnotes
    const notes = containerRef.current.querySelectorAll('p.footnote');

    // Append footnotes to container
    notes.forEach((note) => {
      note.parentNode.removeChild(note);
      node.appendChild(note);
    });
  }, [node, containerRef, router.asPath]);

  const ref = useCallback((node) => {
    setNode(node);
  }, []);

  useEffect(() => {
    const notes = containerRef.current.querySelectorAll('p.footnote');
    setHasFootnotes(notes.length);
  }, [containerRef, router.asPath, setHasFootnotes]);

  return [ref, !!hasFootnotes];
};

export default useFootnotes;
