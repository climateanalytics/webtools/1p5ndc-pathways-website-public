import { useMemo } from 'react';
import { filters, render as sqrlRender } from 'squirrelly';
import { genitivify } from 'src/utils';
import Html from './Html';

filters.define('s', genitivify);

export const renderTemplate = (template, props = {}) => {
  try {
    const str = sqrlRender(template, props, { useWith: true, varName: 'd' });
    return str;
    // handle html entities properly. problem, document is not available on server which
    // will cause missmatch of strings in browser/server. useEffect would solve the problem
    // but might break footnotes parsing (in case needed for templates...)
    // if (typeof window === 'undefined') return str;
    // var txt = document.createElement('textarea');
    // txt.innerHTML = str;
    // return txt.value;
  } catch (e) {
    if (process.env.NODE_ENV === 'development') {
      throw e;
    } else {
      console.error(e);
      return '';
    }
  }
};

const Template = ({ html, text, data = {}, ...props }) => {
  const rendered = useMemo(() => renderTemplate(text || html, data), [html, text, data]);
  return <Html html={rendered} {...props} />;
};

export default Template;
