import { memo, useEffect, useRef } from 'react';
import cn from 'src/style/page.module.scss';
import { slugify } from 'src/utils';
import cx from 'classnames';

export default function Html({ html, className = '', tag = 'div', applyStyle, ...rest }) {
  if (!html) return null;
  const Tag = tag;
  const contentRef = useRef(null);
  useEffect(() => {
    if (!contentRef.current) return;
    Array.from(contentRef.current.querySelectorAll('h1, h2, h3, h4, h5, h6')).forEach((el) => {
      const slug = slugify(el.innerText);
      const id = el.getAttribute('id');
      if (!id) el.setAttribute('id', slug);
    });
  }, [html, contentRef]);

  return (
    <Tag
      {...rest}
      ref={contentRef}
      className={cx(className, applyStyle && cn.content)}
      dangerouslySetInnerHTML={{
        __html: html
      }}
    />
  );
}

function StyledHtmlComponent({ className = '', modifiers = [], ...rest }) {
  return (
    <Html {...rest} className={cx(className, cn.content, ...modifiers.map((m) => cn[m] || m))} />
  );
}

export const StyledHtml = memo(StyledHtmlComponent);
