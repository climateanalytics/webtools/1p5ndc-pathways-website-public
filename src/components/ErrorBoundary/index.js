import { Component } from 'react';
import cn from './style.module.scss';
import cx from 'classnames';
import gridCn from 'src/style/grid.module.scss';
import typoCn from 'src/style/typography.module.scss';
import { useSiteContent } from 'src/data/siteContent';

const DefaultErrorMessage = ({ error }) => {
  const siteContent = useSiteContent();
  return (
    <div className={cx(gridCn.col12, cn.wrapper)}>
      <div>
        <span className={cx(typoCn.metaBase, typoCn.top)}>{siteContent.default_error_name}</span>
      </div>
      <h3 className={cn.title}>{siteContent.default_error_title}</h3>
      <p className={cn.description}>{siteContent.default_error_text}</p>
    </div>
  );
};

class ErrorBoundary extends Component {
  constructor(props) {
    super(props);
    this.state = { hasError: false };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    return { hasError: true, error };
  }

  componentDidCatch(error, errorInfo) {
    // You can also log the error to an error reporting service
    //logErrorToMyService(error, errorInfo);
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <this.props.ErrorMessage error={this.state.error} />;
    }

    return this.props.children;
  }
}

export default ErrorBoundary;

export const withErrorBoundary = (Component, ErrorMessage = DefaultErrorMessage) => {
  const WrappedComponent = (props) => {
    return (
      <ErrorBoundary ErrorMessage={ErrorMessage}>
        <Component {...props} />
      </ErrorBoundary>
    );
  };

  return WrappedComponent;
};
