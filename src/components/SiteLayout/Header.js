import ActiveLink from 'src/components/utils/ActiveLink';
import cx from 'classnames';
import cn from './style.module.scss';
import gridCn from 'src/style/grid.module.scss';
import buttonCn from 'src/style/button.module.scss';
import { useState, useRef, useCallback, useEffect } from 'react';
import SimpleSelect from 'src/components/controls/SimpleSelect';
import useMedia from 'src/utils/useMedia';
import { useSiteContent } from 'src/data/siteContent';
import { useRouter } from 'next/router';

const Header = ({ countries, country }) => {
  const wrapperRef = useRef(null);
  const navRef = useRef(null);
  const siteContent = useSiteContent();
  const router = useRouter();

  const isMobile = useMedia({ values: [false, false, false], defaultValue: true });
  const [isOpen, setIsOpen] = useState(false);

  const handleToggle = useCallback(() => {
    setIsOpen((isOpen) => !isOpen);
  }, [setIsOpen]);

  useEffect(() => setIsOpen(false), [router.asPath]);

  const renderNav = isOpen || !isMobile;

  //console.log(isOpen, isMobile, renderNav);

  return (
    <header className={cx(cn.header, gridCn.wrapper)}>
      <a className={cx(cn.skipLink, buttonCn.cta)} href="#main">
        {siteContent.skip_to_content_label}
      </a>
      <div ref={wrapperRef} className={cn.headerInner}>
        <div className={cn.headerStart}>
          {isMobile && (
            <div className={cx(cn.toggleWrapper)}>
              <button
                onClick={handleToggle}
                className={cx(cn.toggleButton, isOpen ? 'icon-close' : 'icon-menu')}></button>
            </div>
          )}

          <div>
            <ActiveLink href="/">
              <a>
                <span className={cn.toolName}>
                  <span className={cn.toolNameLine}>1.5°C national</span>
                  <span className={cn.toolNameLine}>pathway explorer</span>
                </span>
              </a>
            </ActiveLink>
          </div>
        </div>
        <nav ref={navRef} className={cn.nav} aria-hidden={!renderNav}>
          <ul role="menubar" className={cn.navigationItems}>
            <li role="none" className={cn.navigationItem}>
              <SimpleSelect
                id="header-nav-countries"
                items={countries}
                value={country?.slug}
                buttonLabel={siteContent.countries_label}
                trayPositionRef={isMobile ? navRef : wrapperRef}
                trayModifiers={['full', cn.countriesTray]}
                trayGap={0}
              />
            </li>
          </ul>
          <ul role="menubar" className={cn.navigationItems}>
            <li role="none" className={cn.navigationItem}>
              <ActiveLink activeClassName={cn.isActive} href="/activities">
                <a
                  className={cx(cn.navigationLink, buttonCn.secondary, buttonCn.bare)}
                  role="menuitem">
                  {siteContent.activities_label}
                </a>
              </ActiveLink>
            </li>
            <li role="none" className={cn.navigationItem}>
              <ActiveLink activeClassName={cn.isActive} href="/about">
                <a
                  className={cx(cn.navigationLink, buttonCn.secondary, buttonCn.bare)}
                  role="menuitem">
                  {siteContent.about_label}
                </a>
              </ActiveLink>
            </li>
            <li role="none" className={cn.navigationItem}>
              <ActiveLink activeClassName={cn.isActive} href="/methodology">
                <a
                  className={cx(cn.navigationLink, buttonCn.secondary, buttonCn.bare)}
                  role="menuitem">
                  {siteContent.methodology_label}
                </a>
              </ActiveLink>
            </li>
          </ul>
        </nav>
        <div className={cn.headerEnd}>
          <a href="https://climateanalytics.org" className={cn.caLink}>
            <img className={cn.caLogo} src="/static/logo-ca.svg" alt="Climate Analytics logo" />
          </a>
        </div>
      </div>
    </header>
  );
};
export default Header;
