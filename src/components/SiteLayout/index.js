import { useRouter } from 'next/router';
import HtmlHead from 'src/components/HtmlHead';
import Header from './Header';
import Footer from './Footer';
import EmbedFooter from './EmbedFooter';
import { isIframe } from 'src/utils';
import { useEffect, useState, Children, cloneElement } from 'react';
import { renderTemplate } from 'src/components/content/Template';
import ErrorBoundary from '../ErrorBoundary';
import CookieBanner from 'src/components/utils/CookieConsent/Banner';

export default function Layout({ children, siteContent, meta, country }) {
  const router = useRouter();
  const [isEmbed, setIsEmbed] = useState(false);
  const isStatic = !!router.query.static;
  const isBare = !!router.query.bare;
  useEffect(() => setIsEmbed(isIframe()));

  const childrenWithProps = Children.map(children, (child) =>
    cloneElement(child, { isEmbed, isStatic, isBare })
  );
  const title = renderTemplate(siteContent.page_title, { country });

  // Handling of scrolling to targets for ids that are generated after first render
  useEffect(() => {
    const hasHashId = router.asPath.includes('#');
    if (!hasHashId) return;
    const hashId = router.asPath.split('#').pop();

    const target = document.getElementById(hashId);
    target?.scrollIntoView();
  }, [router.asPath]);

  let footer =
    isEmbed || isStatic ? (
      <EmbedFooter siteContent={siteContent} country={country} isStatic={router.query.static} />
    ) : (
      <Footer siteContent={siteContent} />
    );

  return (
    <>
      <HtmlHead country={country} title={title} description={siteContent.description} />
      {!isEmbed && !isStatic && !isBare && (
        <Header countries={meta.countryGroups} country={country} />
      )}
      <ErrorBoundary>
        {childrenWithProps}
        {!isStatic && <CookieBanner />}
      </ErrorBoundary>
      {!isBare && footer}
    </>
  );
}
