import gridCn from 'src/style/grid.module.scss';
import Link from 'next/link';
import cx from 'classnames';
//import btnCn from 'src/style/buttons.module.scss';
import cn from './style.module.scss';

const EmbedFooter = ({ siteContent, country, isStatic }) => {
  const toolPath = country?.href || '/';
  const url = `${process.env.url}${toolPath}`;

  return (
    <footer className={cn.embedFooterWrapper}>
      <div className={gridCn.grid}>
        <div className={cx(gridCn.col12, cn.embedFooter)}>
          <div>
            {isStatic
              ? siteContent.embed_footer_link_text_static
              : siteContent.embed_footer_link_text}{' '}
            <Link href={toolPath}>
              <a className={cn.embedFooterLink}>{isStatic ? url : siteContent.tool_name}</a>
            </Link>
          </div>

          <Link target="_blank" rel="noreferrer" href="https://climateanalytics.org">
            <a>
              <img
                className={cn.embedCaLogo}
                src="/static/logo-ca.svg"
                alt="Climate Analytics logo"
              />
            </a>
          </Link>
        </div>
      </div>
    </footer>
  );
};

export default EmbedFooter;
