import gridCn from 'src/style/grid.module.scss';
import Link from 'next/link';
import cx from 'classnames';
import { useEffect, useState } from 'react';
import buttonCn from 'src/style/button.module.scss';
import cn from './style.module.scss';

const Footer = ({ siteContent }) => {
  const [renderContact, setRenderContact] = useState(false);
  useEffect(() => setRenderContact(true), []);

  return (
    <footer className={cx(gridCn.wrapper, cn.footerWrapper)}>
      <div className={gridCn.container}>
        <div className={cx(gridCn.grid, cn.footer, cn.toolFooter)}>
          <div className={cx(gridCn.col4, gridCn.mCol6, gridCn.sCol10, cn.footerSection)}>
            <Link href="/">
              <a className={cn.footerToolTitle}>{siteContent.tool_name}</a>
            </Link>

            <p className={cx(cn.footerText, 'margin-bottom-m')}>{siteContent.footer_tools_note}</p>

            <a
              className={cx(buttonCn.cta, buttonCn.s)}
              target="_blank"
              rel="noreferrer"
              href="https://climateanalytics.org/tools">
              {siteContent.all_tools_cta}
            </a>
          </div>
          <nav className={cx(gridCn.col2, gridCn.offset10, gridCn.mCol6)}>
            <ul className={cn.footerNav}>
              <li className={cn.footerNavItem}>
                <Link href="/about">
                  <a className={cx(cn.footerLink)}>{siteContent.about_label}</a>
                </Link>
              </li>
              <li className={cn.footerNavItem}>
                <Link href="/methodology">
                  <a className={cx(cn.footerLink)}>{siteContent.methodology_label}</a>
                </Link>
              </li>
              {renderContact && (
                <li className={cn.footerNavItem}>
                  <Link href="/about#contact">
                    <a className={cx(cn.footerLink, cn.secondary)}>Contact us</a>
                  </Link>
                </li>
              )}
            </ul>
          </nav>
        </div>
        <div className={cx(gridCn.grid, cn.footer, cn.caFooter)}>
          <div className={gridCn.col12}>
            <p className={cn.footerCaTitle}>
              <a href="http://climateanalytics.org">
                <b>Climate Analytics</b>
              </a>
            </p>
          </div>
          <div className={cx(gridCn.col4, gridCn.mCol6, cn.caInfo, cn.footerSection)}>
            <p className={cn.footerText}>{siteContent.footer_ca_byline}</p>
          </div>
          <div className={cx(gridCn.col2, gridCn.mCol4, gridCn.mOffset10, cn.footerSection)}>
            <div>Berlin, Germany</div>
            <div>Lomé, Togo</div>
            <div>New York, USA</div>
          </div>
          <div className={cx(gridCn.col4, gridCn.mCol6, cn.footerSection)}>
            <div>
              {renderContact && (
                <a href="tel:+49(0)30259229520" title="Call us at +49 (0)30 2 59 22 95 20">
                  +49 (0)30 2 59 22 95 20
                </a>
              )}
            </div>
            <div>
              <a href="mailto:contact@climateanalytics.org">contact@climateanalytics.org</a>
            </div>
          </div>
          <nav className={cx(gridCn.col2, gridCn.mCol6, gridCn.offset10, cn.footerSection)}>
            <ul className={cn.footerNav}>
              <li className={cn.footerNavItem}>
                <a href="https://climateanalytics.org//legal/">Legal</a>
              </li>
              <li className={cn.footerNavItem}>
                <a href="https://climateanalytics.org//disclaimer/">Disclaimer</a>
              </li>
              <li className={cn.footerNavItem}>
                <Link href="/privacy-statement/">Privacy Statement</Link>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
