import { useCallback, useEffect, useState } from 'react';
import { useMatomo } from '@datapunt/matomo-tracker-react';
import useMatomoInstance from 'src/utils/useMatomoInstance';
import cn from './style.module.scss';
import cx from 'classnames';
import buttonCn from 'src/style/button.module.scss';
import pageCn from 'src/style/page.module.scss';

const CookieBanner = () => {
  const { pushInstruction } = useMatomo();
  const [consentGiven, setConsentGiven] = useState(false);
  const matomo = useMatomoInstance();

  useEffect(() => {
    if (!matomo) return;
    const tracker = matomo.getTracker('https://climateanalytics.org/stats/matomo.php', 8);
    setConsentGiven(tracker.hasRememberedConsent());
  }, [matomo]);

  const handleOptOut = useCallback(() => {
    pushInstruction('forgetConsentGiven');
    setConsentGiven(false);
  }, []);

  return (
    <div className={pageCn.content}>
      <h2>Cookie consent status</h2>
      <div className={cx(cn.optOutBox)}>
        {consentGiven ? (
          <>
            <p className={cn.optOutNote}>
              You may choose to prevent this website from aggregating and analyzing the actions you
              take here. Doing so will protect your privacy, but will also prevent the owner from
              learning from your actions and creating a better experience for you and other users.
            </p>
            <button
              onClick={handleOptOut}
              className={cx(cn.optOutButton, buttonCn.l, buttonCn.outline)}>
              Opt-out from being tracked
            </button>
          </>
        ) : (
          <p className={cn.optOutNote}>
            You are not currently being tracked since you have not yet given consent to do so.
          </p>
        )}
      </div>
    </div>
  );
};

export default CookieBanner;
