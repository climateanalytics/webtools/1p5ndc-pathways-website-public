import { useCallback, useEffect, useState } from 'react';
import { useMatomo } from '@datapunt/matomo-tracker-react';
import useMatomoInstance from 'src/utils/useMatomoInstance';
import cn from './style.module.scss';
import Link from 'next/link';
import cx from 'classnames';
import buttonCn from 'src/style/button.module.scss';

const CookieBanner = () => {
  const { pushInstruction, trackPageView } = useMatomo();
  const [showBanner, setShowBanner] = useState(false);
  const matomo = useMatomoInstance();

  useEffect(() => {
    if (!matomo) return;
    const tracker = matomo.getTracker('https://climateanalytics.org/stats/matomo.php', 8);
    setShowBanner(!tracker.hasRememberedConsent());
  }, [matomo]);

  const handleConsent = useCallback(() => {
    pushInstruction('rememberConsentGiven');
    trackPageView();
    setShowBanner(false);
  }, []);

  return (
    showBanner && (
      <div className={cx(cn.consentBox, 'consent-box')}>
        <p className={cn.consentNote}>
          We use cookies to ensure that we give you the best experience on our website. You can find
          more details in our <Link href="/privacy-statement">Privacy Statement</Link>.
        </p>
        <button
          className={cx(cn.consentButton, buttonCn.l, buttonCn.outline, buttonCn.inverted)}
          onClick={handleConsent}>
          OK
        </button>
      </div>
    )
  );
};

export default CookieBanner;
