import cn from './style.module.scss';
import cx from 'classnames';
import gridCn from 'src/style/grid.module.scss';
import pageCn from 'src/style/page.module.scss';
import iconCn from 'src/style/icon.module.scss';

import Html, { StyledHtml } from 'src/components/content/Html';
import { useMemo } from 'react';

const Targets = ({ siteContent, country, meta }) => {
  const { content } = country;
  const targetType = meta.emissionTargetTypesById[content.target_type];
  const coveredSectors = content.sector_coverage.map((id) => meta.sectorsById[id]).filter((d) => d);
  const coveredGases = content.gas_coverage.map((id) => meta.gasesById[id]).filter((d) => d);
  const sectoralTargets = content.sectoral_targets.map(({ id, content }) => ({
    ...meta.sectorsById[id],
    content
  }));

  const economyWideItems = [
    [
      {
        title: siteContent.target_type_title,
        content: targetType?.name
      },
      {
        title: siteContent.ndc_target_title,
        content: content.ndc_target
      },
      {
        title: siteContent.national_target_title,
        content: content.national_target
      },
      {
        title: siteContent.market_mechanisms_title,
        content: content.market_mechanisms
      }
    ],
    [
      {
        title: siteContent.long_term_target_title,
        content: content.long_term_target
      },
      {
        title: siteContent.sector_coverage_title,
        component:
          coveredSectors.length &&
          coveredSectors.map((sector) => (
            <span key={sector.id} className={cx(cn.coverageItem, `sector-${sector.id}`)}>
              {sector.name}
            </span>
          ))
      },
      {
        title: siteContent.gas_coverage_title,
        component:
          coveredGases.length &&
          coveredGases.map((gas) => (
            <span key={gas.id} className={cx(cn.coverageItem, `gas-${gas.id}`)}>
              {gas.name}
            </span>
          ))
      }
    ]
  ];

  const sectorItems = useMemo(() => {
    return sectoralTargets.reduce(
      (acc, { name, content, id }) => {
        const index = acc[0].length > acc[1].length ? 1 : 0;
        acc[index].length += content.length;
        acc[index].items.push(
          <div key={id} className={cx(cn.target)}>
            <h4 className={cn.targetTitle}>
              <span className={cx(`icon-${id}`, iconCn.l, iconCn.bare, iconCn.before)} />
              {name}
            </h4>
            <Html className={pageCn.enhanceContent} html={content} />
          </div>
        );
        return acc;
      },
      [
        { items: [], length: 0 },
        { items: [], length: 0 }
      ]
    );
  }, [sectoralTargets]);

  return (
    <>
      <StyledHtml html={content.targets_and_commitments} className={cx(gridCn.col6)} />
      <h3 className={cx(gridCn.col10)}>{siteContent.economy_wide_targets_title}</h3>
      <div className={cx(gridCn.col10, cn.targetsCard)}>
        {economyWideItems.map((col, i) => (
          <div key={i} className={cn.targets}>
            {col.map(({ content, component, title }, itemIndex) => {
              if (!content && !component) return;
              return (
                <div key={itemIndex} className={cx(pageCn.enhanceContent, cn.target)}>
                  <h4 className={cn.targetTitle}>{title}</h4>
                  {component || <Html className={pageCn.enhanceContent} html={content} />}
                </div>
              );
            })}
          </div>
        ))}
      </div>
      <h3 className={cx(gridCn.col10)}>{siteContent.sectoral_targets_title}</h3>
      <div className={cx(gridCn.col10, cn.targetsCard)}>
        {sectorItems.map((col, i) => (
          <div key={i} className={cn.targets}>
            {col.items}
          </div>
        ))}
      </div>
    </>
  );
};

export default Targets;
