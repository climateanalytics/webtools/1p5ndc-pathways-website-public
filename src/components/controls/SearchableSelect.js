import { useCombobox } from 'downshift';
import { useMemo, useCallback, useState, useRef } from 'react';
import { sortByName } from 'src/utils';
import buttonCn from 'src/style/button.module.scss';
import usePopupPosition from './usePopupPosition';
import cx from 'classnames';
import SimpleTray from './SimpleTray';
import ctrCn from './style.module.scss';
import { useRouter } from 'next/router';

const SearchableSelect = ({
  id,
  items = [],
  label,
  buttonLabel,
  value,
  placeholder = 'Search...',
  emptyLabel = 'Make a selection...',
  wrapperClassName,
  wrapperModifiers = [],
  buttonClassName,
  buttonModifiers = ['base'],
  trayPositionRef,
  size,
  onChange,
  disabled,
  showToggleIcon = true
}) => {
  const selectId = id || items[0]?.id || label;
  const router = useRouter();
  const [inputValue, setInputValue] = useState('');
  const handleSelectedItemChange = useCallback(
    ({ selectedItem, type }) => {
      if (selectedItem.href && !selectedItem.external) router.push(selectedItem.href);
      if (selectedItem.href && selectedItem.external) window.location === selectedItem.href;
      if (type === useCombobox.stateChangeTypes.InputBlur) return;
      onChange && onChange(selectedItem.id, id);
    },
    [onChange]
  );

  //const isMobile = useMedia({ values: [true, true], defaultValue: false });

  const flatItems = useMemo(
    () => items.reduce((acc, item) => [...acc, ...(item.children || [item])], []),
    [items]
  );

  const { filteredItems, filteredFlatItems } = useMemo(() => {
    const search = inputValue.toLowerCase();

    const filteredFlatItems = flatItems
      .filter((child) => child.name.toLowerCase().includes(search))
      .sort(sortByName);

    let filteredItems = items
      .map((item) => {
        if (!item.children) return filteredFlatItems.includes(item) ? item : null;
        const children = filteredFlatItems.filter((d) => item.children.indexOf(d) >= 0);
        return children.length
          ? {
              ...item,
              children
            }
          : null;
      })
      .filter((d) => d)
      .sort(sortByName);

    return { filteredItems, filteredFlatItems };
  }, [items, flatItems, inputValue]);

  const selectedItem = useMemo(
    () => flatItems.find((item) => item.id === value) || null,
    [value, items]
  );

  const {
    isOpen,
    getToggleButtonProps,
    getLabelProps,
    getMenuProps,
    getInputProps,
    getComboboxProps,
    highlightedIndex,
    openMenu,
    getItemProps
  } = useCombobox({
    items: filteredFlatItems,
    id: selectId,
    inputValue,
    selectedItem,
    onSelectedItemChange: handleSelectedItemChange,
    itemToString: (item) => (item ? item.name : ''),
    onInputValueChange: ({ inputValue }) => setInputValue(inputValue)
  });

  const toggleRef = useRef(null);
  const trayPosition = usePopupPosition({
    isOpen,
    ref: trayPositionRef || toggleRef
  });

  const toggleClass = cx(
    ...buttonModifiers.map((m) => buttonCn[m] || m),
    buttonCn[size],
    buttonClassName || ctrCn.button
  );

  return (
    <div
      className={cx(
        wrapperClassName,
        ...wrapperModifiers,
        ctrCn.wrapper,
        ctrCn.searchable,
        isOpen && ctrCn.isOpen,
        ctrCn[size]
      )}>
      {label && (
        <label {...getLabelProps()} className={cx(ctrCn.label)}>
          {label}
        </label>
      )}
      <div {...getComboboxProps()} className={ctrCn.inputWrapper}>
        <input
          className={cx(buttonCn.ghost, ctrCn.input, toggleClass)}
          placeholder={placeholder}
          style={{ opacity: isOpen ? '1' : '0' }}
          {...getInputProps({
            onFocus: () => {
              if (!isOpen) {
                openMenu();
                setInputValue('');
              }
            }
          })}
        />
        <button
          type="button"
          {...getToggleButtonProps({ ref: toggleRef, disabled })}
          aria-label="toggle menu"
          className={toggleClass}
          disabled={disabled}>
          <span className={buttonCn.buttonText}>
            {buttonLabel || selectedItem?.name || emptyLabel}
          </span>
          {showToggleIcon && (
            <span
              className={cx(isOpen ? 'icon-carret-up' : 'icon-carret-down', ctrCn.toggleIcon)}
            />
          )}
        </button>
      </div>
      <SimpleTray
        menuProps={getMenuProps()}
        isOpen={isOpen}
        position={trayPosition}
        items={filteredItems}
        highlightedIndex={highlightedIndex}
        selectedItem={selectedItem}
        getItemProps={getItemProps}
      />
    </div>
  );
};

export default SearchableSelect;
