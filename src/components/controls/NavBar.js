import Link from 'next/link';
import buttonCn from 'src/style/button.module.scss';
import ctrlCn from './style.module.scss';
import cx from 'classnames';
import useGroupFocus from 'src/utils/useGroupFocus';
import useScrollWrapper from 'src/utils/useScrollWrapper';

export default function NavBar({
  pages,
  basePath,
  current,
  label,
  wrapperModifiers = [],
  size = 'l'
}) {
  const [wrapperRef, overflow] = useScrollWrapper();
  if (!pages) return null;

  const focusProps = useGroupFocus({
    items: pages,
    selector: '[role="menuitem"]'
  });

  return (
    <nav
      className={cx(
        overflow[0] && ctrlCn.overflowLeft,
        overflow[1] && ctrlCn.overflowRight,
        ctrlCn.navBarWrapper,
        ...wrapperModifiers
      )}
      aria-label={label}
      ref={wrapperRef}>
      <ul role="menubar" {...focusProps} className={buttonCn.group}>
        {pages.map((item) => {
          const isCurrent = current === item.page;

          return (
            <li key={item.slug} className={ctrlCn.tabWrapper}>
              <Link href={`${basePath}/${item.slug}`}>
                <a
                  className={cx(
                    buttonCn[size],
                    isCurrent && buttonCn.base,
                    !isCurrent && buttonCn.secondary,
                    ctrlCn.tab
                  )}
                  aria-current={isCurrent && 'location'}
                  role="menuitem">
                  <span className={buttonCn.buttonText}>{item.name}</span>
                </a>
              </Link>
            </li>
          );
        })}
      </ul>
    </nav>
  );
}
