import SegmentedControl from './SegmentedControl';
import SimpleSelect from './SimpleSelect';

export default {
  SegmentedControl,
  SimpleSelect
};
