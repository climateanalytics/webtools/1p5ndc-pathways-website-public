import cn from './style.module.scss';
import cx from 'classnames';

const Static = ({ showInStatic, className, selected, label }) =>
  showInStatic ? (
    <div className={cx(className, cn.static)}>
      {label && <span className={cn.staticLabel}>{label}: </span>}{' '}
      {selected && <b> {selected.name}</b>}
    </div>
  ) : null;

export default Static;
