import { memo } from 'react';
import ctrCn from './style.module.scss';
import SelectItem from './SelectItem';
import cx from 'classnames';
import { reduceBy } from 'src/utils';

const GroupedItems = memo(function GroupedItems({
  items,
  allowEmpty,
  highlightedIndex,
  selectedItem,
  getItemProps
}) {
  return items.reduce(
    (acc, item) => {
      let element;
      if (item.children) {
        const children = item.children.map((item, itemIndex) => {
          const index = acc.itemIndex++;
          const isSelected = selectedItem === item;
          return (
            <SelectItem
              key={itemIndex}
              itemProps={getItemProps({ item, index })}
              isHighlighted={index === highlightedIndex}
              isSelected={isSelected}
              clearable={isSelected && allowEmpty}
              {...item}
            />
          );
        });
        element = (
          <li key={item.id} className={ctrCn.selectGroup}>
            {item.name && <div className={ctrCn.selectGroupName}>{item.name}</div>}
            <ul className={ctrCn.selectItems}>{children}</ul>
          </li>
        );
      } else {
        const index = acc.itemIndex++;
        const isSelected = selectedItem === item;
        element = (
          <SelectItem
            key={item.id}
            itemProps={getItemProps({ item, index })}
            isHighlighted={index === highlightedIndex}
            isSelected={isSelected}
            clearable={isSelected && allowEmpty}
            {...item}
          />
        );
      }
      acc.sections.push(element);

      return acc;
    },
    { sections: [], itemIndex: 0 }
  ).sections;
});

const Tray = ({
  menuProps,
  isOpen,
  dimensions = {},
  emptyListText = 'No options found',
  renderWhenClosed,
  children,
  trayTag = 'ul',
  modifiers = [],
  items
}) => {
  const renderTray = renderWhenClosed || isOpen;
  const showTray = isOpen;
  // const renderTray = true;
  // const showTray = true;
  const className = cx(
    !showTray && ctrCn.isHidden,
    ctrCn.tray,
    modifiers.map((m) => ctrCn[m] || m)
  );

  const TrayTag = trayTag;

  return (
    <TrayTag
      {...menuProps}
      aria-hidden={!showTray}
      className={className}
      style={{
        [dimensions.alignVertical]: dimensions[dimensions.alignVertical],
        [dimensions.alignHorizontal]: dimensions[dimensions.alignHorizontal],
        minWidth: dimensions.minWidth
      }}>
      {!items.length && <div className={cx(ctrCn.emtyListText, ctrCn.item)}>{emptyListText}</div>}
      {renderTray && children}
    </TrayTag>
  );
};

const SimpleTray = ({
  menuProps,
  isOpen,
  dimensions,
  items,
  allowEmpty,
  selectedItem,
  highlightedIndex,
  getItemProps,
  emptyListText = 'No options found',
  renderWhenClosed,
  trayModifiers,
  trayTag,
  children
}) => {
  return (
    <Tray
      menuProps={menuProps}
      isOpen={isOpen}
      trayTag={trayTag}
      renderWhenClosed={renderWhenClosed}
      items={items}
      modifiers={trayModifiers}
      dimensions={dimensions}
      emptyListText={emptyListText}>
      {children ? (
        children({
          items,
          highlightedIndex,
          getItemProps,
          selectedItem,
          itemsById: reduceBy(items)
        })
      ) : (
        <GroupedItems
          items={items}
          highlightedIndex={highlightedIndex}
          selectedItem={selectedItem}
          allowEmpty={allowEmpty}
          getItemProps={getItemProps}
        />
      )}
    </Tray>
  );
};

export default memo(SimpleTray);
