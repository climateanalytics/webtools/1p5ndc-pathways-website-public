import { memo } from 'react';
import ctrCn from './style.module.scss';
import cx from 'classnames';
import Link from 'next/link';

const SelectItem = ({
  itemProps,
  disabled,
  iconBefore,
  iconAfter,
  href,
  download,
  name,
  isHighlighted,
  isSelected,
  clearable,
  external
}) => {
  const className = cx(
    ctrCn.selectItem,
    isHighlighted && ctrCn.isHighlighted,
    isSelected && ctrCn.isSelected,
    iconBefore && ctrCn.iconBefore,
    iconAfter && ctrCn.iconAfter,
    disabled && ctrCn.disabled
  );

  const itemContent = (
    <>
      <span className={ctrCn.selectItemContent}>
        {iconBefore && <span className={cx(`icon-${iconBefore}`, ctrCn.selectItemIcon)} />} {name}
      </span>
      {iconAfter && <span className={cx(`icon-${iconAfter}`, ctrCn.selectItemIcon)} />}
    </>
  );

  if (href) {
    return (
      <li>
        <Link href={disabled ? window.location.pathname : href}>
          <a {...itemProps} download={download} className={className}>
            {itemContent}
          </a>
        </Link>
      </li>
    );
  }

  if (href && external) {
    return (
      <li>
        <a
          {...itemProps}
          href={disabled ? window.location.pathname : href}
          download={download}
          className={className}>
          {itemContent}
        </a>
      </li>
    );
  }

  return (
    <li {...itemProps} className={className}>
      {itemContent}
      {clearable && <span className={'icon-clear'} />}
    </li>
  );
};

export default memo(SelectItem);
