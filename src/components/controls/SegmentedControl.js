import { Fragment } from 'react';
import cn from './style.module.scss';
import cx from 'classnames';
import buttonCn from 'src/style/button.module.scss';
import useGroupFocus from 'src/utils/useGroupFocus';
import Static from './Static';

const SegmentedControl = ({
  segments,
  value,
  onChange,
  label,
  size,
  id,
  buttonModifiers = ['base'],
  wrapperModifiers = [],
  wrapperClassName,
  showInStatic = true,
  isStatic = false
}) => {
  const segmentClass = cx(
    ...buttonModifiers.map((m) => buttonCn[m] || m),
    buttonCn[size],
    cn.segment
  );
  const labelId = `segmented-control-${id}-label`;
  const focusProps = useGroupFocus({
    items: segments,
    selector: '[role="radio"]'
  });

  const selectedSegment = segments.find((segment) => segment.id === value) || segments[0];
  const wrapperClass = cx(
    wrapperClassName || [cn.wrapper, cn.segmentedControl, cn[size]],
    ...wrapperModifiers.map((m) => cn[m] || m)
  );

  if (isStatic)
    return (
      <Static
        selected={selectedSegment}
        label={label}
        showInStatic={showInStatic}
        className={wrapperClass}
      />
    );

  return (
    <div className={wrapperClass}>
      {label && (
        <div className={cn.label} id={labelId}>
          {label}
        </div>
      )}

      <div
        role="radiogroup"
        aria-labelledby={labelId}
        {...focusProps}
        className={cx(buttonCn.group, buttonCn.equal, cn.segments)}>
        {segments.map((item) => {
          const selected = item === selectedSegment;
          return (
            <button
              role="radio"
              key={item.id}
              aria-checked={selected}
              onClick={() => onChange && onChange(item.id, id)}
              disabled={item.disabled}
              className={cx(
                segmentClass,
                selected && buttonCn.selected,
                item.iconBefore && buttonCn.iconBefore
              )}>
              {item.iconBefore && <span className={`icon-${item.iconBefore}`} />}
              {item.icon ? (
                <span className={`icon-${item.icon}`} />
              ) : (
                <span className={buttonCn.buttonText}>{item.name}</span>
              )}
            </button>
          );
        })}
      </div>
    </div>
  );
};

export default SegmentedControl;
