import { memo, useMemo, useCallback, useRef } from 'react';
import buttonCn from 'src/style/button.module.scss';
import Tooltip from 'src/components/Tooltip';
import ctrCn from './style.module.scss';
import cx from 'classnames';

const SimpleSelect = ({
  id,
  items = [], // Item ids need to be unique
  label,
  showLabel = true,
  value,
  description,
  wrapperClassName, // Replaces wrapper class
  wrapperModifiers = [], // Added to wrapper class
  buttonClassName, // Replaces button class
  buttonModifiers = ['base'], // Added to wrapper class, either modifiers from regular button or full class names
  size,
  buttonIcon,
  onChange,
  disabled
}) => {
  const isIconButton = buttonModifiers.includes('icon');
  const toggleRef = useRef(null);

  const descriptionId = `${id}-select-description`;
  const selectId = `${id}-select`;

  const wrapperClass = cx(
    wrapperClassName || [ctrCn.wrapper, ctrCn[size]],
    ...wrapperModifiers,
    ...buttonModifiers.map((m) => ctrCn[m]), // enable specific wrapper styles tailored to button types
    disabled && ctrCn.disabled
  );

  const handleChange = useCallback(
    (value) => {
      let selectedItem;
      items.forEach((item) => {
        if (selectedItem) return;
        if (item.id === value) selectedItem = item;
        if (item.children?.length) selectedItem = item.children.find((child) => child.id === value);
      });
      onChange({ selectedItem });
    },
    [onChange]
  );

  const buttonClass =
    buttonClassName ||
    cx(
      ctrCn.button,
      ...buttonModifiers.map((m) => buttonCn[m] || m),
      buttonCn[size],
      buttonIcon && !isIconButton && buttonCn.iconBeforeAndAfter
    );

  return (
    <>
      <div id={selectId} className={wrapperClass}>
        <label htmlFor={selectId} className={cx(ctrCn.label, !showLabel && 'screenreader-only')}>
          {label}
        </label>
        <select id={selectId} className={buttonClass} onBlur={handleChange}>
          {items.map((item) => {
            if (item.children?.length)
              return (
                <optgroup disabled={item.disabled} key={item.id} label={item.name}>
                  {item.children.map((child) => (
                    <option
                      key={child.id}
                      value={child.id}
                      disabled={child.disabled}
                      selected={value === child.id}>
                      {child.name}
                    </option>
                  ))}
                </optgroup>
              );
            return (
              <option
                key={item.id}
                value={item.id}
                disabled={item.disabled}
                selected={value === item.id}>
                {item.name}
              </option>
            );
          })}
        </select>
      </div>
      {description && (
        <Tooltip
          autoControl={true}
          wrapperModifiers={['delayed', 'mini']}
          el={toggleRef.current}
          id={descriptionId}>
          {description}
        </Tooltip>
      )}
    </>
  );
};

export default memo(SimpleSelect);
