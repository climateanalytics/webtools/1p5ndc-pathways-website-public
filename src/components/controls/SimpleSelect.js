import { useSelect } from 'downshift';
import { memo, useMemo, useCallback, useRef, useEffect, useState } from 'react';
import buttonCn from 'src/style/button.module.scss';
import usePopupPosition from './usePopupPosition';
import SimpleTray from './SimpleTray';
import Tooltip from 'src/components/Tooltip';
import ctrCn from './style.module.scss';
import cx from 'classnames';
import { useRouter } from 'next/router';
import Static from './Static';

const getStateReducer = ({ allowEmpty }) => {
  function handleSelect(state, changes) {
    if (!allowEmpty) return changes;
    return {
      selectedItem: changes.selectedItem === state.selectedItem ? null : changes.selectedItem
    };
  }

  function stateReducer(state, actionAndChanges) {
    const { type, changes } = actionAndChanges;
    switch (type) {
      case useSelect.stateChangeTypes.MenuKeyDownEnter:
        return handleSelect(state, changes);
      case useSelect.stateChangeTypes.ItemClick:
        return handleSelect(state, changes);
      default:
        return changes; // otherwise business as usual.
    }
  }
  return stateReducer;
};

const SimpleSelect = ({
  id,
  items = [], // Item ids need to be unique
  label,
  value,
  buttonLabel,
  description,
  wrapperClassName, // Replaces wrapper class
  wrapperModifiers = [], // Added to wrapper class
  buttonClassName, // Replaces button class
  buttonModifiers = ['base'], // Added to wrapper class, either modifiers from regular button or full class names
  toggleIconClassName, // Replaces toggle icon class
  trayModifiers,
  trayGap,
  allowEmpty = false,
  emptyText = 'Select an option',
  size,
  buttonIcon,
  trayPositionRef,
  onChange,
  disabled,
  trayTag,
  showToggleIcon = true,
  renderWhenClosed = false, // Make tray element accessible even when tray is not open
  children, // custom tray,
  isStatic = false,
  showInStatic = true,
  dynamicWidth = true
}) => {
  const isIconButton = buttonModifiers.includes('icon');
  const selectId = id || items[0]?.id;
  const router = useRouter();
  const flatItems = useMemo(
    () => items.reduce((acc, item) => [...acc, ...(item.children || [item])], []),
    [items]
  );

  const selectedItem = useMemo(
    () => flatItems.find((item) => item.id === value) || null,
    [value, items]
  );

  const handleSelectedItemChange = useCallback(
    ({ selectedItem } = {}) => {
      if (selectedItem.href && !selectedItem.external) router.push(selectedItem.href);
      if (selectedItem.href && selectedItem.external) window.location === selectedItem.href;
      onChange && onChange(selectedItem?.id, id);
    },
    [selectedItem, onChange]
  );

  const {
    isOpen,
    getToggleButtonProps,
    getLabelProps,
    getMenuProps,
    highlightedIndex,
    getItemProps
  } = useSelect({
    id: selectId,
    items: flatItems,
    selectedItem,
    onSelectedItemChange: handleSelectedItemChange,
    itemToString: (item) => (item ? item.name : ''),
    stateReducer: getStateReducer({ allowEmpty })
  });

  const toggleRef = useRef(null);
  const trayPosition = usePopupPosition({
    isOpen,
    ref: trayPositionRef || toggleRef
  });

  const trayYPosition = usePopupPosition({
    isOpen,
    ref: toggleRef,
    gap: trayGap
  });

  const trayDimensions = useMemo(
    () => ({
      ...trayPosition,
      top: trayYPosition.top
    }),
    [trayYPosition, trayPosition]
  );

  const showLabel = label && !isIconButton;

  const descriptionId = `${id}-select-description`;
  const wrapperId = `${id}-select-wrapper`;

  const toggleIcon =
    !isIconButton && showToggleIcon ? (
      <span className={cx(isOpen ? 'icon-carret-up' : 'icon-carret-down', toggleIconClassName)} />
    ) : null;

  const toggleButtonProps = getToggleButtonProps({
    ref: toggleRef,
    disabled
  });

  const wrapperClass = cx(
    wrapperClassName || [ctrCn.wrapper, ctrCn[size]],
    ...wrapperModifiers.map((m) => ctrCn[m] || m),
    ...buttonModifiers.map((m) => ctrCn[m]), // enable specific wrapper styles tailored to button types
    disabled && ctrCn.disabled
  );

  const buttonText = !isIconButton && (buttonLabel || selectedItem?.name || emptyText);
  const buttonClass =
    buttonClassName ||
    cx(
      ctrCn.button,
      ...buttonModifiers.map((m) => buttonCn[m] || m),
      buttonCn[size],
      !isIconButton && showToggleIcon && buttonCn.iconAfter,
      buttonIcon && !isIconButton && !showToggleIcon && buttonCn.iconBefore,
      buttonIcon && !isIconButton && showToggleIcon && buttonCn.iconBeforeAndAfter
    );

  // useEffect(() => {
  //   // Move Tray into view on mobile if there are many options
  //   if (isOpen && isMobile && flatItems.length > 20) {
  //     toggleRef.current.scrollIntoView();
  //     document.body.style.setProperty('overflow', 'hidden');
  //   } else if (!isOpen) {
  //     document.body.style.removeProperty('overflow');
  //   }
  // }, [isOpen, isMobile, toggleRef]);

  if (isStatic)
    return (
      <Static
        selected={selectedItem}
        label={label}
        showInStatic={showInStatic}
        className={wrapperClass}
      />
    );

  return (
    <>
      <div id={wrapperId} className={wrapperClass}>
        <label {...getLabelProps()} className={cx(ctrCn.label, !showLabel && 'screenreader-only')}>
          {label}
        </label>
        <button
          {...toggleButtonProps}
          aria-describedby={descriptionId}
          type="button"
          disabled={disabled}
          aria-label="toggle menu"
          className={buttonClass}>
          {buttonIcon && <span className={cx(`icon-${buttonIcon}`)} />}
          {dynamicWidth ? <span className={buttonCn.buttonText}>{buttonText}</span> : buttonText}
          {toggleIcon}
        </button>

        <SimpleTray
          menuProps={getMenuProps()}
          isOpen={isOpen}
          dimensions={trayDimensions}
          items={items}
          highlightedIndex={highlightedIndex}
          selectedItem={selectedItem}
          getItemProps={getItemProps}
          renderWhenClosed={renderWhenClosed}
          trayTag={trayTag}
          trayModifiers={trayModifiers}>
          {children}
        </SimpleTray>
      </div>
      {description && (
        <Tooltip
          autoControl={true}
          wrapperModifiers={['delayed', 'mini']}
          el={toggleRef.current}
          id={descriptionId}>
          {description}
        </Tooltip>
      )}
    </>
  );
};

export default memo(SimpleSelect);
