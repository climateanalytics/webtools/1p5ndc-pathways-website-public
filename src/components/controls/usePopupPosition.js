import { useState, useEffect } from 'react';
import { debounce } from 'src/utils';

const usePopupPosition = ({ isOpen, gap = 5, direction = 'vertical', ref }) => {
  const [position, setPosition] = useState({ left: 0, top: 0, width: 0, height: 0 });
  //const [node, setNode] = useState(null);

  useEffect(() => {
    const updatePosition = debounce(
      () => {
        if (!ref.current) return;
        const node = ref.current;
        const bbox = node.getBoundingClientRect();
        const alignHorizontal = bbox.left > window.innerWidth / 2 ? 'right' : 'left';
        isOpen && alignHorizontal;
        const alignVertical = bbox.top > window.innerHeight / 2 ? 'bottom' : 'top';
        const xGap = direction === 'vertical' ? 0 : gap;
        const yGap = direction === 'vertical' ? gap : 0;

        if (bbox.bottom !== position.top || bbox.left !== position.left)
          setPosition({
            bottom: window.innerHeight - bbox.top + yGap,
            top: bbox.bottom + yGap,
            left: bbox.left + xGap,
            right: window.innerWidth - bbox.right + xGap,
            centerX: bbox.left + bbox.width / 2,
            centerY: bbox.top + bbox.height / 2,
            alignHorizontal,
            alignVertical,
            minWidth: bbox.width
          });
      },
      isOpen ? 10 : 100
    );

    window.addEventListener('scroll', updatePosition);
    window.addEventListener('resize', updatePosition);
    updatePosition();
    return () => {
      window.removeEventListener('scroll', updatePosition);
      window.removeEventListener('resize', updatePosition);
    };
  }, [ref, isOpen]);

  // const ref = useCallback((node) => {
  //   setNode(node);
  // }, []);
  return position;
};

export default usePopupPosition;
