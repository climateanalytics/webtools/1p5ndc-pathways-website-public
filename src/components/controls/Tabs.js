import { useState, useMemo, memo, useEffect } from 'react';
import btnCn from 'src/style/button.module.scss';
import gridCn from 'src/style/grid.module.scss';
import ctrlCn from './style.module.scss';
import useGroupFocus from 'src/utils/useGroupFocus';
import cx from 'classnames';
import useScrollWrapper from 'src/utils/useScrollWrapper';

const Tabs = ({
  tabs,
  props,
  id,
  label,
  size = 'l',
  wrapperModifiers = [],
  tabModifiers = ['bare']
}) => {
  const [selected, setSelected] = useState(tabs[0].id);

  const enabledTabs = useMemo(() => tabs.filter((tab) => !tab.disabled && tab.Panel), [tabs]);
  const focusProps = useGroupFocus({
    items: enabledTabs,
    selector: '[role="tab"]'
  });
  const [, overflow] = useScrollWrapper(focusProps.node);

  useEffect(() => {
    const hash = window.location.hash.replace('#', '');
    const hashTarget = tabs.find((t) => t.id === hash);
    if (hashTarget) {
      setSelected(hash);
    }
  }, [focusProps]);

  const selectedTab = useMemo(() => tabs.find((t) => t.id === selected), [tabs, selected]);

  const tabClass = cx(
    btnCn[size],
    ctrlCn.tab,
    btnCn.iconBefore,
    tabModifiers.map((m) => btnCn[m] || m)
  );

  const currentPanelProps = {
    role: 'tabpanel',
    id: `${id}-panel-${selected}`,
    tabIndex: 0,
    'aria-labelledby': `${id}-tab-${selected}`
  };

  return (
    <>
      <div className={cx(gridCn.grid, wrapperModifiers)}>
        <div
          {...focusProps}
          className={cx(
            gridCn.col12,
            overflow[0] && ctrlCn.overflowLeft,
            overflow[1] && ctrlCn.overflowRight,
            ctrlCn.tabBarWrapper
          )}>
          <ul role="tablist" aria-label={label} className={btnCn.group}>
            {tabs.map((tab, i) => {
              const isSelected = tab.id === selected;
              return (
                <li key={i}>
                  <button
                    role="tab"
                    disabled={tab.disabled || !tab.Panel}
                    onClick={() => {
                      window.location.hash = tab.id;
                      setSelected(tab.id);
                    }}
                    aria-selected={isSelected}
                    aria-controls={`${id}-panel-${i}`}
                    id={`${id}-tab-${i}`}
                    className={tabClass}
                    tabIndex={isSelected ? 0 : -1}>
                    <span className={cx(ctrlCn.tabIcon, `icon-${tab.id}`)} />
                    <span className={btnCn.buttonText}>{tab.name}</span>
                  </button>
                </li>
              );
            })}
          </ul>
        </div>
      </div>
      {selectedTab.Panel && (
        <selectedTab.Panel panelProps={currentPanelProps} {...props} {...selectedTab} />
      )}
    </>
  );
};

export default memo(Tabs);
