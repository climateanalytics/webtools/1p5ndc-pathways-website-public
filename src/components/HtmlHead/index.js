import Head from 'next/head';
import { useRouter } from 'next/router';
import { useMemo } from 'react';

export default function HtmlHead({ title, description, country }) {
  const router = useRouter();
  const url = useMemo(() => `${process.env.url}${router.asPath}`, [router.asPath]);

  let image = `${process.env.url}/static/hero-1.jpg`;
  if (country) {
    const imageUrl = encodeURIComponent(
      `${process.env.url}/countries/${country.slug}/head_image?bare=true`
    );
    image =
      country &&
      `${process.env.screenshotUrl}?url=${imageUrl}&fileName=${country.slug}-head_image&height=440&width=850`;
  }

  return (
    <Head>
      <title>{title}</title>
      <link rel="icon" href="/static/favicon.ico" />
      <meta key="og-type" property="og:type" content="website" />
      <meta key="og-url" property="og:url" content={url} />
      <meta key="og-description" property="og:description" content={description} />
      <meta key="og-image" property="og:image" content={image} />
    </Head>
  );
}
