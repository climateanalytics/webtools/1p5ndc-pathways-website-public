import { Fragment, useMemo, memo, useCallback, useState, useEffect } from 'react';
import withGraphFrame from '../withGraphFrame';
import useDimensions from 'src/utils/useDimensions';
import { Group } from '@visx/group';
import { extent } from 'd3-array';
import { scaleLinear } from '@visx/scale';
import { LinePath, Area } from '@visx/shape';
import { curveCatmullRom } from '@visx/curve';
import chartCn from '../style.module.scss';
import cn from './style.module.scss';
import cx from 'classnames';
import { renderValue, useSeriesTicks, useDeviceInfo, formatPercent } from 'src/utils';
import { XAxis, YAxis, HorizontalGrid } from '../axes';
import MixedLegend from '../MixedLegend';
import { DotMark, YRangeMark } from './marks';

import {
  getYExtent,
  getLines,
  getAreas,
  getYRanges,
  getDots,
  getYRulers,
  getPoints,
  getMarkers,
  useVoronoiTooltip
} from './helpers';
import { getYTicks } from '../helpers';
import useMarkerLabels from './useMarkerLabels';
import ChartText from '../ChartText';

export { getYExtent };

const margin = { left: 0, top: 10, right: 0, bottom: 20 };

const MixedSeries = ({ chartClassName, data, spec, unit, id }) => {
  const [wrapperProps, { width, height }] = useDimensions({ width: 0, height: 500 });
  const chartWidth = width - margin.left - margin.right;
  const chartHeight = height - margin.top - margin.bottom;
  const { isXS } = useDeviceInfo();
  const isRelative = unit.id === 'pc';

  const yExtent = useMemo(() => getYExtent(spec, data), [spec, data]);
  const xExtent = useMemo(() => extent(data, (d) => d.year), [data]);

  const xScale = useMemo(
    () => scaleLinear({ domain: xExtent, range: [0, chartWidth] }),
    [chartWidth, xExtent]
  );
  const yScale = useMemo(
    () => scaleLinear({ domain: yExtent, range: [chartHeight, 0], nice: true }),
    [chartHeight, yExtent]
  );

  const memoProps = [spec, data, xScale, yScale, unit];
  const lines = useMemo(() => getLines({ spec, data, xScale, yScale }), memoProps);
  const areas = useMemo(() => getAreas({ spec, data, xScale, yScale }), memoProps);
  const yRanges = useMemo(() => getYRanges({ spec, xScale, yScale }), memoProps);
  const dots = useMemo(() => getDots({ spec, xScale, yScale }), memoProps);
  const yRulers = useMemo(() => getYRulers({ spec, xScale, yScale }), memoProps);
  const markers = useMemo(() => getMarkers({ spec, yScale, xScale, unit }), memoProps);
  const points = useMemo(
    () => getPoints({ lines, areas, yScale, unit }),
    [...memoProps, lines, areas]
  );

  const [visibleMarkers, setVisibleMarkers] = useState(markers);
  const [indicateTouch, setIndicateTouch] = useState(false);
  const touchIndicators = indicateTouch ? dots.filter((d) => !d.showOnMobile) : [];

  const [tooltip, { handleMouseLeave, handleMouseMove }] = useVoronoiTooltip({
    points,
    margin,
    width,
    height
  });

  const { ticks, specTick } = useMemo(() => getYTicks(spec, yScale), memoProps);

  const xTicks = useSeriesTicks(chartWidth, xScale.domain());

  const tooltipTicks = useMemo(() => {
    if (!tooltip) return [];
    const ttTick = tooltip.data.year;
    if (xTicks.includes(ttTick)) return xTicks;
    let ticks = xTicks.filter((t) => Math.abs(t - ttTick) > 4);
    return [...ticks, ttTick].sort((a, b) => a - b);
  }, [tooltip, xTicks]);

  const tooltipPoints = useMemo(
    () =>
      points.map((d) => {
        const isVisible = tooltip?.data.pointKey === d.pointKey && tooltipTicks.includes(d.year);
        const isMajor = tooltip?.data.year === d.year;
        return { ...d, isVisible, addUnit: isMajor && !isRelative, isMajor };
      }),
    [tooltip, points]
  );

  const handleMarkClick = useCallback(
    (mark) => {
      setVisibleMarkers([markers.find((m) => m.id === mark.id)]);
      setIndicateTouch(false);
    },
    [markers]
  );

  // Necessary in order to be consistent with ssr
  useEffect(() => setIndicateTouch(isXS), [isXS]);

  const resetMarkers = useCallback(
    () => isXS && setVisibleMarkers(markers.filter((d) => d.showOnMobile)),
    [isXS, markers]
  );

  useEffect(() => {
    setVisibleMarkers(isXS ? markers.filter((d) => d.showOnMobile) : markers);
  }, [markers, isXS]);

  const { labels: markerLabels, lines: markerLines } = useMarkerLabels({
    dots,
    markers: visibleMarkers,
    yRanges,
    width: chartWidth,
    height: chartHeight - 20,
    wrapperClassName: tooltip && cn.dimmed,
    labelClassName: cn.markerLabel,
    textClassName: cx(chartWidth < 800 && cn.smallLabel),
    margin
  });

  return (
    <>
      <div
        {...wrapperProps}
        className={cx(chartClassName, cn.wrapper, chartCn.chartWrapper, chartCn.accommodateYAxis)}>
        <svg id={`${id}-canvas`} width={width} height={height} className={chartCn.svg}>
          <defs>
            <filter id="shadow" x="-50%" y="-50%" width="200%" height="200%">
              <feDropShadow dx="0" dy="0" stdDeviation="2.5" floodColor="black" floodOpacity=".2" />
            </filter>
          </defs>
          <Group left={margin.left} top={margin.top}>
            <HorizontalGrid
              scale={yScale}
              ticks={ticks}
              highlightTicks={[specTick]}
              width={chartWidth}
            />
            <YAxis
              renderTick={renderValue}
              scale={yScale}
              ticks={ticks}
              numTicks={6}
              unit={unit.id === 'pc' && unit}
            />
            <XAxis
              showDomain={true}
              top={chartHeight}
              ticks={xTicks}
              width={chartWidth}
              height={chartHeight}
              scale={xScale}
            />
            {lines.map((line, i) => {
              let opacity = line.opacity;
              if (tooltip) opacity = line.key === tooltip?.data.key || 0.1;
              return (
                <LinePath
                  key={i}
                  data={line.data}
                  x={(d) => d.x}
                  y={(d) => d.y}
                  style={{
                    stroke: line.color,
                    ...line.style,
                    ...(opacity ? { opacity } : {})
                  }}
                  curve={curveCatmullRom}
                  className={cx(chartCn.markLine, line.dashed && chartCn.dashed)}
                />
              );
            })}
            {areas.map((area, i) => {
              let opacity = area.opacity;
              if (tooltip) opacity = area.key === tooltip?.data.key || 0.1;
              return (
                <Fragment key={i}>
                  <Area
                    data={area.data}
                    x={(d) => d.x}
                    y0={(d) => d.y0}
                    y1={(d) => d.y1}
                    style={{
                      fill: area.color,
                      ...area.style,
                      ...(opacity ? { opacity } : {})
                    }}
                    className={chartCn.markArea}
                    curve={curveCatmullRom}
                  />
                  <LinePath
                    data={area.data}
                    x={(d) => d.x}
                    y={(d) => d.y0}
                    style={{ stroke: area.color }}
                    className={chartCn.markAreaOutline}
                    curve={curveCatmullRom}
                  />
                  <LinePath
                    data={area.data}
                    x={(d) => d.x}
                    y={(d) => d.y1}
                    style={{ stroke: area.color }}
                    className={chartCn.markAreaOutline}
                    curve={curveCatmullRom}
                  />
                </Fragment>
              );
            })}
            {yRulers.map((ruler, i) => (
              <line
                key={i}
                y1={ruler.y}
                y2={ruler.y}
                x1={ruler.x1}
                x2={ruler.x2}
                className={chartCn.yRuler}
                style={{ stroke: ruler.color, opacity: tooltip ? 0.1 : 1 }}
              />
            ))}
            {chartWidth > 0 && (
              <rect
                width={chartWidth}
                height={chartHeight}
                className={cn.hoverArea}
                onClick={resetMarkers}
                onMouseMove={handleMouseMove}
                onMouseLeave={handleMouseLeave}
                onTouchMove={handleMouseMove}
                onTouchEnd={handleMouseLeave}
              />
            )}
            {markerLines}
            {yRanges.map((range, i) => {
              return (
                <YRangeMark dimm={!!tooltip} onClick={handleMarkClick} key={i} range={range} />
              );
            })}

            {dots.map((dot, i) => (
              <DotMark
                dimm={!!tooltip}
                indicateTouch={touchIndicators.includes(dot)}
                onClick={handleMarkClick}
                key={i}
                dot={dot}
              />
            ))}
          </Group>
        </svg>
        {markerLabels}
        <svg className={cn.tooltipContainer} width={width} height={height}>
          <Group top={margin.top} left={margin.left}>
            {tooltipPoints.map((d, i) => {
              return (
                <Group key={i} top={d.y} left={d.x}>
                  <ChartText
                    key={i}
                    className={cx(cn.label, d.isMajor && cn.major, d.isVisible && cn.isVisible)}
                    textAnchor="middle"
                    y={-8}>
                    {renderValue(d?.value, {
                      signed: isRelative,
                      unit,
                      addUnit: d.isMajor
                    })}
                  </ChartText>
                  <circle
                    filter="url(#shadow)"
                    r={4}
                    className={cx(cn.tooltipDot, d.isVisible && cn.isVisible)}
                    style={{ fill: d.color }}
                  />
                </Group>
              );
            })}
          </Group>
        </svg>
      </div>
      <MixedLegend items={spec} />
    </>
  );
};

export default withGraphFrame(memo(MixedSeries));
