import { Fragment, memo } from 'react';
import chartCn from '../style.module.scss';
import cx from 'classnames';
import { Group } from '@visx/group';

const yRangeWidth = 10;
const dotSize = yRangeWidth / 2;
const yRangeCenter = yRangeWidth / 2;

const DotMarkComponent = ({ dot, onClick, dimm, indicateTouch }) => {
  const { x, y, color } = dot;
  return (
    <>
      {indicateTouch && (
        <circle
          r={dotSize}
          cx={x}
          cy={y}
          style={{
            fill: color,
            stroke: color,
            transformOrigin: `${x}px ${y}px`,
            opacity: dimm ? 0.2 : 1
          }}
          className={cx(chartCn.interactive, chartCn.markForeground)}
        />
      )}
      <circle
        r={dotSize}
        cx={x}
        cy={y}
        style={{ opacity: dimm ? 0.2 : 1 }}
        className={chartCn.markBackground}
      />
      <circle
        r={dotSize}
        cx={x}
        cy={y}
        onClick={() => onClick && onClick(dot)}
        style={{
          fill: color,
          stroke: color,
          opacity: dimm ? 0.2 : 1
        }}
        className={chartCn.markForeground}
      />
    </>
  );
};

export const DotMark = memo(DotMarkComponent);

export const YRangeMarkComponent = ({ range, dimm, onClick }) => {
  const { left, top, height, color } = range;
  return (
    <Group left={Math.round(left - yRangeCenter) + 0.5} top={top}>
      <rect
        rx={yRangeCenter}
        ry={yRangeCenter}
        height={Math.max(yRangeWidth, height)}
        width={yRangeWidth}
        className={cx(chartCn.markBackground)}
      />
      <rect
        rx={yRangeCenter}
        ry={yRangeCenter}
        height={Math.max(yRangeWidth, height)}
        width={yRangeWidth}
        onClick={() => onClick && onClick(range)}
        style={{
          fill: color,
          stroke: color,
          opacity: dimm ? 0.2 : 1
        }}
        className={chartCn.markForeground}
      />
    </Group>
  );
};

export const YRangeMark = memo(YRangeMarkComponent);
