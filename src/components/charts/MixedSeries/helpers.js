import { voronoi } from '@visx/voronoi';
import { localPoint } from '@visx/event';
import { useMemo, useCallback, useState } from 'react';
import { extent, min, max } from 'd3-array';
import { renderValue } from 'src/utils';

/*
 * Utility functions
 */

export function getYExtent(spec, data) {
  const lines = spec.filter((s) => s.type === 'line');
  const areas = spec.filter((s) => s.type === 'area');

  const dataKeys = [
    ...lines.map((l) => l.key),
    ...areas.map((a) => `${a.key}_min`),
    ...areas.map((a) => `${a.key}_max`)
  ];
  const dataMin = min(data, (d) => min(dataKeys, (key) => d[key]));
  const dataMax = max(data, (d) => max(dataKeys, (key) => d[key]));
  const otherMin = min(spec, (d) => min([d.value, d.yMin, d.yMax]));
  const otherMax = max(spec, (d) => max([d.value, d.yMin, d.yMax]));
  return extent([dataMax, dataMin, otherMax, otherMin]);
}

export function useVoronoiTooltip({ margin, width, height, points }) {
  const [tooltip, setTooltip] = useState(null);

  const voronoiLayout = useMemo(
    () =>
      voronoi({
        x: (d) => d.x,
        y: (d) => d.y,
        width,
        height
      })(points),
    [width, height, points]
  );

  const handleMouseMove = useCallback(
    (event) => {
      const position = localPoint(event);
      if (!position) return;
      let closest = voronoiLayout.find(position.x - margin.left, position.y - margin.top, 20);
      if (!closest) setTooltip(null);

      const point = closest;

      if (point && tooltip?.data !== point.data)
        setTooltip({
          left: point.data.x + margin.left,
          top: point.data.y + margin.top,
          x: point.data.x,
          y: point.data.y,
          data: point.data
        });
    },
    [voronoiLayout, tooltip]
  );

  const handleMouseLeave = useCallback(() => {
    setTooltip(null);
  }, []);

  return [tooltip, { handleMouseLeave, handleMouseMove }];
}

export function getLines({ spec, data, xScale, yScale }) {
  return spec
    .filter((s) => s.type === 'line')
    .map((line) => ({
      ...line,
      style: line.style || {},
      data: data
        .map((d) => ({
          value: d[line.key],
          year: d.year,
          y: yScale(d[line.key]),
          x: xScale(d.year)
        }))
        .filter((d) => d.y !== undefined)
    }));
}

export function getAreas({ spec, data, xScale, yScale }) {
  return spec
    .filter((s) => s.type === 'area')
    .map((area) => ({
      ...area,
      style: area.style || {},
      data: data
        .map((d) => ({
          yMin: d[`${area.key}_min`],
          yMax: d[`${area.key}_max`],
          year: d.year,
          y0: yScale(d[`${area.key}_min`]),
          y1: yScale(d[`${area.key}_max`]),
          x: xScale(d.year)
        }))
        .filter((d) => d.y0 !== undefined)
    }));
}

export function getYRanges({ spec, xScale, yScale }) {
  return (
    spec
      // Only get the y ranges that are actually ranges, if not it will be treated as dot
      .filter((s) => s.type === 'y_range' && s.yMin !== s.yMax)
      .filter((s) => s.yMin !== undefined && s.yMax !== undefined)
      .map((range) => {
        const topVal = Math.max(range.yMax, range.yMin);
        const bottomVal = Math.min(range.yMax, range.yMin);
        const left = xScale(range.year);
        const top = yScale(topVal);
        const bottom = yScale(bottomVal);
        const height = bottom - top;

        return {
          ...range,
          top,
          bottom,
          left,
          height
        };
      })
  );
}

export function getDots({ spec, xScale, yScale }) {
  const dots = spec
    .filter((s) => s.type === 'dot' || s.type === 'y_range')
    // Get the y ranges that are not really ranges
    .filter((s) => s.type === 'dot' || s.yMin === s.yMax)
    .map((s) => (s.type === 'dot' ? s : { ...s, value: s.yMin }))
    .filter((s) => s.value !== undefined && s.year !== undefined)
    .map((point) => ({
      ...point,
      x: xScale(point.year),
      y: yScale(point.value)
    }));

  return dots;
}

export function getYRulers({ spec, xScale, yScale }) {
  return spec
    .filter((s) => s.type === 'y_ruler')
    .filter((s) => s.value !== undefined)
    .map((ruler) => ({
      ...ruler,
      y: yScale(ruler.value),
      x1: xScale(ruler.xMin),
      x2: xScale(ruler.xMax)
    }));
}

export function getPoints({ lines = [], areas = [], unit }) {
  const isRelative = unit.id === 'pc';
  return [
    ...lines.reduce(
      (acc, line) =>
        acc.concat(
          line.data.map((d) => ({
            ...line,
            ...d,
            pointKey: line.key,
            formattedValue: renderValue(d.value, { unit, signed: isRelative })
          }))
        ),
      []
    ),
    ...areas.reduce(
      (acc, area) =>
        acc
          .concat(
            area.data.map((d) => ({
              ...area,
              ...d,
              formattedValue: renderValue(d.yMin, { unit, signed: isRelative }),
              value: d.yMin,
              y: d.y0,
              key: area.key,
              pointKey: `${area.key}_min`
            }))
          )
          .concat(
            area.data.map((d) => ({
              ...area,
              ...d,
              formattedValue: renderValue(d.yMax, { unit, signed: isRelative }),
              value: d.yMax,
              y: d.y1,
              key: area.key,
              pointKey: `${area.key}_max`
            }))
          ),
      []
    )
  ];
}

export function getMarkers({ spec, xScale, yScale, unit, markerSize = 10 }) {
  const isRelative = unit.id === 'pc';

  const markers = spec
    .filter((d) => d.marker)
    .map((d) => {
      let value = d.primaryKey ? d[d.primaryKey] : d.value;
      let yValue = d.markerYKey ? d[d.markerYKey] : d.value;

      if (d.type === 'y_range') {
        value = d.primaryKey ? d[d.primaryKey] : d.yMin - d.yMax;
        yValue = d.markerYKey ? d[d.markerYKey] : d.yMax + (d.yMin - d.yMax) / 2;
      }

      return {
        ...d,
        value,
        yValue
      };
    })
    .filter((d) => d.value !== undefined && !isNaN(d.value))
    .map((d) => {
      const origX = xScale && xScale(d.year);
      const origY = yScale && yScale(d.yValue);
      let angle;
      let textAnchor;
      let verticalAnchor;

      const right = d.marker.includes('right');
      const left = d.marker.includes('left');
      const top = d.marker.includes('top');
      const bottom = d.marker.includes('bottom');

      if (right) {
        angle = 0;
      }
      if (left) {
        angle = Math.PI;
        textAnchor = 'end';
        verticalAnchor = 'middle';
      }
      if (bottom) {
        angle = Math.PI / 2;
        textAnchor = 'middle';
        verticalAnchor = 'start';
      }
      if (top) {
        angle = (3 * Math.PI) / 2;
        textAnchor = 'middle';
        verticalAnchor = 'end';
      }
      if (right && bottom) {
        angle = Math.PI / 4;
        textAnchor = 'start';
        verticalAnchor = 'start';
      }
      if (left && bottom) {
        angle = (3 * Math.PI) / 4;
        textAnchor = 'end';
        verticalAnchor = 'start';
      }
      if (left && top) {
        angle = (5 * Math.PI) / 4;
        textAnchor = 'end';
        verticalAnchor = 'end';
      }
      if (right && top) {
        angle = (7 * Math.PI) / 4;
        textAnchor = 'start';
        verticalAnchor = 'end';
      }

      const xOffset = Math.cos(angle) * markerSize * 10;
      const yOffset = Math.sin(angle) * markerSize * 10;

      return {
        ...d,
        targetX: origX,
        targetY: origY,
        x: origX + xOffset,
        y: origY + yOffset,
        textAnchor,
        verticalAnchor,
        formattedValue:
          d.primaryKey === 'year' ? d.value : renderValue(d.value, { unit, signed: isRelative })
      };
    });
  return markers;
}
