import { Fragment, useState, useEffect, useMemo, useCallback } from 'react';
import cn from './style.module.scss';
import cx from 'classnames';

import { forceRectCollide, forceBox } from 'src/utils/forceRect';
import { forceSimulation, forceManyBody, forceX, forceY } from 'd3-force';

import { SwoopyArrow, SwoopyText } from 'src/components/Swoopy';

const useMarkerLabels = ({
  markers,
  dots,
  yRanges,
  width,
  height,
  margin,
  wrapperClassName,
  labelClassName,
  textClassName,
  angle
}) => {
  const [positionedItems, setPositionedItems] = useState([]);
  const [boxes, setBoxes] = useState([]);
  const [labelContainerNode, setlabelContainerNode] = useState(null);
  const boxPadding = 4;

  const labelContainerRef = useCallback((node) => {
    setlabelContainerNode(node);
  }, []);

  const simulationPoints = useMemo(() => {
    return [
      ...markers.map((d) => ({
        ...d,
        isLabel: true
      })),
      ...(dots ? dots.map((d) => ({ ...d, width: 40, height: 40, fx: d.x, fy: d.y })) : []),
      ...(yRanges
        ? yRanges.map((d) => ({ fx: d.left, fy: d.top, width: 40, height: d.height }))
        : [])
    ];
  }, [markers, dots, yRanges]);

  useEffect(() => {
    if (!labelContainerNode) return;
    // Get all rendered labels
    const elements = Array.from(labelContainerNode.querySelectorAll(`.${labelClassName} `));

    const resizeObserver = new ResizeObserver((items) => {
      const labelItems = simulationPoints.filter((d) => d.isLabel);

      // Set new dimensions from resize observer
      setBoxes((boxes) => {
        const newBoxes = [...boxes];
        for (let entry of items) {
          // We need some way to match the rendered labels to the data. Usually in react this
          // happens simply within the datastructure. Here however we need to match an array of
          // elements to an array of data, so we use the index
          const index = parseInt(entry.target.getAttribute('data-id'));
          const { width, height } = entry.contentRect;

          const { x, y, isLabel } = labelItems[index];

          // x/y positions are assumed to be the center of the label. rectangle forces are assuming
          // x/y to be top left corner of rectangle. To compensate, we subtract half of with/height
          newBoxes[index] = {
            isLabel,
            x: x - width / 2 - boxPadding,
            y: y - height / 2 - boxPadding,
            width: width + boxPadding * 2,
            height: height + boxPadding * 2
          };
        }
        return newBoxes;
      });
    });

    // Observ resizing of labels
    elements.forEach((el) => resizeObserver.observe(el));

    return () => {
      resizeObserver.disconnect();
    };
  }, [simulationPoints]);

  useEffect(() => {
    // Combine labels and other items so labels don't overlap important
    // elements of the graph either
    const nonLabelItems = simulationPoints.filter((d) => !d.isLabel);
    const placedItems = [...boxes, ...nonLabelItems];

    var collisionForce = forceRectCollide()
      .size(function (d) {
        return [d.width, d.height];
      })
      .strength(6);

    var boxForce = forceBox()
      .bounds([
        [0, 0],
        [width, height]
      ])
      .size(function (d) {
        return [d.width, d.height];
      });

    const simulation = forceSimulation(placedItems)
      .force(
        'x',
        forceX()
          .x((d) => d.x - boxPadding)
          .strength(0.3)
      )
      .force(
        'y',
        forceY()
          .y((d) => d.y - boxPadding)
          .strength(0.3)
      )
      .force('charge', forceManyBody().strength(-20))
      .force('box', boxForce)
      .force('collision', collisionForce);

    simulation.tick(50);
    simulation.stop();
    // Only return the labels since the rest is only used for the simulation
    setPositionedItems(placedItems.filter((d) => d.isLabel));
  }, [simulationPoints, boxes]);

  const renderedItems = simulationPoints
    .filter((d) => d.isLabel)
    .map((item, i) => {
      const dimensions = positionedItems[i] || { width: 0, height: 0 };

      return {
        ...item,
        ...dimensions
      };
    });

  const lines = (
    <g className={cx(cn.markerLabels, wrapperClassName)}>
      {renderedItems.map((marker, i) => (
        <Fragment key={i}>
          <SwoopyArrow
            fromWidth={marker.width}
            fromHeight={marker.height}
            to={[marker.targetX, marker.targetY]}
            from={[marker.x, marker.y]}
            hide={marker.hide}
            angle={angle}
            id={marker.id}
          />
        </Fragment>
      ))}
    </g>
  );

  const labels = (
    <div
      ref={labelContainerRef}
      style={{ top: margin.top, left: margin.left, width, height }}
      className={cx(cn.markerLabels, wrapperClassName)}>
      {renderedItems.map((marker, i) => {
        return (
          <SwoopyText
            key={i}
            id={i}
            from={[marker.x, marker.y]}
            to={[marker.targetX, marker.targetY]}
            width={marker.width}
            verticalAnchor={marker.verticalAnchor}
            textAnchor={marker.textAnchor}
            text={marker.name}
            className={labelClassName}
            textClassName={textClassName}
            hide={marker.hide}
            value={marker.formattedValue}
          />
        );
      })}
    </div>
  );

  return { labels, lines };
};

export default useMarkerLabels;
