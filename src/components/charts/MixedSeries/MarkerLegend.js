import useScrollWrapper from 'src/utils/useScrollWrapper';
import cn from './style.module.scss';
import cx from 'classnames';

const MarkerLegend = ({ markers, label }) => {
  const [wrapperRef, overflow] = useScrollWrapper();

  return (
    <div className={cn.markerLegendWrapper}>
      <div className={cn.markerLegendLabel}>{label}</div>
      <div
        className={cx(
          cn.markerLegendInnerWrapper,
          overflow[0] && cn.overflowLeft,
          overflow[1] && cn.overflowRight
        )}>
        <div ref={wrapperRef} className={cn.markerLegendScrollWrapper}>
          <ol className={cn.markerLegend}>
            {markers.map((marker, i) => (
              <li key={i} className={cn.markerLegendItem} style={{ '--color': marker.color }}>
                <span className={cn.markerLegendItemNumber}>{i + 1}</span>
                <div className={cn.markerLegendContent}>
                  <div className={cn.markerLegendItemName}>{marker.name}</div>
                  <div className={cn.markerLegendItemValue}>{marker.formattedValue}</div>
                </div>
              </li>
            ))}
          </ol>
        </div>
      </div>
    </div>
  );
};

export default MarkerLegend;
