import { Fragment, useState, useCallback, useMemo, memo } from 'react';
import { Group } from '@visx/group';
import chartCn from '../style.module.scss';
import cn from './style.module.scss';
import cx from 'classnames';
import { renderValue } from 'src/utils';

import { forceRectCollide, forceBox } from 'src/utils/forceRect';

import { forceSimulation, forceX, forceY, forceCollide, forceManyBody } from 'd3-force';

import { SwoopyArrow, SwoopyText } from 'src/components/Swoopy';

const MarkerLabels = ({ markers, dots, yRanges, width, height, children }) => {
  const [markerLabels, setMarkerLabels] = useState([]);

  const labelWidth = 60;

  const simulationPoints = useMemo(() => {
    return [
      ...markers.map((d) => ({
        ...d,
        isLabel: true
        /* fy: d.marker.includes('right') || d.marker.includes('left') ? d.y : undefined,
        fx: d.marker.includes('top') || d.marker.includes('bottom') ? d.x : undefined */
      })),
      ...dots.map((d) => ({ ...d, fx: d.x, fy: d.y })),
      ...yRanges.reduce(
        (acc, d) => [
          ...acc,
          { fx: d.left, fy: d.top },
          { fx: d.left, fy: d.bottom },
          { fx: d.left, fy: d.top + d.height / 2 }
        ],
        []
      )
    ];
  }, [markers, dots, yRanges]);

  // const simulationLinks = useMemo(() => {

  // }, [markers, dots, yRanges])

  const simulation = useMemo(() => {
    const simulation = forceSimulation(simulationPoints)
      .force(
        'x',
        forceX()
          .x((d) => d.x)
          .strength(0.03)
      )
      .force(
        'y',
        forceY()
          .y((d) => d.y)
          .strength(0.03)
      )
      .force(
        'charge',
        forceManyBody().strength((d) => (d.isLabel ? -10 : -20))
      )
      .force(
        'collide',
        forceCollide()
          .radius((d) => (d.isLabel ? labelWidth / 2 : 10))
          .iterations(5)
      )
      .force('constrain', function constrain() {
        for (let i = 0, n = simulationPoints.length, node; i < n; ++i) {
          node = simulationPoints[i];
          node.x = Math.max(Math.min(width - labelWidth, node.x), labelWidth);
          node.y = Math.max(Math.min(height - labelWidth, node.y), labelWidth);
        }
      })
      .on('tick', handleTick);

    function handleTick() {
      if (simulation.alpha() > 0.05) {
        setMarkerLabels(simulationPoints.filter((d) => d.isLabel));
      } else {
        simulation.stop();
      }
    }

    // console.time('sim');
    // sim.tick(30);
    // console.timeEnd('sim');
    return simulation;
  }, [simulationPoints]);

  return (
    <>
      {markerLabels.map((marker, i) => (
        <Fragment key={i}>
          <circle
            filter="url(#shadow)"
            cx={marker.x}
            cy={marker.y}
            r={labelWidth}
            className={cn.markerDebug}
          />
          <SwoopyArrow to={[marker.targetX, marker.targetY]} from={[marker.x, marker.y]} />
        </Fragment>
      ))}
      {children}
      {markerLabels.map((marker, i) => (
        <SwoopyText
          key={i}
          width={labelWidth * 2}
          from={[marker.x, marker.y]}
          to={[marker.targetX, marker.targetY]}
          textOffset={[3, 3]}
          verticalAnchor={marker.verticalAnchor}
          textAnchor={marker.textAnchor}
          text={marker.name.slice(0, 25)}
        />
      ))}
    </>
  );
};

export default memo(MarkerLabels);
