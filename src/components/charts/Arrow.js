const Arrow = ({ x = 0, y = 0, headSize = 5, length = 10, rotate = 0, color, className }) => {
  return (
    <g transform={`translate(${x}, ${y}) rotate(${rotate})`}>
      <line
        y1={headSize / 2}
        y2={headSize / 2}
        x2={length}
        className={className}
        style={{ stroke: 'black', strokeWidth: 2 }}
      />
      <path
        d={`M ${length - headSize / 2}, 0 L ${length}, ${headSize / 2} L ${
          length - headSize / 2
        } ${headSize} Z`}
        style={{ fill: 'black' }}
      />
    </g>
  );
};
export default Arrow;
