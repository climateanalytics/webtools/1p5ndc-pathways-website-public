/*
 * Get the y tick from the spec that should be highlighted. Currently only works
 * for one single y tick
 */
export function getYTicks(spec, scale) {
  const specTick = spec.find((s) => s.type === 'tick')?.value;
  const scaleTicks = scale.copy().ticks(5);
  const ticks = scaleTicks.reduce((acc, tick, i) => {
    const prevTick = scaleTicks[i - 1];
    const nextTick = scaleTicks[i + 1];
    // get center between previous tick and current tick
    const dPrev = tick - (tick - prevTick) / 2;
    // get center between next tick and current tick
    const dNext = nextTick - (nextTick - tick) / 2;
    // Push the fixed tick if it is bigger than center to prev tick
    // and smaller than center to next tick

    if (specTick >= dPrev && specTick < dNext) acc.push(specTick);
    // ...or if it is the first tick and it is smaller than the first regular tick
    else if (i === 0 && specTick < dNext) acc.push(specTick);
    else if (i === scaleTicks.length - 1 && specTick >= dPrev) acc.push(specTick);
    else acc.push(tick);
    return acc;
  }, []);

  return { ticks, specTick };
}
