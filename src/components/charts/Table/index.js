import withGraphFrame from '../withGraphFrame';
import { memo, useCallback } from 'react';
import { renderRange, renderValue } from 'src/utils';
import cn from './style.module.scss';
import cx from 'classnames';
import buttonCn from 'src/style/button.module.scss';
import useScrollWrapper from 'src/utils/useScrollWrapper';

const Table = ({ columns, rows, wrapperClassName }) => {
  const [wrapperRef, overflow, hasInteracted] = useScrollWrapper();

  const scrollToEnd = useCallback(() => {
    if (!wrapperRef.current) return;
    const wrapperEl = wrapperRef.current;
    wrapperEl.scrollLeft = wrapperEl.clientWidth;
  }, [wrapperRef]);

  return (
    <div
      className={cx(
        cn.wrapper,
        wrapperClassName,
        overflow[0] && cn.overflowLeft,
        overflow[1] && cn.overflowRight
      )}>
      {overflow[1] && !hasInteracted && (
        <button
          onClick={scrollToEnd}
          className={cx(buttonCn.icon, buttonCn.cta, buttonCn.raised, cn.scrollCta)}>
          <span className="icon-arrow-right" />
        </button>
      )}
      <div ref={wrapperRef} className={cn.innerWrapper}>
        <table className={cn.table}>
          <thead>
            <tr className={cx(cn.row, cn.head)}>
              {columns.map((column, i) => (
                <th className={cn.cell} key={i}>
                  <div className={cn.mainValue}>{column.name}</div>
                  {column.secondaryName && (
                    <div className={cn.secondaryValue}>{column.secondaryName}</div>
                  )}
                </th>
              ))}
            </tr>
          </thead>
          <tbody className={cn.body}>
            {rows.map((row, rowIndex) => {
              return (
                <tr key={rowIndex} className={cx(cn.row, row.secondary && cn.secondary)}>
                  {row.map((cell, colIndex) => {
                    if (colIndex === 0) {
                      return (
                        <td key={colIndex} className={cn.cell}>
                          {cell.name && <div className={cn.mainValue}>{cell.name}</div>}
                          {cell.secondaryName && (
                            <div className={cn.secondaryValue}>{cell.secondaryName}</div>
                          )}
                        </td>
                      );
                    }

                    if (
                      cell.value === undefined &&
                      cell.min === undefined &&
                      cell.max === undefined
                    )
                      return <td key={colIndex} className={cn.cell}></td>;
                    else {
                      const value =
                        cell.value !== undefined &&
                        renderValue(cell.value, { unit: cell.unit, addUnit: !!cell.addUnit });
                      const range =
                        cell.min !== undefined &&
                        renderRange([cell.min, cell.max], {
                          unit: cell.unit,
                          addUnit: !!cell.addUnit
                        });
                      return (
                        <td key={colIndex} className={cn.cell}>
                          {(value || range) && <div className={cn.mainValue}>{value || range}</div>}
                          {range && value && <div className={cn.secondaryValue}>{range}</div>}
                        </td>
                      );
                    }
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};

export default withGraphFrame(memo(Table));
