import { useEffect, useMemo, useRef, useState } from 'react';
import withGraphFrame from '../withGraphFrame';
import chartCn from '../style.module.scss';
import cn from './style.module.scss';
import cx from 'classnames';
import useDimensions from 'src/utils/useDimensions';
import { renderValue, renderRange, hexToRgba, uniq } from 'src/utils';
import { scaleLinear } from 'd3-scale';
import { extent, max, range } from 'd3-array';

const padding = 7; // Min padding between boxes
const lineDistance = 22;
const margin = { left: 0, right: 0 };

const Side = ({ items, sign, width, scale, color }) => {
  if (!items.length) return null;
  const wrapperRef = useRef(null);
  const index = sign === 1 ? 0 : 1;
  const invertedIndex = sign === 1 ? 1 : 0;

  const [boxes, setBoxes] = useState([]);

  // Sort items by year and add formatted values etc
  const processedItems = useMemo(() => {
    return items.map((d) => ({
      ...d,
      y: 0,
      yearLabel: d.years && d.years.join(' - '),
      formattedValue: d.values
        ? renderRange(d.values, { unit: d.unit })
        : renderValue(d.value, { unit: d.unit })
    }));
  }, [items]);

  useEffect(() => {
    const elements = Array.from(wrapperRef.current.querySelectorAll(`.${cn.step} `));

    const resizeObserver = new ResizeObserver((items) => {
      setBoxes((boxes) => {
        const newBoxes = [...boxes];
        for (let entry of items) {
          const index = parseInt(entry.target.getAttribute('data-id'));
          const box = entry.contentRect;

          const item = processedItems[index];
          // centerX is where the year is positioned
          const centerX = Math.floor(scale(item.year || item.years[0]));
          // x might shift if the item is close to the edge
          let x = centerX;
          x = x + box.width / 2 > window.innerWidth ? window.innerWidth - box.width / 2 : x;
          x = x - box.width / 2 < 0 ? box.width / 2 : x;
          const centerOffset = box.width / 2 + (centerX - x);

          newBoxes[index] = {
            ...item,
            x,
            index, // Add original index to box so we can later match them again
            centerX,
            centerOffset,
            left: Math.floor(x - box.width / 2),
            right: x + box.width / 2,
            width: box.width,
            height: box.height
          };
        }
        return newBoxes;
      });
    });

    elements.forEach((el) => resizeObserver.observe(el));

    return () => {
      resizeObserver.disconnect();
    };
  }, [setBoxes, processedItems, scale]);

  const getPositionedItems = () => {
    let rows = [];

    // Loop through newDimensions and find the first free row space
    boxes.forEach((box) => {
      const freeRow = rows.find((row) => {
        const lastItem = row.items[row.items.length - 1];
        if (box.left - padding > lastItem.right) return row;
      });
      if (freeRow) {
        freeRow.height = Math.max(box.height, freeRow.height);
        freeRow.items.push(box);
      } else {
        rows.push({
          height: box.height,
          items: [box]
        });
      }
    });

    // Assign a y value to each item according to the assigned row and the heights of previous rows
    rows.forEach((row, i) => {
      const prevRow = rows[i - 1];
      const y = i === 0 ? 0 : prevRow.height + padding + prevRow.y;
      row.y = y;
      row.items.forEach((el) => {
        el.y = y;
        el.rowIndex = i;
      });
    });

    // Flatten and sort rows by index so they will match the data again
    return rows.reduce((acc, row) => [...acc, ...row.items], []).sort((a, b) => a.index - b.index);
  };

  // Calculate positions from new boxes and match them to the items
  const positionedItems = getPositionedItems();
  const renderedItems = processedItems.map((item, i) => {
    const dimensions = positionedItems[i] || { top: 0, y: 0, left: 0, height: 0, rowIndex: 0 };
    return {
      ...item,
      ...dimensions
    };
  });

  const height = max(renderedItems, (d) => d.y + d.height);

  return (
    <ol
      ref={wrapperRef}
      style={{ width, height, left: margin.left }}
      className={cx(cn.sideWrapper)}>
      {renderedItems.map((step, itemIndex) => {
        const lineLength = step.y + lineDistance;
        const lineTop = step.height * index + -lineLength * invertedIndex;
        return (
          <li
            key={itemIndex}
            className={cn.step}
            data-id={itemIndex}
            style={{
              left: step.left,
              top: step.y * sign + (height - step.height) * index,
              zIndex: 10 - step.rowIndex,
              '--sign': sign,
              '--color': color,
              '--color-weaker': hexToRgba(color, 0.2)
            }}>
            <span className={cn.stepInner}>
              {step.formattedValue} {step.name}
              {step.yearLabel && (
                <span className={cn.stepSecondary}>&thinsp;({step.yearLabel})</span>
              )}
            </span>
            <svg
              height={lineLength}
              width={2}
              style={{ top: lineTop, left: step.centerOffset }}
              className={cn.connectorCanvas}>
              <line
                x1={0.5}
                x2={0.5}
                y2={lineLength}
                className={cn.connector}
                style={{ stroke: color }}
              />
              <rect
                style={{ fill: color }}
                width={3}
                height={3}
                x={-1}
                y={(lineLength - 1) * index}
              />
            </svg>
          </li>
        );
      })}
    </ol>
  );
};

const Axis = ({ ticks, scale }) => {
  return (
    <div className={cn.axis} style={{ left: margin.left, width: scale.range()[1] }}>
      {ticks.map((tick) => {
        return (
          <span
            key={tick.year}
            style={{ left: scale(tick.year) }}
            className={cx(
              cn.tick,
              tick.major && cn.major,
              tick.minor && cn.minor,
              tick.showLabel && cn.hasLabel
            )}>
            {tick.showLabel && tick.year}
          </span>
        );
      })}
    </div>
  );
};

const Timeline = ({ data }) => {
  const bottom = data[0];
  const top = data[1];
  const hasBottom = !!bottom.data.length;
  const [wrapperProps, { width }] = useDimensions();
  const chartWidth = width - margin.left - margin.right;

  const { yearExtent, yearTicks } = useMemo(() => {
    const dataYears = [...data[0].data, ...data[1].data]
      .map((step) => (step.years ? step.years[0] : step.year))
      .filter(uniq)
      .sort((a, b) => a - b);

    const yearExtent = extent(dataYears);
    yearExtent[0] = Math.floor((yearExtent[0] - 1) / 10) * 10 - 3;
    yearExtent[1] = Math.max(Math.ceil((yearExtent[1] + 1) / 10) * 10 + 3, 2055);
    const yearTicks = range(yearExtent[0], yearExtent[1] + 1)
      .map((year) => {
        const isInData = dataYears.includes(year);
        const showLabel = isInData || year % 10 === 0;
        return {
          year,
          minor: year % 5 === 0,
          major: year % 10 === 0,
          showLabel
        };
      })
      .map((tick, i, ticks) => {
        if (i === 0) return tick;
        if (!ticks[i - 1].showLabel && tick.showLabel) return tick;
        return { ...tick, showLabel: false };
      });

    return { yearTicks, yearExtent };
  }, [data]);

  const scale = useMemo(
    () => scaleLinear().domain(yearExtent).range([0, chartWidth]),
    [yearExtent, chartWidth]
  );

  return (
    <>
      {hasBottom && (
        <div className={cn.sideNames}>
          <div
            style={{ '--color': top.color, '--color-weaker': hexToRgba(top.color, 0.2) }}
            className={cx(cn.sideName, cn.top)}>
            {top.name}
          </div>
          <div
            style={{ '--color': bottom.color, '--color-weaker': hexToRgba(bottom.color, 0.2) }}
            className={cx(cn.sideName, cn.bottom)}>
            {bottom.name}
          </div>
        </div>
      )}
      <div {...wrapperProps} className={cx(chartCn.chartWrapper, cn.wrapper)}>
        {chartWidth > 0 && (
          <>
            <Side sign={-1} scale={scale} width={chartWidth} items={top.data} color={top.color} />
            <Axis ticks={yearTicks} scale={scale} />
            {hasBottom && (
              <>
                <Side
                  sign={1}
                  scale={scale}
                  width={chartWidth}
                  items={bottom.data}
                  color={bottom.color}
                />
              </>
            )}
          </>
        )}
      </div>
    </>
  );
};

export default withGraphFrame(Timeline);
