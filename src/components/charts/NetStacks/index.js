import withGraphFrame from '../withGraphFrame';
import useDimensions from 'src/utils/useDimensions';
import { renderValue, reduceBy, dynamicallyRoundNumber, uniqify } from 'src/utils';
import { useMemo, memo, Fragment, useState } from 'react';
import { max as d3Max, min as d3Min, sum as d3Sum } from 'd3-array';
import { stack as d3Stack } from 'd3-shape';
import chartCn from '../style.module.scss';
import cn from './style.module.scss';
import cx from 'classnames';
import { scaleLinear, scaleBand } from '@visx/scale';
import { Group } from '@visx/group';
import { XAxis, YAxis, HorizontalGrid } from '../axes';
import MixedLegend from '../MixedLegend';
import Hatching from '../Hatching';

const margin = { top: 10, right: 0, bottom: 25, left: 0 };

function getStack({ data, segments, width, height, padding = 0.3 }) {
  const segmentsById = reduceBy(segments);
  let positiveKeys = [];
  let negativeKeys = [];

  const splitData = data.map((d) => {
    const positive = {};
    const negative = {};

    segments.forEach(({ id }) => {
      if (d[id] >= 0) positive[id] = d[id];
    });
    segments.forEach(({ id }) => {
      if (d[id] < 0) negative[id] = d[id];
    });
    const positiveTotal = d3Sum(Object.values(positive)) || 0;
    const negativeTotal = d3Sum(Object.values(negative)) || 0;
    positiveKeys = uniqify([...positiveKeys, ...Object.keys(positive)]);
    negativeKeys = uniqify([...negativeKeys, ...Object.keys(negative)]);

    const net = positiveTotal + negativeTotal;
    return { positive, positiveTotal, negativeTotal, negative, net, year: d.year };
  });

  const positive = d3Stack().keys(positiveKeys)(splitData.map((d) => d.positive));
  const negative = d3Stack().keys(negativeKeys)(splitData.map((d) => d.negative));
  const max = d3Max(splitData, (d) => d.positiveTotal);
  const min = d3Min(splitData, (d) => d.negativeTotal);

  const yScale = scaleLinear().domain([min, max]).range([height, 0]);
  const xScale = scaleBand()
    .domain(data.map((d) => d.year))
    .range([0, width])
    .paddingInner(padding);

  const barWidth = xScale.bandwidth();
  const maxTipHeight = barWidth * 0.4;
  const centerX = barWidth / 2;

  const stacks = splitData.map((d, i) => {
    const pos = positive.map((p) => p[i]);
    const neg = negative.map((n) => n[i]);

    return {
      ...d,
      x: xScale(d.year),
      width: barWidth,
      positive: pos
        .map((p, j) => {
          if (isNaN(p[0]) || isNaN(p[1])) return;
          const key = positive[j].key;
          const value = p.data[key];
          const y = Math.round(yScale(p[1]));
          const height = Math.round(yScale(p[0]) - y);
          const hasTip = true; //j === pos.length - 1 && !!value; -> needs to be changed if multiple segments per stack should be possible
          const tipHeight = Math.round(Math.min(maxTipHeight, height));
          const barHeight = hasTip ? height - tipHeight : height;
          const tipBase = Math.round(y + tipHeight);

          return {
            value,
            barY: hasTip ? y + tipHeight : y,
            width: barWidth,
            barHeight: barHeight || 0,
            tipHeight,
            tipTop: tipBase - tipHeight - 4,
            hasTip,
            tipD: `M${centerX}, ${y}L${barWidth}, ${tipBase}L${0}, ${tipBase}Z`,
            ...segmentsById[key],
            year: d.year
          };
        })
        .filter((d) => d),
      negative: neg
        .map((p, j) => {
          if (isNaN(p[0]) || isNaN(p[1])) return;
          const key = negative[j].key;
          const value = p.data[key];
          const y = Math.round(yScale(p[0]));
          const height = Math.round(yScale(p[1]) - y);
          const hasTip = j === neg.length - 1 && !!value;
          const tipHeight = Math.round(Math.min(maxTipHeight, height));
          let barHeight = hasTip ? height - tipHeight : height;
          const tipBase = Math.round(y + barHeight);
          return {
            value,
            barY: y,
            width: barWidth,
            barHeight: barHeight || 0,
            tipHeight,
            tipTop: tipBase + tipHeight + 4,
            hasTip,
            tipD: `M${centerX}, ${
              y + barHeight + tipHeight
            }L${barWidth}, ${tipBase}L${0}, ${tipBase}Z`,
            ...segmentsById[key],
            year: d.year
          };
        })
        .filter((d) => d),
      positiveTotal: {
        y: yScale(d.positiveTotal) - 3,
        value: d.positiveTotal
      },
      negativeTotal: {
        y: yScale(d.negativeTotal) + 3,
        value: d.negativeTotal
      },
      net: {
        y: yScale(d.net),
        value: d.net
      }
    };
  });

  return { stacks, xScale, yScale };
}

const NetStacks = ({ segments, data, id, unit, chartClassName }) => {
  const [highlighted, setHighlighted] = useState(null);
  const [wrapperProps, { width, height }] = useDimensions({ height: 500, width: 100 });
  const chartWidth = width - margin.left - margin.right;
  const chartHeight = height - margin.top - margin.bottom;
  const padding = Math.min(data.length * 0.1, 0.6);

  const { stacks, xScale, yScale } = useMemo(
    () =>
      getStack({
        data,
        segments,
        width: chartWidth,
        height: chartHeight,
        padding
      }),
    [data, segments, chartWidth, chartHeight]
  );

  const yTicks = yScale.ticks(5);

  const legendItems = useMemo(() => [...segments.map((s) => ({ ...s, type: 'bar' }))], [segments]);

  const hatchingId = `${id}-hatching`;
  const netColor = segments.find((d) => d.id.includes('net_'))?.color;

  return (
    <>
      <div {...wrapperProps} className={cx(chartClassName, chartCn.chartWrapper)}>
        <svg width={width} height={height} className={chartCn.svg}>
          <Hatching id={hatchingId} />
          <Group top={margin.top} left={margin.left}>
            <XAxis scale={xScale} top={chartHeight + 1} scaleType="band" />
            <YAxis
              scale={yScale}
              ticks={yTicks}
              renderTick={renderValue}
              unit={unit}
              addUnit={false}
            />
            <HorizontalGrid scale={yScale} ticks={yTicks} width={chartWidth} />
            {stacks.map((stack, i) => {
              const dimmed = highlighted !== null && i !== highlighted;
              return (
                <Fragment key={i}>
                  <Group left={stack.x} className={cx(cn.barGroup, dimmed && cn.dimmed)}>
                    {[...stack.positive, ...stack.negative].map((segment, j) => {
                      return (
                        <Fragment key={`${i}-${j}`}>
                          {segment.hasTip && (
                            <path d={segment.tipD} style={{ fill: segment.color }} />
                          )}
                          {segment.hatching && (
                            <path d={segment.tipD} fill={`url(#${hatchingId})`} />
                          )}
                          <rect
                            className={cn.bar}
                            width={segment.width}
                            height={Math.max(segment.barHeight, 0)}
                            y={segment.barY}
                            style={{ fill: segment.color }}
                          />
                          {segment.hatching && (
                            <rect
                              className={cn.bar}
                              width={segment.width}
                              height={Math.max(segment.barHeight, 0)}
                              y={segment.barY}
                              fill={`url(#${hatchingId})`}
                            />
                          )}
                        </Fragment>
                      );
                    })}
                    <circle
                      className={cn.netDot}
                      cy={stack.net.y}
                      cx={stack.width / 2}
                      style={{ fill: netColor }}
                      r={Math.min(stack.width * 0.35, 5.5)}
                    />
                    {stack.positive[0].hatching && (
                      <circle
                        className={cn.netDot}
                        cy={stack.net.y}
                        cx={stack.width / 2}
                        fill={`url(#${hatchingId})`}
                        r={Math.min(stack.width * 0.35, 5.5)}
                      />
                    )}
                  </Group>
                  <rect
                    className={cn.hoverArea}
                    x={stack.x}
                    width={stack.width}
                    height={chartHeight}
                    onMouseEnter={() => setHighlighted(i)}
                    onMouseLeave={() => setHighlighted(null)}
                  />
                </Fragment>
              );
            })}
          </Group>
        </svg>
        <div>
          {stacks.map((stack, i) => {
            const visible = i === highlighted;
            return (
              <div
                key={i}
                className={cx(cn.labels, visible && cn.visible)}
                style={{ transform: `translate(${margin.left}px, ${margin.top}px)` }}>
                <span
                  className={cn.label}
                  style={{
                    left: `${stack.x + stack.width / 2}px`,
                    top: `${stack.positiveTotal.y}px`,
                    transform: 'translate(-50%, -130%)',
                    textAlign: 'center'
                  }}>
                  {dynamicallyRoundNumber(stack.positiveTotal.value)}
                </span>
                <span
                  className={cn.label}
                  style={{
                    left: `${stack.x + stack.width / 2}px`,
                    top: `${stack.negativeTotal.y}px`,
                    transform: 'translate(-50%, 0)',
                    textAlign: 'center'
                  }}>
                  {dynamicallyRoundNumber(stack.negativeTotal.value)}
                </span>
                <span
                  className={cx(cn.label, cn.primary)}
                  style={{
                    left: `${stack.x + stack.width + 4}px`,
                    top: `${stack.net.y}px`,
                    transform: 'translateY(-50%)',
                    textAlign: 'left',
                    color: netColor
                  }}>
                  {dynamicallyRoundNumber(stack.net.value)}
                </span>
              </div>
            );
          })}
        </div>
      </div>
      <MixedLegend items={legendItems} />
    </>
  );
};

export default withGraphFrame(memo(NetStacks));
