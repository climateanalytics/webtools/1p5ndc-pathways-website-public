const RoundedRect = ({ width, height, x = 0, y = 0, r = [0, 0, 0, 0], ...props }) => {
  const d = `
    M ${x + r[0]}, ${y}
    L ${x + width - r[1]}, ${y}
    A ${r[1]} ${r[1]} 0 0 1 ${x + width}, ${y + r[1]}
    L ${x + width}, ${y + height - r[2]}
    A ${r[2]} ${r[2]} 0 0 1 ${x + width - r[2]}, ${y + height}
    L ${x + r[3]}, ${y + height}
    A ${r[3]} ${r[3]} 0 0 1 ${x}, ${y + height - r[3]}
    L ${x}, ${y + r[0]}
    A ${r[0]} ${r[0]} 0 0 1 ${x + r[0]}, ${y}
  `;
  return <path d={d} {...props} />;
};

export default RoundedRect;
