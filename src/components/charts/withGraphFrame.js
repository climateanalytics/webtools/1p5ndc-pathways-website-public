import Template, { renderTemplate } from 'src/components/content/Template';
import Link from 'next/link';
import cn from './style.module.scss';
import cx from 'classnames';
import { useMemo, memo } from 'react';
import SimpleSelect from 'src/components/controls/SimpleSelect';
import { useSiteContent } from 'src/data/siteContent';
import allControls from 'src/components/controls';
import { createUrl, groupBy } from 'src/utils';
import { withErrorBoundary } from 'src/components/ErrorBoundary';
import btnCn from 'src/style/button.module.scss';

const InfoDropdownComponent = ({
  id,
  caption,
  methodology,
  siteContent,
  description,
  meta,
  references: rawReferences
}) => {
  const references = rawReferences.map((ref) => meta.referencesById[ref.id]);

  const selectItems = [
    {
      groupId: 'caption',
      caption
    },
    ...(methodology?.map((d) => ({ ...d, groupId: 'methodology' })) || []),
    ...(references?.map((d) => ({ ...d, groupId: 'references' })) || [])
  ];

  return (
    <SimpleSelect
      id={`${id}-info-select`}
      items={selectItems}
      wrapperModifiers={[cn.metaButton]}
      buttonModifiers={['bare']}
      trayTag="div"
      description={description}
      size="s"
      buttonIcon="info-circled"
      buttonLabel="Info"
      showToggleIcon={false}>
      {({ items, getItemProps }) => {
        const { caption, methodology, references } = groupBy(items, 'groupId');

        return (
          <div className={cn.infoTray}>
            {caption[0] && (
              <>
                <h5 className={cn.infoTraySectionTitle}>{siteContent.chart_description_label}</h5>
                <p
                  className={cx(cn.infoTraySection)}
                  {...getItemProps({ item: caption[0], index: 0 })}>
                  {caption[0].caption}
                </p>
              </>
            )}
            {methodology && (
              <div className={cn.infoTraySection}>
                {methodology.length > 1 && (
                  <h5 className={cn.infoTraySectionTitle}>{siteContent.methodology_label}</h5>
                )}
                {methodology.map((item, i) => (
                  <div key={i} className={cn.infoTrayItem}>
                    <Link href={`/methodology#${item.id}`}>
                      <a
                        className={cx(btnCn.s, btnCn.bare, btnCn.iconBefore)}
                        {...getItemProps({ item, index: items.indexOf(item) })}>
                        <span className="icon-file-text" />
                        {methodology.length > 1 ? item.name : siteContent.methodology_label}
                      </a>
                    </Link>
                  </div>
                ))}
              </div>
            )}
            {references && (
              <div className={cn.infoTraySection}>
                <h5 className={cn.infoTraySectionTitle}>{siteContent.references_label}</h5>
                {references.map((item, i) => (
                  <div key={i} className={cn.infoTrayItem}>
                    <Link href={`/methodology#${item.id}`}>
                      <a {...getItemProps({ item, index: items.indexOf(item) })}>{item.name}</a>
                    </Link>
                  </div>
                ))}
              </div>
            )}
          </div>
        );
      }}
    </SimpleSelect>
  );
};

const InfoDropdown = memo(InfoDropdownComponent);

const DownloadDropdownComponent = ({
  imageUrl,
  dataUrl,
  siteContent,
  description,
  fileName,
  id,
  imageWidth,
  allowDataDownload
}) => {
  const encodedImageUrl = encodeURIComponent(imageUrl);
  const pngUrl = createUrl({
    url: process.env.screenshotUrl,
    params: { fileName, url: imageUrl, width: imageWidth, format: 'png' }
  });
  const pdfUrl = createUrl({
    url: process.env.screenshotUrl,
    params: { fileName, url: imageUrl, width: imageWidth, format: 'pdf' }
  });

  const items = useMemo(
    () => [
      {
        id: 'graph',
        name: siteContent.download_graph_label,
        children: [
          {
            id: 'png',
            name: siteContent.download_png_label,
            href: pngUrl,
            external: true
          },
          {
            id: 'pdf',
            name: siteContent.download_pdf_label,
            href: pdfUrl,
            external: true
          }
        ]
      },
      ...(allowDataDownload
        ? [
            {
              id: 'data',
              name: siteContent.download_data_label,
              children: [
                {
                  id: 'csv',
                  name: siteContent.download_csv_label,
                  href: dataUrl,
                  external: true
                }
              ]
            }
          ]
        : [])
    ],
    [siteContent, encodedImageUrl]
  );

  return (
    <SimpleSelect
      id={`${id}-download-select`}
      items={items}
      size="s"
      description={description}
      buttonModifiers={['bare']}
      buttonIcon="download"
      buttonLabel="Download"
      showToggleIcon={false}
    />
  );
};

const DownloadDropdown = memo(DownloadDropdownComponent);

// const ShareDropdownComponent = ({ embedUrl, description, id }) => {
//   return (
//     <SimpleSelect
//       id={`${id}-share-select`}
//       size="s"
//       description={description}
//       buttonModifiers={['icon', 'ghost', cn.headButton]}
//       buttonIcon="link">
//       {() => (
//         <Link href={embedUrl}>
//           <a>Link</a>
//         </Link>
//       )}
//     </SimpleSelect>
//   );
// };

//const ShareDropdown = memo(ShareDropdownComponent);

const withGraphFrame = (GraphComponent) => {
  const WrappedComponent = (props) => {
    const siteContent = useSiteContent();

    const {
      id,
      dataId,
      frameClassName,
      footerClassName,
      titleInFooter = false,
      compactHeader = false,
      titleClassName,
      title,
      secondary_title,
      country,
      caption,
      methodology,
      references,
      disclaimers,
      controls,
      showCaption,
      showFrameButtons = true,
      allowDataDownload = true,
      addFrame = true,
      showHeader = true,
      showFooter = true,
      showDisclaimers = true,
      isStatic,
      reducer = {},
      meta,
      imageWidth = 1200
    } = props;

    // const embedUrl = createUrl({
    //   url: process.env.url,
    //   path: `/countries/${country.slug}/${id}`,
    //   params: reducer.state
    // });

    const imageUrl = createUrl({
      url: process.env.url,
      path: `/countries/${country.slug}/${id}`,
      params: { ...reducer.state, static: true }
    });

    const dataUrl = `${process.env.dataApiUrl}/${country.slug}/${dataId}.csv`;

    if (!addFrame) return <GraphComponent {...props} />;

    const renderedCaption = renderTemplate(caption, props);
    const showButtons = showFrameButtons && !isStatic;

    return (
      <>
        {showHeader && (
          <div
            className={cx(
              frameClassName,
              cn.head,
              showCaption && cn.hasDescription,
              compactHeader && cn.compact
            )}>
            <div className={cn.headTop}>
              <div className={cn.titles}>
                {title && (
                  <h3 className={cx(titleClassName, cn.title)}>
                    <Template text={title} tag="span" data={props} />
                  </h3>
                )}
                {secondary_title && <p className={cn.subtitle}>{secondary_title}</p>}
              </div>
              {showCaption && (
                <figcaption className={cn.chartCaption}>
                  <p>{renderedCaption}</p>
                </figcaption>
              )}
            </div>
            {controls && (
              <div className={cn.controls}>
                {controls.map((control) => {
                  const Control = allControls[control.type];
                  return (
                    <Control
                      size="s"
                      buttonModifiers={['bare']}
                      wrapperModifiers={[cn.control, 'expand']}
                      value={reducer.state[control.id]}
                      isStatic={isStatic}
                      {...control}
                      key={control.id}
                      id={control.id}
                      onChange={reducer.dispatch}
                    />
                  );
                })}
              </div>
            )}
          </div>
        )}
        <div className={frameClassName}>
          <GraphComponent {...props} />
        </div>
        {showFooter && (
          <div
            className={cx(
              frameClassName,
              footerClassName,
              cn.footer,
              showButtons && cn.hasButtons,
              disclaimers && showDisclaimers && cn.hasDisclaimers,
              titleInFooter && cn.hasTitle
            )}>
            {disclaimers && showDisclaimers && (
              <p className={cn.disclaimers}>
                {disclaimers.map((disclaimer, i) => (
                  <span key={i}>{disclaimer} </span>
                ))}
              </p>
            )}
            {titleInFooter && (
              <div className={cn.titles}>
                {title && (
                  <h3 className={cx(titleClassName, cn.title)}>
                    <Template text={title} tag="span" data={props} />
                  </h3>
                )}
                {secondary_title && <p className={cn.subtitle}>{secondary_title}</p>}
              </div>
            )}
            {showButtons && (
              <div className={cn.metaButtons}>
                <InfoDropdown
                  caption={renderedCaption}
                  methodology={methodology}
                  meta={meta}
                  references={references}
                  siteContent={siteContent}
                  description={siteContent.info_select_description}
                  id={id}
                />
                <DownloadDropdown
                  description={siteContent.download_select_description}
                  id={id}
                  imageUrl={imageUrl}
                  dataUrl={dataUrl}
                  imageWidth={imageWidth}
                  fileName={`${country.slug}-${id}`}
                  siteContent={siteContent}
                  allowDataDownload={allowDataDownload}
                />
              </div>
            )}
          </div>
        )}
      </>
    );
  };
  return withErrorBoundary(memo(WrappedComponent));
};

export default withGraphFrame;
