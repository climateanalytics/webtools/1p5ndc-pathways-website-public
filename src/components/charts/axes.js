import { AxisBottom } from '@visx/axis';
import { Group } from '@visx/group';
import cn from './style.module.scss';
import cx from 'classnames';
import { memo } from 'react';
import { Text } from '@visx/text';
import { dynamicallyRoundNumber } from 'src/utils';

function XAxisComponent({
  scale,
  top,
  height,
  numTicks,
  ticks,
  showDomain = false,
  scaleType = 'continuous'
}) {
  const domain = scale.domain();
  const domainMin = domain[0];
  const domainMax = domain[1];
  const adjustLabels = scaleType === 'continuous';

  return (
    <AxisBottom left={0} top={top} tickValues={ticks} numTicks={numTicks} scale={scale}>
      {({ ticks }) => {
        return (
          <Group>
            {ticks.map((tick) => (
              <Group
                key={tick.index}
                left={tick.from.x}
                className={cx(
                  cn.tick,
                  cn.x,
                  adjustLabels && tick.value === domainMin && cn.first,
                  adjustLabels && tick.value === domainMax && cn.last
                )}>
                {showDomain && <line y1={-height} y2={5} className={cx(cn.tickLine)} />}
                <Text y={showDomain ? 10 : 5} verticalAnchor="start" className={cn.tickLabel}>
                  {tick.value}
                </Text>
              </Group>
            ))}
          </Group>
        );
      }}
    </AxisBottom>
  );
}

export const XAxis = memo(XAxisComponent);

function YAxisComponent({
  scale,
  top,
  left,
  align = 'end',
  unit,
  ticks,
  addUnit,
  numTicks = 6,
  format = dynamicallyRoundNumber,
  renderTick
}) {
  // Dont render 0 tick if scale starts at 0
  const axisTicks = (ticks || scale.ticks(numTicks)).filter(
    (t, i) => i !== 0 || scale.domain()[0] !== 0
  );

  return (
    <Group top={top} left={left}>
      <Group left={-4}>
        {axisTicks.map((tick, i) => (
          <Group key={i} top={scale(tick)} className={cn.tick}>
            <Text verticalAnchor="middle" textAnchor={align} className={cn.tickLabel}>
              {renderTick(tick, { unit, addUnit, format })}
            </Text>
          </Group>
        ))}
      </Group>
    </Group>
  );
}

export const YAxis = memo(YAxisComponent);

export function HorizontalGrid({ highlightTicks = [], ticks, scale, width }) {
  return (
    <g>
      {ticks.map((tick, i) => (
        <line
          key={i}
          x2={width}
          y1={scale(tick)}
          y2={scale(tick)}
          className={cx(cn.tickLine, highlightTicks.includes(tick) && cn.highlighted)}
        />
      ))}
    </g>
  );
}
