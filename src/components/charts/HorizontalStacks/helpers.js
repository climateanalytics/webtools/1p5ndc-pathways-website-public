import { reduceBy } from 'src/utils';
import { sum as d3Sum } from 'd3-array';
import { stack as visxStack } from '@visx/shape';
import { scaleLinear } from '@visx/scale';

const sortByParentAndValue = (a, b) => {
  if (a.parent && !b.parent) return -1;
  if (!a.parent && b.parent) return 1;
  if (!a.parent && !b.parent) return b.value - a.value;
  if (a.parent !== b.parent) return a.parent.name > b.parent.name ? -1 : 1;
  return b.value - a.value;
};

const processStack = ({ raw, scale, total, negative, segments }) => {
  return raw
    .map((segmentData) => {
      const d = segmentData[0];
      const segment = segments[segmentData.key];
      const startVal = scale(Math.abs(d[0]));
      const endVal = scale(Math.abs(d[1]));
      const left = negative ? endVal : startVal;
      const right = negative ? startVal : endVal;
      const value = d.data[segmentData.key];
      const width = right - left;
      const share = value / total;
      if (value !== undefined)
        return {
          ...segment,
          left,
          right,
          width,
          centerX: left + width / 2,
          value,
          share
        };
    })
    .filter((d) => d);
};

/* 
[
  {
    id,
    name,
    value,
    parent: id
  }
]
 */
function getStack({ data, width, id }) {
  const segmentsById = reduceBy(data);
  const segmentsData = data.reduce((acc, segment) => ({ ...acc, [segment.id]: segment.value }), {});
  const pSegments = data.filter((segment) => segment.value >= 0).sort(sortByParentAndValue);
  const nSegments = data.filter((segment) => segment.value < 0).sort(sortByParentAndValue);
  const parentSegments = Object.values(
    data.reduce((acc, d) => ({ ...acc, [d.parent?.id]: d.parent }), {})
  )
    .filter((d) => d)
    .map((p) => ({ ...p, value: d3Sum(p.children, (child) => segmentsData[child]) }));
  const parentData = parentSegments.reduce((acc, p) => ({ [p.id]: p.value }), {});

  const pKeys = pSegments.map((s) => s.id);
  const nKeys = nSegments.map((s) => s.id);
  const parentKeys = parentSegments.map((s) => s.id);

  const pTotal = d3Sum(pKeys, (key) => segmentsData[key]);
  const nTotal = d3Sum(nKeys, (key) => Math.abs(segmentsData[key]));

  const positiveIsBigger = pTotal >= nTotal;
  const maxTotal = positiveIsBigger ? pTotal : nTotal;

  const rawPositiveStack = visxStack({
    keys: pKeys,
    offset: positiveIsBigger ? 'expand' : 'none'
  })([segmentsData]);

  const rawNegativeStack = visxStack({
    keys: nKeys,
    offset: positiveIsBigger ? 'none' : 'expand'
  })([segmentsData]);

  const rawParentStack = visxStack({
    keys: parentKeys,
    offset: 'none'
  })([parentData]);

  const pScale = scaleLinear({
    domain: [0, positiveIsBigger ? 1 : maxTotal],
    range: [0, width]
  });

  const nScale = scaleLinear({
    domain: [0, positiveIsBigger ? maxTotal : 1],
    range: [width, 0]
  });

  const parentScale = scaleLinear({
    domain: [0, positiveIsBigger ? pTotal : maxTotal],
    range: [0, width]
  });

  return {
    positiveStack: processStack({
      raw: rawPositiveStack,
      scale: pScale,
      total: maxTotal,
      segments: segmentsById
    }),
    negativeStack: processStack({
      raw: rawNegativeStack,
      scale: nScale,
      negative: true,
      total: maxTotal,
      segments: segmentsById
    }),
    parentStack: processStack({
      raw: rawParentStack,
      scale: parentScale,
      total: maxTotal,
      segments: segmentsById
    })
  };
}
export default getStack;
