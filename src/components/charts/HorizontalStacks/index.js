import withGraphFrame from '../withGraphFrame';
import useDimensions from 'src/utils/useDimensions';
import { renderValue, formatPercent, dynamicallyRoundNumber, useDeviceInfo } from 'src/utils';
import { useMemo, memo } from 'react';
import chartCn from '../style.module.scss';
import cn from './style.module.scss';
import cx from 'classnames';
import { Group } from '@visx/group';
import Tooltip, { useTooltip } from 'src/components/Tooltip';
import { Text } from '@visx/text';
import MixedLegend from '../MixedLegend';
import getStack from './helpers';

// ← ⟵ ⇠ ⇢ ⟶ → ➞
const StackSegments = ({
  data,
  unit,
  height,
  negative,
  showShare,
  onMouseEnter,
  onMouseLeave,
  highlighted
}) => {
  const showUnitAbove = 90;
  const showValueAbove = 30;

  return data.map((d, i) => {
    const barEnd = negative ? 0 : d.width;
    const showUnit = d.width > showUnitAbove && i === 0;
    const showValue = d.width > showValueAbove;
    const formattedValue = showShare
      ? formatPercent(d.share)
      : renderValue(d.value, { format: dynamicallyRoundNumber });
    const isDimmed = highlighted && d.id !== highlighted;

    return (
      <Group key={i} left={d.left} className={cx(cn.barGroup, isDimmed && cn.dimmed)}>
        <rect
          className={cx(chartCn.markBar)}
          width={d.width}
          height={height}
          style={{ fill: d.color }}
          onMouseEnter={(e) => onMouseEnter({ el: e.target, data: d })}
          onMouseLeave={onMouseLeave}
        />
        <rect
          className={chartCn.barSeparator}
          width={Math.min(1, d.width)}
          x={barEnd - 1}
          height={height}
        />
        {showValue && (
          <text
            y={height + 15}
            x={barEnd}
            textAnchor={negative ? 'start' : 'end'}
            className={cn.barValue}>
            {formattedValue}
            <tspan className={cn.barUnit}>{showUnit && !showShare && unit.name}</tspan>
          </text>
        )}
      </Group>
    );
  });
};

const ParentStackSegments = ({ data, unit, showShare }) => {
  const joinLabelsBelow = 100;
  return data.map((d, i) => {
    const isDimmed = false; //tooltip.data && tooltip.data[0].id !== d.id;
    const formattedValue = showShare ? formatPercent(d.share) : renderValue(d.value, { unit });
    const joinLabels = d.width < joinLabelsBelow;
    return (
      <Group key={i} left={d.left} className={cx(cn.barGroup, isDimmed && cn.dimmed)}>
        <line y1={3} y2={3} className={cn.parentBar} x2={d.width} />
        <Group top={-5}>
          <Text className={cn.parentLabel}>
            {joinLabels ? `${d.name} (${formattedValue})` : d.name}
          </Text>
          {!joinLabels && (
            <Text className={cn.parentValue} textAnchor="end" x={d.width}>
              {formattedValue}
            </Text>
          )}
        </Group>
      </Group>
    );
  });
};

const margin = { top: 20, left: 0, right: 0, bottom: 0 };

const Stack = ({
  data,
  unit,
  stackTitle,
  reducer,
  chartClassName,
  negativeLabel,
  positiveLabel,
  showLegend = true,
  id
}) => {
  const [wrapperProps, { width }] = useDimensions({ width: 50 });
  const { handleMouseLeave, handleMouseEnter, tooltip } = useTooltip();
  const { isXS } = useDeviceInfo();

  const barHeight = isXS ? 26 : 32;

  const chartWidth = width - margin.left - margin.right;

  const { negativeStack, positiveStack, parentStack } = useMemo(
    () =>
      getStack({
        data: data,
        width: chartWidth,
        id
      }),
    [data, chartWidth]
  );

  const legendItems = useMemo(
    () => [...positiveStack, ...negativeStack].map((s) => ({ ...s, type: 'bar' })),
    [positiveStack, negativeStack]
  );

  const labelHeight = 18;
  const hasParents = parentStack.length;
  const hasNegativeStack = negativeStack.length;
  const stackHeight = barHeight + labelHeight;
  const parentHeight = 12;
  const stackSpacing = 32;
  let chartHeight = stackHeight;
  if (hasParents) chartHeight += parentHeight;
  if (hasNegativeStack) chartHeight += stackHeight + stackSpacing;
  const positiveTop = hasParents ? parentHeight : 0;
  const negativeTop = positiveTop + stackHeight + stackSpacing;
  const showShare = reducer.state.displayType === 'pc';
  const marginTop = hasParents ? margin.top : 0;
  const height = chartHeight + marginTop + margin.bottom;

  return (
    <>
      {stackTitle && <h4 className={cn.stackTitle}>{stackTitle}</h4>}
      {showLegend && <MixedLegend wrapperModifiers={[cn.legend]} items={legendItems} />}
      <div {...wrapperProps} className={cx(chartClassName, cn.chartWrapper, chartCn.chartWrapper)}>
        <svg width={width} height={height} className={chartCn.svg}>
          <Group top={marginTop} left={margin.left}>
            {hasParents && (
              <Group>
                <ParentStackSegments
                  data={parentStack}
                  height={parentHeight}
                  showShare={showShare}
                  highlighted={tooltip?.data.id}
                  unit={unit}
                />
              </Group>
            )}
            <Group top={positiveTop}>
              <StackSegments
                data={positiveStack}
                height={barHeight}
                directionLabel={positiveLabel}
                showShare={showShare}
                highlighted={tooltip?.data.id}
                onMouseEnter={handleMouseEnter}
                onMouseLeave={handleMouseLeave}
                unit={unit}
              />
            </Group>
            {hasNegativeStack && (
              <Group top={negativeTop - 10}>
                <Text
                  verticalAnchor="middle"
                  textAnchor="end"
                  x={chartWidth}
                  className={cn.directionLabel}>
                  {`⟵ ${negativeLabel}`}
                </Text>
              </Group>
            )}
            <Group top={negativeTop}>
              <StackSegments
                data={negativeStack}
                height={barHeight}
                directionLabel={negativeLabel}
                negative={true}
                showShare={showShare}
                highlighted={tooltip?.data.id}
                onMouseEnter={handleMouseEnter}
                onMouseLeave={handleMouseLeave}
                unit={unit}
              />
            </Group>
          </Group>
        </svg>
        {tooltip && (
          <Tooltip {...tooltip}>
            <div className={cn.tooltipTitle}>{tooltip.data.name}</div>
            <div>
              <b>{renderValue(tooltip.data.value, { unit, format: dynamicallyRoundNumber })}</b> (
              {formatPercent(tooltip.data.share)})
            </div>
          </Tooltip>
        )}
      </div>
    </>
  );
};

const Stacks = ({ data, stackTitle, ...props }) => {
  return (
    <>
      <h4 className={cn.stackTitle}>{stackTitle}</h4>
      <div className={cn.stackMultiples}>
        {data.map((d, i) => (
          <div key={i} className={cn.stackMultiple}>
            <h5 className={cn.stackName}>{d.name}</h5>
            <Stack showLegend={false} data={d.data} {...props} />
          </div>
        ))}
      </div>
    </>
  );
};

export const HorizontalStack = withGraphFrame(memo(Stack));
export const HorizontalStacks = withGraphFrame(memo(Stacks));
