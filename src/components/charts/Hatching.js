import { memo } from 'react';
const HatchingComponent = ({ size = 5, strokeWidth = 1, color = 'white', id = 'hatching' }) => {
  return (
    <pattern id={id} width={size} height={size} patternUnits="userSpaceOnUse">
      <g>
        <line x1={0} x2={size} y1={size} y2={0} style={{ stroke: color, strokeWidth }} />
        <line
          x1={-strokeWidth}
          x2={strokeWidth}
          y1={strokeWidth}
          y2={-strokeWidth}
          style={{ stroke: color, strokeWidth }}
        />
        <line
          x1={size - strokeWidth}
          x2={size + strokeWidth}
          y1={size + strokeWidth}
          y2={size - strokeWidth}
          style={{ stroke: color, strokeWidth }}
        />
      </g>
    </pattern>
  );
};

export default memo(HatchingComponent);
