import { useMemo, memo } from 'react';
import cn from './style.module.scss';
import cx from 'classnames';
import Hatching from './Hatching';

const Legend = ({ items, wrapperModifiers = [] }) => {
  const legendItems = useMemo(
    () =>
      items
        .filter((item) => !item.hideInLegend && item.type !== 'tick')
        .map((item) => ({
          ...item,
          name: item.legendName || item.name
        })),
    [items]
  );

  return (
    <ul className={cx(cn.legend, ...wrapperModifiers)}>
      {legendItems.map((item, i) => {
        return (
          <li className={cx(cn.legendItem, cn[item.type])} key={i}>
            <svg className={cn.legendItemMark}>
              <Hatching id={`legend-hatching`} />
              {item.type === 'area' && (
                <circle
                  className={cn.markArea}
                  cx="50%"
                  cy="50%"
                  r="50%"
                  style={{ '--color': item.color, ...(item.style || {}) }}
                />
              )}
              {item.type === 'bar' && (
                <>
                  <circle
                    className={cn.markBar}
                    cx="50%"
                    cy="50%"
                    r="50%"
                    style={{ fill: item.color, ...(item.style || {}) }}
                  />
                  {item.hatching && (
                    <circle cx="50%" cy="50%" r="50%" fill="url(#legend-hatching)" />
                  )}
                </>
              )}
              {item.type === 'line' && (
                <line
                  className={cx(cn.markLine, item.dashed && cn.dashed)}
                  x2="100%"
                  y1="50%"
                  y2="50%"
                  style={{ stroke: item.color, ...(item.style ? item.style : {}), strokeWidth: 3 }}
                />
              )}
            </svg>
            <span>{item.name}</span>
          </li>
        );
      })}
    </ul>
  );
};

export default memo(Legend);
