import { Text } from '@visx/text';
import cx from 'classnames';
import { useEffect, useState } from 'react';

const ChartText = ({
  className,
  frontClassName,
  backClassName,
  frontColor,
  backColor = '#fff',
  strokeWidth = 3,
  children,
  ...rest
}) => {
  // Needed because visx/text and ssr for some reason don't play well together
  const [initialized, setInitialized] = useState(false);
  useEffect(() => setInitialized(true), []);
  return initialized ? (
    <>
      <Text
        style={{ stroke: backColor, strokeWidth }}
        className={cx(className, backClassName)}
        {...rest}>
        {children}
      </Text>
      <Text style={{ fill: frontColor }} className={cx(className, frontClassName)} {...rest}>
        {children}
      </Text>
    </>
  ) : null;
};
export default ChartText;

export const ChartTextSvg = ({
  className,
  frontClassName,
  backClassName,
  frontColor,
  backColor = '#fff',
  strokeWidth = 3,
  children,
  ...rest
}) => {
  return (
    <>
      <text
        style={{ stroke: backColor, strokeWidth }}
        className={cx(className, backClassName)}
        {...rest}>
        {children}
      </text>
      <text style={{ fill: frontColor }} className={cx(className, frontClassName)} {...rest}>
        {children}
      </text>
    </>
  );
};
