import withGraphFrame from '../withGraphFrame';
import useDimensions from 'src/utils/useDimensions';
import chartCn from '../style.module.scss';
import cn from './style.module.scss';
import cx from 'classnames';
import { scaleLinear } from '@visx/scale';
import { Group } from '@visx/group';
import { memo, useEffect, useMemo, useRef, useState } from 'react';
import { renderValue } from 'src/utils';
import ChartText from '../ChartText';
import { HorizontalGrid } from '../axes';
import { getYTicks } from '../helpers';
import { Text } from '@visx/text';
import { useTheme } from 'src/style/ThemeProvider';

const defaultMargin = { left: 10, top: 10, right: 10, bottom: 35 };

const NDCGraph = ({
  chartClassName,
  data,
  yExtent,
  unit,
  baselineValue,
  referenceValue,
  baselineLabel,
  baselineUnit,
  chartLabel
}) => {
  const margin = defaultMargin;
  const [wrapperProps, { width, height }] = useDimensions({ width: 100, height: 500 });
  const chartWidth = width - margin.left - margin.right;
  const chartHeight = height - margin.top - margin.bottom;
  const containerRef = useRef();
  const [barLabels, setBarLabels] = useState([]);
  const theme = useTheme();

  const yScale = useMemo(
    () => scaleLinear({ domain: yExtent, range: [chartHeight, 0], nice: true }),
    [yExtent, chartHeight]
  );

  const baselineY = yScale(baselineValue);

  const yTicks = useMemo(
    () => getYTicks([{ type: 'tick', value: baselineValue }], yScale).ticks,
    [yScale]
  );

  const dataWithValues = useMemo(
    () => data.filter((d) => d.max !== undefined && d.min !== undefined),
    [data]
  );

  const stepWidth = Math.round(chartWidth / dataWithValues.length);
  const barWidth = 10;
  const stepCenterX = stepWidth / 2;
  const barOffset = Math.round(stepCenterX - barWidth / 2);

  const isRelative = unit.id === 'pc';

  const chartData = useMemo(
    () =>
      dataWithValues.map((d, i) => {
        const formattedMax = renderValue(d.max, { unit, addUnit: isRelative, signed: isRelative });
        const formattedMin = renderValue(d.min, { unit, addUnit: isRelative, signed: isRelative });
        const isRange = formattedMax !== formattedMin;
        let y = Math.round(yScale(d.max)) + 0.5;
        y = isRange ? y : y - barWidth / 2; // if is not range, we need to adjust for "artificial minimal height"
        const bottom = Math.round(yScale(d.min));
        const height = isRange ? Math.max(bottom - y, barWidth) : barWidth;
        return {
          ...d,
          y,
          height,
          isRange,
          formattedMax,
          formattedMin,
          x: Math.floor(stepWidth * i),
          markY: yScale(d.mark || d.min) - y,
          formattedMark: renderValue(d.mark || d.min, {
            unit,
            addUnit: isRelative,
            signed: isRelative
          }),
          hasMark: !!d.mark
        };
      }),
    [dataWithValues, chartWidth, stepWidth]
  );

  useEffect(() => {
    if (!containerRef.current) return;
    const elements = Array.from(containerRef.current.querySelectorAll(`.${cn.barLabel}`));
    const placed = [];
    const minGap = 12; // Minimum vertical gap between labels
    const minYOffset = 10;
    const labelStrokePadding = 4;
    elements.forEach((el, i) => {
      const { height } = el.getBoundingClientRect();
      const { y: barY, isRange, height: barHeight } = chartData[i];
      const barCenter = barY + barHeight / 2;
      const sign = barCenter > chartHeight / 2 ? -1 : 1;
      const yPadding = isRange ? 20 : 5;
      const halfHeight = (height / 2) * sign;
      let targetY = barCenter + (barHeight / 2 + yPadding) * sign;
      let y = targetY + (height + minYOffset) * sign;

      if (i > 0) {
        const { height: prevHeight, y: prevY } = placed[i - 1];
        const dY = Math.abs(y - prevY);
        const minDist = height / 2 + prevHeight / 2 + minGap;
        const overlapping = dY < minDist;
        y = overlapping ? prevY + (minDist + minYOffset) * sign : y;
      }

      let originY = y - halfHeight - labelStrokePadding * sign;

      placed.push({
        targetY,
        originY,
        height,
        y
      });
    });
    setBarLabels(placed);
  }, [containerRef, chartData]);

  const renderedLabels = chartData.map((d, i) => {
    const { y, targetY, originY } = barLabels[i] || { y: 0, targetY: 0 };
    return {
      ...d,
      targetY,
      originY,
      y
    };
  });

  return (
    <div className={cn.wrapper}>
      <div {...wrapperProps} className={cx(chartClassName, chartCn.chartWrapper, cn.chart)}>
        <svg width={width} height={height} className={chartCn.svg}>
          <Group left={margin.left} top={margin.top}>
            <HorizontalGrid
              ticks={yTicks}
              highlightTicks={[baselineValue]}
              scale={yScale}
              width={chartWidth}
            />
            <Group top={chartHeight + 5}>
              <line x2={chartWidth} className={cx(chartCn.tickLine, chartCn.major)} />
              <Text
                y={10}
                x={chartWidth / 2}
                verticalAnchor="start"
                textAnchor="middle"
                className={cx(chartCn.label, chartCn.primary)}>
                {chartLabel}
              </Text>
            </Group>

            {chartData.map((bar, i) => (
              <Group key={i} top={bar.y} left={bar.x - 0.5}>
                <rect
                  x={barOffset}
                  className={chartCn.markBackground}
                  style={{ stroke: theme.color.brand.weakest }}
                  height={bar.isRange ? bar.height : barWidth}
                  rx={barWidth / 2}
                  width={barWidth}
                />
                <rect
                  x={barOffset}
                  className={chartCn.markForeground}
                  height={bar.isRange ? bar.height : barWidth}
                  rx={barWidth / 2}
                  style={{ fill: bar.color, stroke: bar.color }}
                  width={barWidth}
                />
                {bar.hasMark && bar.isRange && (
                  <rect
                    x={barOffset}
                    className={cn.rangeMark}
                    y={bar.markY - 1}
                    height={2}
                    width={barWidth}
                    style={{ fill: bar.color }}
                  />
                )}
                {bar.isRange && (
                  <ChartText
                    strokeWidth={4}
                    className={cn.barValue}
                    backColor={theme.color.brand.weakest}
                    x={stepCenterX}
                    y={-5}
                    textAnchor="middle">
                    {bar.formattedMax}
                  </ChartText>
                )}

                {bar.isRange && (
                  <ChartText
                    strokeWidth={4}
                    className={cn.barValue}
                    backColor={theme.color.brand.weakest}
                    x={stepCenterX}
                    y={bar.height + 5}
                    verticalAnchor="start"
                    textAnchor="middle">
                    {bar.formattedMin}
                  </ChartText>
                )}
                {(bar.hasMark || !bar.isRange) && (
                  <ChartText
                    strokeWidth={4}
                    className={cx(cn.barValue, cn.primary)}
                    backColor={theme.color.brand.weakest}
                    x={barOffset + barWidth + 4}
                    y={bar.markY}
                    verticalAnchor="middle">
                    {bar.formattedMark}
                  </ChartText>
                )}
              </Group>
            ))}
            {renderedLabels.map((label, i) => (
              <Group key={i} left={label.x + stepCenterX}>
                <line y1={label.targetY} y2={label.originY} className={cn.labelLineBackground} />
                <line y1={label.targetY} y2={label.originY} className={cn.labelLine} />
              </Group>
            ))}
          </Group>
        </svg>
        <div
          className={cn.barLabels}
          ref={containerRef}
          style={{ top: margin.top, left: margin.left }}>
          {renderedLabels.map((bar, i) => (
            <div
              key={i}
              className={cx(cn.barLabel)}
              style={{
                top: bar.y,
                left: bar.x + stepCenterX,
                width: chartWidth * 0.38
              }}>
              {bar.name}
            </div>
          ))}
        </div>
      </div>
      <div className={cn.baselineLabelWrapper}>
        <div className={cn.baselineLabel} style={{ top: margin.top + baselineY }}>
          <div className={cn.baselineLabelName}>{baselineLabel}</div>
          <div className={cn.baselineLabelValue}>
            {renderValue(referenceValue, { unit: baselineUnit })}
          </div>
        </div>
      </div>
    </div>
  );
};

export default withGraphFrame(memo(NDCGraph));
