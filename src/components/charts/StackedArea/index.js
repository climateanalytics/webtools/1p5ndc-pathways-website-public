import withGraphFrame from '../withGraphFrame';
import useDimensions from 'src/utils/useDimensions';
import { renderValue, reduceBy, dynamicallyRoundNumber, useSeriesTicks } from 'src/utils';
import { useMemo, memo, Fragment, useState } from 'react';
import { max as d3Max, extent as d3Extent } from 'd3-array';
import { stack as d3Stack, area as d3Area, stackOrderDescending } from 'd3-shape';
import chartCn from '../style.module.scss';
import cn from './style.module.scss';
import cx from 'classnames';
import { scaleLinear, scaleBand } from '@visx/scale';
import { Group } from '@visx/group';
import { XAxis, YAxis, HorizontalGrid } from '../axes';
import MixedLegend from '../MixedLegend';

const margin = { top: 0, right: 0, bottom: 20, left: 0 };

function getStack({ data, id, segments, width, height }) {
  const stacks = d3Stack()
    .order(stackOrderDescending)
    .keys(segments.map((d) => d.id))(data);
  const maxY = d3Max(stacks, (stack) => d3Max(stack, (d) => d[1]));
  const extentX = d3Extent(data, (d) => d.year);
  const yScale = scaleLinear().domain([0, maxY]).range([height, 0]);
  const xScale = scaleLinear().domain(extentX).range([0, width]);
  return { stacks, xScale, yScale };
}

const NetStacks = ({ segments, data, unit, chartClassName }) => {
  const [highlighted, setHighlighted] = useState(null);
  const [wrapperProps, { width, height }] = useDimensions({ height: 500, width: 100 });
  const chartWidth = width - margin.left - margin.right;
  const chartHeight = height - margin.top - margin.bottom;
  const segmentsById = reduceBy(segments);

  const { stacks, xScale, yScale } = useMemo(
    () =>
      getStack({
        data,
        segments,
        width: chartWidth,
        height: chartHeight
      }),
    [data, segments, chartWidth, chartHeight]
  );

  const yTicks = yScale.ticks(5);
  const xTicks = xScale.ticks(5);

  const area = d3Area()
    .x((d) => xScale(d.data.year))
    .y0((d) => yScale(d[0]))
    .y1((d) => yScale(d[1]));

  const legendItems = useMemo(() => segments.map((s) => ({ ...s, type: 'bar' })), [segments]);
  return (
    <>
      <div {...wrapperProps} className={cx(chartClassName, chartCn.chartWrapper)}>
        <svg width={width} height={height} className={chartCn.svg}>
          <Group top={margin.top} left={margin.left}>
            <XAxis scale={xScale} ticks={xTicks} top={chartHeight + 1} scaleType="band" />
            <YAxis
              scale={yScale}
              top={margin.top}
              ticks={yTicks}
              renderTick={renderValue}
              unit={unit}
              addUnit={false}
            />
            <HorizontalGrid scale={yScale} ticks={yTicks} width={chartWidth} />
            {stacks.map((stack, i) => {
              const dim = highlighted !== null && stack.key !== highlighted;
              return (
                <path
                  key={i}
                  onMouseEnter={() => setHighlighted(stack.key)}
                  onMouseLeave={() => setHighlighted(null)}
                  d={area(stack)}
                  fill={segmentsById[stack.key].color}
                  style={{ opacity: dim ? 0.5 : 1 }}
                />
              );
            })}
            {stacks.map((stack, i) => {
              const highlight = highlighted === stack.key;
              return (
                highlight &&
                stack.map((d, i) => (
                  <circle
                    key={i}
                    className={cn.labelMark}
                    cx={xScale(d.data.year)}
                    cy={yScale(d[1])}
                    r={3}
                    style={{ fill: segmentsById[stack.key].color }}
                  />
                ))
              );
            })}
          </Group>
        </svg>
        <div>
          {stacks.map((stack, i) => {
            const visible = stack.key === highlighted;
            return stack.map((d, j) => (
              <div key={j} className={cx(cn.labels, visible && cn.visible)}>
                <span
                  className={cn.label}
                  style={{
                    left: `${xScale(d.data.year)}px`,
                    top: `${yScale(d[1]) - 4}px`,
                    transform: 'translate(-50%, -100%)',
                    textAlign: 'center'
                  }}>
                  {dynamicallyRoundNumber(d.data[stack.key])}
                </span>
              </div>
            ));
          })}
        </div>
      </div>
      <MixedLegend items={legendItems} />
    </>
  );
};

export default withGraphFrame(memo(NetStacks));
