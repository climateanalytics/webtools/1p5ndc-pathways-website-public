import withGraphFrame from '../withGraphFrame';
import useDimensions from 'src/utils/useDimensions';
import { renderValue, useSeriesTicks } from 'src/utils';
import { useMemo, memo, useCallback, useRef } from 'react';
import { extent, max, min } from 'd3-array';
import { Area, LinePath } from '@visx/shape';
import { localPoint } from '@visx/event';
import chartCn from '../style.module.scss';
import cn from './style.module.scss';
import cx from 'classnames';
import { scaleLinear } from '@visx/scale';
import { Group } from '@visx/group';
import Tooltip, { useTooltip } from 'src/components/Tooltip';
import Hatching from '../Hatching';
import { XAxis } from '../axes';
import ChartText, { ChartTextSvg } from '../ChartText';
import { Fragment } from 'react';

const margin = { top: 15, left: 0, right: 0, bottom: 15 };

const AreaChartComponent = ({
  data,
  yExtent,
  onMouseMove,
  onMouseLeave,
  hatchingRange,
  hatchingLabel,
  name,
  unit,
  color,
  tooltipIndex,
  chartClassName,
  showXAxis
}) => {
  const [wrapperProps, { width }] = useDimensions({
    width: 200,
    height: 100
  });
  const rootRef = useRef(null);

  const chartWidth = width - margin.left - margin.right;

  const xScale = useMemo(
    () => scaleLinear({ domain: extent(data, (d) => d.year), range: [0, chartWidth] }),
    [data, width]
  );

  const xTicks = useSeriesTicks(chartWidth / 2, xScale.domain());

  // In order to optimize vertical space, we need to individually adjust the height of each
  // svg element based on the actual data extent of the data and therefore adjust the y scale
  const maxHeight = 100;
  const dataMax = max(data, (d) => d.value);
  // Make sure we have 0 as min if values don't go below 0
  const dataMin = Math.min(
    min(data, (d) => d.value),
    0
  );
  const maxChartHeight = maxHeight - margin.top - margin.bottom;
  const maxYScale = useMemo(
    () =>
      scaleLinear({
        domain: [yExtent[1], yExtent[0]],
        range: [0, maxChartHeight]
      }),
    [data, maxChartHeight]
  );

  const realYExtent = extent(data, (d) => maxYScale(d.value));
  const chartHeight = realYExtent[1] - realYExtent[0];
  const height = chartHeight + margin.top + margin.bottom;

  const realYScale = useMemo(
    () =>
      scaleLinear({
        domain: [dataMax, dataMin],
        range: [0, chartHeight]
      }),
    [dataMax, dataMin, chartHeight]
  );

  const labels = useMemo(() => {
    return data
      .map(({ year, value }, i) => {
        return {
          isVisible: xTicks.includes(year) || tooltipIndex === i,
          isHighlighted: tooltipIndex === undefined || tooltipIndex === i,
          x: xScale(year),
          y: realYScale(value),
          value: renderValue(value, { unit: tooltipIndex === i && unit })
        };
      })
      .sort((a, b) => (a.isHighlighted ? 1 : -1));
  }, [xScale, data, tooltipIndex]);

  // Tooltip stuff
  const handleMouseMove = useCallback(
    (e) => {
      const point = localPoint(e);
      const xVal = xScale.invert(point.x);
      // Get closest point
      const closest = data.reduce(
        (closest, datum, index) => {
          const dist = Math.abs(datum.year - xVal);
          return dist < closest.dist ? { dist, index } : closest;
        },
        { dist: Infinity }
      );
      onMouseMove({ data: closest.index });
    },
    [onMouseMove, xScale, data]
  );

  // Hatching props
  const hatchingData = useMemo(
    () =>
      hatchingRange && data.filter((d) => d.year >= hatchingRange[0] && d.year <= hatchingRange[1]),
    [data, hatchingRange]
  );
  const hatchingStart = xScale(hatchingRange[0]);
  const hatchingEnd = xScale(hatchingRange[1]);
  const hatchingWidth = hatchingEnd - hatchingStart;
  const nonHatchingData = useMemo(
    () => hatchingRange && data.filter((d) => d.year >= hatchingRange[1]),
    [data, hatchingRange]
  );

  // Render main graphics in separate variable so we can prevent
  // unnecessry updates when rendering tooltip
  const graphics = useMemo(
    () => (
      <>
        {showXAxis && <XAxis ticks={xTicks} scale={xScale} top={chartHeight + 15} />}
        {hatchingLabel && (
          <Group left={hatchingStart} top={chartHeight + 5}>
            <line x2={hatchingWidth} className={cn.hatchingDomain} />
            <ChartText
              strokeWidth={11}
              verticalAnchor="middle"
              x={hatchingWidth / 2}
              width={hatchingWidth * 0.8}
              className={cn.hatchingLabel}
              textAnchor="middle">
              {hatchingLabel}
            </ChartText>
          </Group>
        )}
        <Area
          data={data}
          x={(d) => xScale(d.year)}
          y1={(d) => realYScale(d.value)}
          y0={() => realYScale(0)}
          style={{ opacity: 0.3, fill: color }}
        />
        <Area
          data={hatchingData}
          x={(d) => xScale(d.year)}
          y1={(d) => realYScale(d.value)}
          y0={() => realYScale(0)}
          fill="url(#hatching)"
        />
        <LinePath
          data={nonHatchingData}
          x={(d) => xScale(d.year)}
          y={(d) => realYScale(d.value)}
          style={{ stroke: color, strokeWidth: 2 }}
        />
        <LinePath
          data={hatchingData}
          className={cn.dotted}
          x={(d) => xScale(d.year)}
          y={(d) => realYScale(d.value)}
          style={{ stroke: color, strokeWidth: 2 }}
        />
      </>
    ),
    [realYScale, xScale, data, chartHeight, hatchingData]
  );

  return (
    <div
      onMouseMove={handleMouseMove}
      onMouseLeave={onMouseLeave}
      onTouchMove={handleMouseMove}
      onTouchEnd={onMouseLeave}
      className={cn.multipleChartWrapper}>
      <div {...wrapperProps} className={cx(chartClassName, chartCn.chartWrapper)}>
        <svg width={width} height={height} ref={rootRef} className={chartCn.svg}>
          <defs>
            <Hatching id="hatching" width={chartWidth} height={chartHeight} />
          </defs>
          <Group top={margin.top} left={margin.left}>
            {graphics}
            {labels.map((tick, i) => (
              <Group
                className={cx(
                  cn.label,
                  tick.isVisible && cn.isVisible,
                  tick.isHighlighted && cn.isHighlighted
                )}
                key={i}>
                <ChartTextSvg
                  key={i}
                  x={tick.x}
                  y={tick.y - 10}
                  className={cn.tickValue}
                  frontColor={color}>
                  {tick.value}
                </ChartTextSvg>
                <circle
                  cx={tick.x}
                  cy={tick.y}
                  r={3}
                  className={cn.tickDot}
                  style={{ fill: color }}
                />
              </Group>
            ))}
          </Group>
        </svg>
      </div>
      <h5 className={cn.multipleName} style={{ '--color': color, top: margin.top + realYScale(0) }}>
        {name}
      </h5>
    </div>
  );
};

const AreaChart = memo(AreaChartComponent);

const AreaMultiples = ({ data, unit, hatchingRange, hatchingLabel, chartClassName }) => {
  const { tooltip, handleMouseMove, handleMouseLeave } = useTooltip();
  const yExtent = useMemo(
    () => [
      // Make sure we have 0 as min if values don't go below 0
      min(data, (series) => min(series.data, (d) => Math.min(d.value, 0))),
      max(data, (series) => max(series.data, (d) => d.value))
    ],
    [data]
  );

  // Add maximum of each series and sort by maximum
  const chartData = useMemo(() => {
    return data
      .map((series) => ({
        ...series,
        max: max(series.data, (d) => d.value)
      }))
      .sort((a, b) => b.max - a.max);
  }, [data]);

  return (
    <>
      {chartData.map((series, i) => {
        return (
          <AreaChart
            chartClassName={chartClassName}
            key={series.id}
            onMouseMove={handleMouseMove}
            onMouseLeave={handleMouseLeave}
            unit={unit}
            tooltipIndex={tooltip?.data}
            showXAxis={i === chartData.length - 1}
            hatchingLabel={i === 0 && hatchingLabel}
            hatchingRange={hatchingRange}
            {...series}
            yExtent={yExtent}
          />
        );
      })}
    </>
  );
};

export default withGraphFrame(memo(AreaMultiples));
