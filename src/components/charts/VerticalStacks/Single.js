import withGraphFrame from '../withGraphFrame';
import { useMemo, memo } from 'react';
import { max as d3Max, sum as d3Sum } from 'd3-array';
import { useTooltip } from 'src/components/Tooltip';
import MixedLegend from '../MixedLegend';
import Chart from './Chart';

const Single = ({ id, reducer, segments, data, unit }) => {
  const { scaling } = reducer.state;

  const { handleMouseLeave, handleMouseEnter, tooltip } = useTooltip();

  const max = useMemo(() =>
    d3Max(data, (d) => d3Sum(segments, (segment) => d[segment.id]), [data])
  );

  const legendItems = useMemo(() => segments.map((s) => ({ ...s, type: 'bar' })), [segments]);

  return (
    <>
      <Chart
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
        scaling={scaling}
        segments={segments}
        highlighted={tooltip?.data}
        id={id}
        unit={unit}
        data={data}
        max={max}
      />
      {segments.length > 1 && <MixedLegend items={legendItems} />}
    </>
  );
};

export default withGraphFrame(memo(Single));
