import useDimensions from 'src/utils/useDimensions';
import { reduceBy, renderValue, dynamicallyRoundNumber, formatPercent } from 'src/utils';
import { useMemo, useCallback } from 'react';
import { sum } from 'd3-array';
import { stack as d3Stack, stackOrderNone, stackOffsetExpand, stackOffsetNone } from 'd3-shape';
import chartCn from '../style.module.scss';
import cn from './style.module.scss';
import cx from 'classnames';
import { scaleLinear, scaleBand } from '@visx/scale';
import { Group } from '@visx/group';
import { XAxis, YAxis, HorizontalGrid } from '../axes';
import Tooltip from 'src/components/Tooltip';
import { units } from 'src/data/meta';

const margin = { top: 0, right: 0, bottom: 25, left: 0 };

const offsets = { relative: stackOffsetExpand, absolute: stackOffsetNone };

function sumSegment(series) {
  var s = 0,
    i = -1,
    n = series.length,
    v;
  while (++i < n) if ((v = +series[i][1])) s += v;
  return s;
}

function getStack({
  data,
  max,
  id,
  scaling = 'absolute',
  segments,
  segmentGroups,
  width,
  height,
  padding = 0.05
}) {
  const segmentsById = reduceBy(segments);
  const isRelative = scaling === 'relative';
  const xScale = scaleBand({
    domain: data.map((d) => d.year),
    range: [0, width],
    paddingInner: padding
  });

  const yScale = scaleLinear({
    domain: [0, isRelative ? 1 : max],
    range: [height, 0]
  });

  const keys = segments.map((s) => s.id);

  const rawStack = d3Stack()
    .keys(keys)
    .order((segment) => {
      // Sort by segment group and total of segment value
      const sums = segment.map(sumSegment);
      const indices = segment.map(({ key }) =>
        segmentGroups.findIndex((group) => group.indexOf(key) !== -1)
      );
      return stackOrderNone(segment).sort((a, b) => {
        return indices[a] === indices[b] ? sums[b] - sums[a] : indices[b] - indices[a];
      });
    })
    .offset(offsets[scaling])(data);
  const stackTotals = data.map((stack) => sum(keys, (key) => stack[key]));

  const stack = rawStack.map((segment) => {
    return segment
      .map((d, stackIndex) => {
        const top = yScale(d[1]);
        const bottom = yScale(d[0]);
        const width = xScale.bandwidth();
        const left = xScale(d.data.year);
        const value = d.data[segment.key];
        const segmentMeta = segmentsById[segment.key];
        return {
          ...segmentMeta,
          elId: `${id}-${segmentMeta.id}-${d.data.year}`,
          top,
          left,
          height: bottom - top,
          width: xScale.bandwidth(),
          centerX: left + width / 2,
          value,
          share: value / stackTotals[stackIndex]
        };
      })
      .filter((d) => d.value !== undefined);
  });

  return {
    xScale,
    yScale,
    stack
  };
}

export default ({
  data,
  id,
  segments,
  segmentGroups = [],
  max,
  unit,
  onMouseEnter,
  onMouseLeave,
  name,
  highlighted,
  scaling
}) => {
  const [wrapperProps, { width, height }] = useDimensions({ height: 500, width: 100 });
  const chartWidth = width - margin.left - margin.right;
  const chartHeight = height - margin.top - margin.bottom;

  const { stack, xScale, yScale } = useMemo(
    () =>
      getStack({
        id,
        data,
        segments,
        segmentGroups,
        scaling,
        max,
        width: chartWidth,
        height: chartHeight,
        padding: 0.3
      }),
    [data, scaling, segments, max, chartWidth, chartHeight]
  );

  const isRelative = scaling === 'relative';
  const chartUnit = isRelative ? units.pc : unit;
  const yTicks = yScale.ticks(3).filter((t) => t !== 0);

  const tooltip = useMemo(() => {
    if (highlighted === undefined) return;
    return stack.find((d) => d[0]?.id === highlighted.segment);
  }, [highlighted, stack]);

  // Tooltip stuff
  const handleMouseEnter = useCallback(
    (segment, index) => {
      onMouseEnter({ data: { segment: segment.id, index } });
    },
    [onMouseEnter, xScale, data]
  );

  return (
    <>
      <div className={cn.bigChartLabel}>
        <div className={cn.bigChartName}>{name}</div>
      </div>
      <div {...wrapperProps} className={cx(cn.stackChart, chartCn.chartWrapper)}>
        <svg width={width} height={height} className={chartCn.svg}>
          <Group top={margin.top} left={margin.left}>
            <XAxis scale={xScale} top={chartHeight + 1} scaleType="band" />
            <YAxis
              scale={yScale}
              ticks={yTicks}
              top={margin.top}
              renderTick={isRelative ? formatPercent : dynamicallyRoundNumber}
              unit={chartUnit}
              addUnit={isRelative}
            />
            <HorizontalGrid scale={yScale} ticks={yTicks} width={chartWidth} />
            {stack.map((segment) => {
              return segment.map((d, i) => {
                const isDimmed = highlighted && highlighted.segment !== d.id;
                return (
                  <Group
                    key={i}
                    top={d.top}
                    left={d.left}
                    className={cx(cn.barGroup, isDimmed && cn.dimmed)}>
                    <rect
                      onMouseEnter={() => handleMouseEnter(d, i)}
                      onMouseLeave={onMouseLeave}
                      className={chartCn.markBar}
                      width={d.width}
                      height={Math.max(d.height, 0)}
                      id={d.elId}
                      style={{ fill: d.color }}
                    />
                  </Group>
                );
              });
            })}
          </Group>
        </svg>
        {tooltip &&
          tooltip.map((d, i) => (
            <Tooltip
              key={i}
              {...tooltip}
              offset={3}
              wrapperClassName={cn.tooltip}
              tooltipFor={d.elId}>
              <span className={cx(i === highlighted.index && cn.major, cn.tooltipValue)}>
                {isRelative
                  ? renderValue(d.share, { unit: chartUnit })
                  : renderValue(d.value, {
                      unit: chartUnit,
                      addUnit: i === highlighted.index,
                      format: dynamicallyRoundNumber
                    })}
              </span>
            </Tooltip>
          ))}
      </div>
    </>
  );
};
