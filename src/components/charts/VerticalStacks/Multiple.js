import withGraphFrame from '../withGraphFrame';
import { reduceBy } from 'src/utils';
import { useMemo, memo } from 'react';
import { max as d3Max, sum as d3Sum } from 'd3-array';
import cn from './style.module.scss';
import cx from 'classnames';
import { useTooltip } from 'src/components/Tooltip';
import MixedLegend from '../MixedLegend';
import Chart from './Chart';

const VerticalStacks = ({ id, chartsMeta, reducer, segments, segmentGroups = [], data, unit }) => {
  const metaById = useMemo(() => reduceBy(chartsMeta), [chartsMeta]);
  const { scaling, selectedChart } = reducer.state;

  const { handleMouseLeave, handleMouseEnter, tooltip } = useTooltip();

  const chartData = useMemo(
    () =>
      data
        .map((chart) => {
          return {
            data: chart.data,
            selected: chart.id === selectedChart,
            ...metaById[chart.id]
          };
        })
        .filter((d) => d.id),
    [metaById, data, selectedChart]
  );

  const max = useMemo(() =>
    d3Max(
      chartData,
      (chart) => d3Max(chart.data, (d) => d3Sum(segments, (segment) => d[segment.id])),
      [chartData]
    )
  );

  const legendItems = useMemo(
    () =>
      segments
        .sort((a, b) => {
          // Sort by segment groups
          const aIndex = segmentGroups.findIndex((g) => g.indexOf(a.id) !== -1);
          const bIndex = segmentGroups.findIndex((g) => g.indexOf(b.id) !== -1);
          return bIndex - aIndex;
        })
        .map((s) => ({ ...s, type: 'bar' })),
    [segments, segmentGroups]
  );

  return (
    <>
      <div className={cn.stacks}>
        {chartData.map((chart, i) => (
          <div key={i} className={cx(cn.stackWrapper, i === 0 && cn.highlight)}>
            <Chart
              onMouseEnter={handleMouseEnter}
              onMouseLeave={handleMouseLeave}
              scaling={scaling}
              segments={segments}
              segmentGroups={segmentGroups}
              highlighted={tooltip?.data}
              id={id}
              unit={unit}
              max={max}
              {...chart}
            />
          </div>
        ))}
      </div>
      <MixedLegend items={legendItems} />
    </>
  );
};

export default withGraphFrame(memo(VerticalStacks));
