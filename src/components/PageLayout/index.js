import { useRef } from 'react';
import { useRouter } from 'next/router';
import cx from 'classnames';
import cn from './style.module.scss';
import gridCn from 'src/style/grid.module.scss';
import typoCn from 'src/style/typography.module.scss';
import buttonCn from 'src/style/button.module.scss';
import NavBar from 'src/components/controls/NavBar';
import InfoBoxes from 'src/components/InfoBoxes';
import BottomNavigation from './BottomNavigation';
import CountryHero from './CountryHero';
import Footnotes from '../Footnotes';
import { formatMonthOfYear } from 'src/utils';
import Link from 'next/link';

const layout = ({ pages = [], className, level = 1 } = {}) => {
  const Layout = ({ siteContent, country, children, meta, page }) => {
    const contentRef = useRef(null);
    const router = useRouter();
    const routeSegments = router.asPath
      .replace(/#.*$/, '') // Remove hash segment
      .split('/') // Split path segments
      .filter((d) => d); // Remove start and end segments
    const basePath = `/${routeSegments.slice(0, level).join('/')}`; // Complete path without last item

    const countryPages = pages.filter(
      (page) => page.page !== 'sectors' || country.hasDetailedAnalysis
    );

    return (
      <>
        {country && <CountryHero siteContent={siteContent} country={country} />}
        <main id="main" ref={contentRef} className={cx(className)}>
          {!!pages.length && (
            <div className={cx(gridCn.container, cn.pageHeader)}>
              <div className={cn.navBarWrapper}>
                {country && (
                  <>
                    <span className={cx(typoCn.metaBas, typoCn.l, cn.navBarName)}>
                      {country.name}
                    </span>
                    <span className={cx('icon-arrow-right', cn.navBarIcon)} />
                  </>
                )}
                <NavBar basePath={basePath} current={page} pages={countryPages} size="m" />
              </div>
              {country?.content.last_update && (
                <div className={cn.pageMeta}>
                  <Link href="/about/#how-to-cite">
                    <a className={cx(buttonCn.bare, buttonCn.s, cn.metaLink)}>
                      {siteContent.how_to_cite_label}
                    </a>
                  </Link>
                  <span className={cn.lastUpdate}>
                    {siteContent.last_update_label}:{' '}
                    {formatMonthOfYear(country.content.last_update)}
                  </span>
                </div>
              )}
            </div>
          )}
          {children}
          <BottomNavigation
            prevLabel={siteContent.prev_label}
            backLabel={siteContent.back_label}
            basePath={basePath}
            pages={countryPages}
            current={page}
          />
          <Footnotes contentRef={contentRef} siteContent={siteContent} />
          <InfoBoxes contentRef={contentRef} siteContent={siteContent} meta={meta} />
        </main>
      </>
    );
  };

  return Layout;
};

export default layout;
