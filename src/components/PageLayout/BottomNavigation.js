import Link from 'next/link';
import gridCn from 'src/style/grid.module.scss';
import cn from './style.module.scss';
import cx from 'classnames';
import { useSiteContent } from 'src/data/siteContent';

const BottomNavigation = ({ pages, current, basePath }) => {
  if (!pages || !pages.length) return null;
  const { next_label, back_label, back_to_start_label } = useSiteContent();
  const currentIndex = pages.findIndex((page) => page.page === current);
  const backPage = currentIndex !== 0 && pages[currentIndex - 1];
  const nextPage = currentIndex < pages.length - 1 && pages[currentIndex + 1];
  const isFirstPage = !backPage;
  const isLastPage = !nextPage;

  return (
    <div className={cx(gridCn.container, cn.bottomNavWrapper)}>
      <div className={cx(cn.bottomNav, isFirstPage && cn.isFirstPage)}>
        {backPage && (
          <Link href={`${basePath}/${backPage.slug}`}>
            <a className={cn.bottomNavLink}>
              <span className={cx('icon-arrow-left', cn.bottomNavIcon)} />
              <span className={cn.bottomNavText}>
                <span className={cn.bottomNavMeta}>{back_label}</span>
                <span className={cn.bottomNavName}>{backPage.name}</span>
              </span>
            </a>
          </Link>
        )}
        {nextPage && (
          <Link href={`${basePath}/${nextPage.slug}`}>
            <a className={cn.bottomNavLink}>
              <span className={cn.bottomNavText}>
                <span className={cn.bottomNavMeta}>{next_label}</span>
                <span className={cn.bottomNavName}>{nextPage.name}</span>
              </span>
              <span className={cx('icon-arrow-right', cn.bottomNavIcon)} />
            </a>
          </Link>
        )}
        {isLastPage && (
          <Link href={`${basePath}/${pages[0].slug}`}>
            <a className={cn.bottomNavLink}>
              <span className={cn.bottomNavText}>
                <span className={cn.bottomNavMeta}>{back_to_start_label}</span>
                <span className={cn.bottomNavName}>{pages[0].name}</span>
              </span>
              <span className={cx('icon-arrow-right', cn.bottomNavIcon)} />
            </a>
          </Link>
        )}
      </div>
    </div>
  );
};

export default BottomNavigation;
