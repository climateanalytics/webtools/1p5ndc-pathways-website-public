import cn from './style.module.scss';
import Template from 'src/components/content/Template';
import gridCn from 'src/style/grid.module.scss';
import cx from 'classnames';

/*
 * @param profileImage Boolean value to indicate that this is the static version of
 * the hero for the open graph head image
 */
const CountryHero = ({ country, siteContent, profileImage = false }) => {
  return (
    <div
      className={cx(gridCn.wrapper, cn.hero, profileImage && cn.isProfileImage)}
      style={{ backgroundImage: `url(/static/hero-${country.heroImage}.jpg)` }}>
      <div className={cx(gridCn.grid, cn.heroContent)}>
        <div className={cx(gridCn.col6, gridCn.lCol7, gridCn.mCol9)}>
          <h2 className={cn.tagline}>
            {country.name} <span className="icon-arrow-right" /> {siteContent.name}
          </h2>
          <Template
            className={cn.leadQuestion}
            tag="p"
            data={{ country }}
            html={siteContent.lead_question}
          />
        </div>
      </div>
    </div>
  );
};

export default CountryHero;
