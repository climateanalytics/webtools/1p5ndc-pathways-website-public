import btnCn from 'src/style/button.module.scss';
import cn from './style.module.scss';
import cx from 'classnames';
import Link from 'next/link';
import { useSiteContent } from 'src/data/siteContent';

const MethodologyLink = ({ data }) => {
  const methodology = data.methodology && data.methodology[0];
  const siteContent = useSiteContent();

  return methodology ? (
    <Link href={`/methodology#${methodology.id}`}>
      <a className={cx(cn.methodologyLink, btnCn.bare, btnCn.s, btnCn.iconBefore)}>
        <span className={'icon-file-text'} />
        {siteContent.methodology_label}
      </a>
    </Link>
  ) : null;
};

export default MethodologyLink;
