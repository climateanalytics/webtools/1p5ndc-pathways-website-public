import cn from './style.module.scss';
import Html from 'src/components/content/Html';
import gridCn from 'src/style/grid.module.scss';
import btnCn from 'src/style/button.module.scss';
import cx from 'classnames';
import { useEffect, useRef, useState } from 'react';
import { genitivify } from 'src/utils';
import Link from 'next/link';
import { renderTemplate } from 'src/components/content/Template';
import SimpleSelect from 'src/components/controls/SimpleSelect';

const LandingHero = ({ countryGroups, siteContent }) => {
  const [country, setCountry] = useState(countryGroups[0].children[0]);
  const [hero, setHero] = useState(null);

  useEffect(() => {
    const flatCountries = countryGroups
      .reduce((acc, group) => acc.concat(group.children), [])
      .filter((c) => c.name.length < 12)
      .filter((c) => !c.disabled);

    const index = Math.floor(Math.random() * flatCountries.length);
    setCountry(flatCountries[index]);
    setHero(Math.ceil(Math.random() * 5));
  }, [countryGroups]);

  return (
    <div
      className={cx(cn.wrapper, gridCn.wrapper)}
      style={{ backgroundImage: hero && `url(/static/hero-${hero}.jpg)` }}>
      <a className={cn.heroImageAttribution} href="https://www.over-view.com/">
        {siteContent.image_attribution_text} <u>Overview</u>
      </a>
      <div className={cx(gridCn.col12, cn.leadQuestionWrapper)}>
        <div className={cn.leadQuestion}>
          <Html html={siteContent.lead_question_start} tag="span" />{' '}
          <Link href={`/countries/${country.slug}`}>
            <a className={cn.leadQuestionCountry}>{genitivify(country.alt_name)}</a>
          </Link>{' '}
          <Html html={siteContent.lead_question_end} tag="span" />
        </div>
        <div className={gridCn.sColAlignLeft}>
          <a href="#all-countries" className={cx(btnCn.cta, btnCn.l, cn.leadCtaButtonWrapper)}>
            {siteContent.all_countries_cta}
          </a>
        </div>
      </div>
    </div>
  );
};

export default LandingHero;
