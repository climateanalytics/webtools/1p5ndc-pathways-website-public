import { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { slugify } from 'src/utils';

const createHierarchy = (flatItems) => {
  if (!flatItems.length) return [];
  let startLevel = Infinity;

  // Set start level in case it doesn't start at 1
  flatItems.forEach((item) => {
    if (item.level < startLevel) {
      startLevel = item.level;
    }
  });
  const createLevel = (items, level) => {
    if (level > 3) return [];
    const levelIndexes = [];
    for (let i = 0; i < items.length; i += 1) {
      if (items[i].level === level) {
        levelIndexes.push(i);
      }
    }

    // If no subindexes are found
    if (!levelIndexes.length) {
      return level === startLevel ? createLevel(items, level + 1) : items;
    }

    // If there are subindexes, create
    return levelIndexes.map((startIndex, i) => {
      const endIndex = i < levelIndexes.length ? levelIndexes[i + 1] : items.length;
      const subItems = items.slice(startIndex + 1, endIndex);
      const item = {
        ...items[startIndex]
      };
      if (subItems.length) {
        item.children = createLevel(subItems, level + 1);
      }
      return item;
    });
  };

  return createLevel(flatItems, startLevel);
};

const useContentHierarchy = (ref, { minLevel = 2, maxLevel = 6 } = {}) => {
  const [toc, setToc] = useState(null);
  const { asPath } = useRouter();
  useEffect(() => {
    const levels = Array(maxLevel + 1 - minLevel)
      .fill(0)
      .map((n, i) => `h${minLevel + i}`)
      .join(', ');

    const headings = ref.current.querySelectorAll(levels);

    const flatToc = [...headings].map((el) => ({
      name: el.innerText,
      level: parseFloat(el.tagName[1]),
      slug: el.getAttribute('id') || slugify(el.innerText)
    }));

    setToc(createHierarchy(flatToc));
  }, [asPath]);
  return toc;
};

export default useContentHierarchy;
