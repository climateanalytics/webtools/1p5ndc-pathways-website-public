import { useCallback, useEffect, useState } from 'react';

const useScrollWrapper = (node) => {
  const [wrapperNode, setWrapperNode] = useState(node);
  const [overflow, setOverflow] = useState([false, false]);
  const [hasInteracted, setHasInteracted] = useState(false);

  const wrapperRef = useCallback((wrapperNode) => {
    setWrapperNode(wrapperNode);
  }, []);

  useEffect(() => node && setWrapperNode(node), [node]);

  useEffect(() => {
    if (!wrapperNode) return;

    const handleScroll = () => {
      const tableEl = wrapperNode.firstChild;
      const wrapperEl = wrapperNode;

      if (tableEl.scrollWidth <= wrapperEl.clientWidth) setOverflow([false, false]);
      else if (wrapperEl.scrollLeft === 0) setOverflow([false, true]);
      else if (wrapperEl.scrollLeft + wrapperEl.clientWidth >= tableEl.scrollWidth) {
        setOverflow([true, false]);
        setHasInteracted(true);
      } else {
        setOverflow([true, true]);
        setHasInteracted(true);
      }
    };

    wrapperNode.addEventListener('scroll', handleScroll);

    handleScroll();

    return () => {
      wrapperNode.removeEventListener('scroll', handleScroll);
    };
  }, [wrapperNode]);

  return [wrapperRef, overflow, hasInteracted];
};

export default useScrollWrapper;
