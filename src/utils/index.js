import { useState, useEffect, useMemo, useRef } from 'react';
import { format as d3Format, formatLocale as d3FormatLocale } from 'd3-format';
import { timeFormat } from 'd3-time-format';
import { useTheme } from 'src/style/ThemeProvider';

export const useDeviceInfo = () => {
  const [deviceInfo, setDeviceInfo] = useState({ isXS: false });
  const theme = useTheme();
  useEffect(() => {
    const breakpoint = parseInt(theme.size.breakpoint.xs.replace('px', ''));
    setDeviceInfo((info) => ({ ...info, isXS: window.outerWidth < breakpoint }));
  }, []);
  return deviceInfo;
};

export const useIsMounted = () => {
  const mounted = useRef(false);

  useEffect(() => {
    mounted.current = true;
    return () => {
      mounted.current = false;
    };
  }, []);
  return mounted.current;
};

export const debounce = (func, wait = 100, immediate) => {
  var timeout;
  return function () {
    var context = this,
      args = arguments;
    clearTimeout(timeout);
    timeout = setTimeout(function () {
      timeout = null;
      if (!immediate) func.apply(context, args);
    }, wait);
    if (immediate && !timeout) func.apply(context, args);
  };
};

export function hexToRgba(hex, a = 1) {
  var c;
  if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
    c = hex.substring(1).split('');
    if (c.length == 3) {
      c = [c[0], c[0], c[1], c[1], c[2], c[2]];
    }
    c = '0x' + c.join('');
    return 'rgba(' + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',') + ',' + a + ')';
  }
  throw new Error('Bad Hex');
}

/*
 * Generic data manipulation
 */

/*
 * Transforms an array of objects into an object where the keys are determined by the "by" paramter
 */
export const reduceBy = (arr, by = 'id') => {
  return arr.reduce((acc, d) => ({ ...acc, [d[by]]: d }), {});
};

export const groupBy = (arr, by = 'id') => {
  return arr.reduce((acc, d) => {
    const existing = acc[d[by]];
    if (existing) existing.push(d);
    else acc[d[by]] = [d];
    return acc;
  }, {});
};

/*
 * Takes an array of objects with a value property and returns an object where
 * all the values have been added up by the `by` parameter
 */
export const sumBy = (data, by = 'id') => {
  return data.reduce((acc, item) => {
    const key = item[by];
    return {
      ...acc,
      [key]: acc[key] ? acc[key] + item.value : item.value
    };
  }, {});
};

/*
 * Remove duplicate occurances of objects in an array that have the same value for the specified key.
 * The first instance that is encountered will be kept.
 */
export const uniqBy = (arr, by = 'id') => {
  const obj = arr.reduce((acc, d) => {
    const key = d[by];
    if (acc[key]) return acc;
    acc[key] = d;
    return acc;
  }, {});
  return Object.values(obj);
};

export const uniq = (value, index, self) => {
  return self.indexOf(value) === index;
};

export const uniqify = (arr) => arr.filter(uniq);

export const sortByName = (a, b) => {
  if (a.name > b.name) return 1;
  if (a.name < b.name) return -1;
  return 0;
};

/*
 * Formatters
 */
export const thinsp = ' ';
export const hairsp = ' '; // \u200A
export const dash = '–';

var customLocale = {
  thousands: ' ',
  decimal: '.',
  grouping: [3],
  currency: ['$', '']
};

var customFormatter = d3FormatLocale(customLocale);

export const roundNumber = customFormatter.format(',.0f');
export const formatDecimal = customFormatter.format(',.1n');

export const dynamicallyRoundNumber = (value) => {
  if (Math.abs(value) < 0.01) return '0';
  if (Math.abs(value) < 5 && value % 1) return formatDecimal(value);
  return roundNumber(value);
};

export const formatPercent = d3Format('.0%');

export function renderValue(value, { unit, format, addUnit = true, signed } = {}) {
  if (value === undefined) return null;
  const formatter = unit?.format || format || roundNumber;

  const formatted = formatter(value);
  const unitName = unit?.name || '';
  const sign = signed && value > 0 ? '+' : '';
  const suffix = addUnit && unit ? `${unitName}` : '';
  return `${sign}${formatted}${suffix}`;
}

export function renderRange(values, { unit, addUnit = true, separator = ` to ` } = {}) {
  let formattedValues = values
    .filter((d) => d !== undefined)
    .map((v) => (unit?.format ? unit.format(v) : roundNumber(v)));
  if (formattedValues[0] === formattedValues[1]) formattedValues = [formattedValues[0]];
  const unitName = unit?.name || '';
  const suffix = addUnit && unit ? `${unitName}` : '';
  return `${formattedValues.join(separator)}${suffix}`;
}

const monthOfYearFormatter = timeFormat('%B %Y');

export const formatMonthOfYear = (d) => monthOfYearFormatter(new Date(d));

export const slugify = (name) =>
  name
    .toString()
    .trim()
    .toLowerCase()
    .replace(/ô|ö|ò|ó/g, 'o')
    .replace(/è|é|ë/g, 'e')
    .replace(/ü/g, 'u')
    .replace(/ï|ì|í/g, 'i')
    .replace(/\W+/g, '-')
    .replace(/^\W|\W$/g, '');

export const genitivify = (str) => str + (str.endsWith('s') ? 'ʼ' : 'ʼs');

export const useSeriesTicks = (width, domain) => {
  return useMemo(() => {
    let ticks =
      width > 700 && domain
        ? [1990, 2000, 2010, 2020, 2030, 2040, 2050, 2060, 2070]
        : [1990, 2010, 2030, 2050, 2070];
    return domain ? ticks.filter((t) => t >= domain[0] && t <= domain[1]) : ticks;
  });
};

/*
 * URL utility
 */

/*
 * Creates a url instance from a baseurl or a base path and generic object which is used to create the url query.
 * @param obj: The object to turn into the url query
 * @param theKeys: If not the whole object should be used to create the query, an optional array of key ids can be passed.
 * @return: If a baseUrl was set, returns the full url object, if only basePath was set, returns the base path with
 * the search query appended
 */
export const createUrl = ({ url, path = '', params = {} }) => {
  const baseUrl = url || `http://blank.com`;
  const urlObj = new URL(`${baseUrl}${path}`);
  const keys = Object.keys(params);
  keys.forEach((key) => {
    const value = params[key] ?? null;
    if (value !== null) urlObj.searchParams.set(key, value);
  });
  return url ? urlObj.href : `${urlObj.pathname}${urlObj.search}`;
};

const parseQueryParam = (value) => {
  let parsed = value;
  if (value === 'false') parsed = false;
  if (value === 'null') parsed = null;
  if (value.length && value.replace(/^\d+/g, '').length === 0) parsed = parseFloat(value);
  return parsed;
};

export const parseQueryObject = (query) => {
  return Object.entries(query).reduce((acc, [key, value]) => {
    acc[key] = Array.isArray(value) ? value.map(parseQueryParam) : parseQueryParam(value);
    return acc;
  }, {});
};

export const isIframe = () => {
  try {
    return window.self !== window.top;
  } catch (e) {
    return true;
  }
};
