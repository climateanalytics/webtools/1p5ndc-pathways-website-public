import { useReducer, useCallback } from 'react';

/* 
 A very simple state reducer hook who's dispatch can be handed down a component in order to 
 receivereactive state changes (vs. having to manually manage state in the upper component).
 Basically a reducer that simply allows for setting new values without any logic.

const Child = (reducer) => {
  return (
    <>
    <input
      type="range"
      value={reducer.state.a}
      onChange={(e) => reducer.dispatch('a', e.value)}
    />
    <input
      type="range"
      value={reducer.state.b}
      onChange={(e) => reducer.dispatch('b', e.value)}
    />
    </>
  );
}

const Parent = () => {
  const reducer = useUiState({a: 1, b: 2});

  return <Child reducer={reducer} />
}

*/

function defaultReducer(state, action) {
  return { ...state, [action.key]: action.value };
}

const useUiReducer = (initialState, reducer = defaultReducer) => {
  const [state, reducerDispatch] = useReducer(reducer, initialState);
  const dispatch = useCallback(
    (value, id) => reducerDispatch({ value, key: id }),
    [reducerDispatch]
  );

  return { state, dispatch };
};

export default useUiReducer;
