import { useState, useEffect } from 'react';

const callbacks = [];

const addMatomoListener = (listener) => {
  if (!callbacks.includes(listener)) callbacks.push(listener);
};

const removeMatomoListener = (listener) => {
  const index = callbacks.indexOf(listener);
  if (index >= 0) callbacks.splice(index, 1);
};

if (typeof window !== 'undefined') {
  window.matomoAsyncInit = function () {
    try {
      callbacks.forEach((cb) => {
        window.Matomo && cb(window.Matomo);
      });
    } catch (err) {
      console.error(err);
    }
  };
}

const useMatomoInstance = () => {
  const [matomo, setMatomo] = useState();

  //window.Matomo.getTracker('http://climateanalytics.org/stats/matomo.php', 8);
  useEffect(() => {
    addMatomoListener(setMatomo);
    return () => removeMatomoListener(setMatomo);
  }, []);
  return matomo;
};

export default useMatomoInstance;
