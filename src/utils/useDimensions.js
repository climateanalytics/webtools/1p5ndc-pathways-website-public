import { useState, useEffect, useCallback } from 'react';
import { debounce } from 'src/utils';

const useDimensions = function (initialSize = { width: null, height: null }) {
  const [size, setSize] = useState(initialSize);
  const [node, setNode] = useState(null);
  const [initialized, setInitialized] = useState(false);

  const ref = useCallback((node) => {
    setNode(node);
  }, []);

  useEffect(() => {
    const handleResize = () => {
      window.requestAnimationFrame(() => {
        if (node) {
          setSize({
            width: node.clientWidth,
            height: node.clientHeight
          });
        }
      });
    };

    const debouncedHandleResize = debounce(handleResize, 100);

    handleResize();

    window.addEventListener('resize', debouncedHandleResize);
    window.addEventListener('orientationchange', debouncedHandleResize);

    return () => {
      window.removeEventListener('resize', debouncedHandleResize);
      window.removeEventListener('orientationchange', debouncedHandleResize);
    };
  }, [node]);

  useEffect(() => {
    if (size.width !== initialSize.width && size.height !== initialSize.height && !initialized) {
      setInitialized(true);
    }
  }, [size, initialSize, initialized]);

  return [{ ref, style: initialized ? {} : { opacity: 0 } }, size, node];
};

export default useDimensions;
