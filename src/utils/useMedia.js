import { useEffect, useMemo, useState } from 'react';
import { useTheme } from 'src/style/ThemeProvider';
import { debounce } from 'src/utils';

export default function useMedia({ values, defaultValue }) {
  const theme = useTheme();
  const queries = useMemo(() =>
    Object.values(theme.size.breakpoint)
      .map((d) => parseInt(d.replace('px', '')))
      .sort((a, b) => b - a)
      .map((d) => `(min-width: ${d}px)`)
  );

  const match = () => {
    const val = values[queries.findIndex((q) => matchMedia(q).matches)] ?? defaultValue;
    return val;
  };
  const [value, set] = useState(defaultValue);

  useEffect(() => {
    const handler = debounce(() => set(match), 50);
    window.addEventListener('resize', handler);
    set(match);

    return () => window.removeEventListener('resize', handler);
  }, [queries]);
  return value;
}

export const useIsMobile = () => useMedia({ values: [false, false, false], defaultValue: true });
