import { useState, useCallback, useEffect } from 'react';

const arrowKeys = [37, 38, 39, 40];
const increaseKeys = [38, 39];
const decreaseKeys = [37, 40];

/*
 * Manages focus for tabbed controls (Navigation, Segmented control, tablist)
 */
const useTabFocus = ({ items, selector }) => {
  const [focusedIndex, setFocusedIndex] = useState(-1); // -1 prevents focus from being on element on page load
  const [node, setNode] = useState(null);
  const onKeyDown = useCallback(
    (e) => {
      // Move right
      if (arrowKeys.includes(e.keyCode)) {
        if (increaseKeys.includes(e.keyCode)) {
          setFocusedIndex((f) => {
            if (focusedIndex === -1) return 1;
            return f >= items.length - 1 ? 0 : f + 1;
          });
          // Move left
        } else if (decreaseKeys.includes(e.keyCode)) {
          setFocusedIndex((f) => (f <= 0 ? items.length - 1 : f - 1));
        }
        e.preventDefault();
      }
      if (e.keyCode === 9) {
        // Doesn't work with onBlur, since it will be triggered every time focus changes
        // stopping propagation didn't work either...
        setFocusedIndex(-1);
      }
    },
    [items, focusedIndex]
  );

  const ref = useCallback((node) => {
    setNode(node);
  }, []);

  const onFocus = useCallback(() => {
    setFocusedIndex((focusedIndex) => (focusedIndex === -1 ? 0 : focusedIndex));
  }, [node]);

  useEffect(() => {
    if (!node) return;
    const tabElements = node.querySelectorAll(selector);
    const focusedElement = tabElements[focusedIndex];
    if (!focusedElement) return; // If element is not focused, focusedIndex is -1
    Array.from(tabElements).forEach((el) => el.setAttribute('tabindex', -1));
    focusedElement.setAttribute('tabindex', 0);
    focusedElement.focus();
  }, [selector, node, focusedIndex]);

  return { ref, onKeyDown, onFocus, node, tabIndex: -1 };
};

export default useTabFocus;
