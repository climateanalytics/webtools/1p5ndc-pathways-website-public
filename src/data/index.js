import fetch from 'node-fetch';
import textile from 'textile-js';
import siteCopy from './copy';
import { uniqBy, uniq } from 'src/utils';

const getContentUrl = (endpoint, filters = []) => {
  let filterString = filters.map((f) => `filter[${f.field}]=${f.value}`).join('&');
  if (filters.length) filterString = `&${filterString}`;
  return `${process.env.contentApiUrl}${endpoint}?token=${process.env.contentApiToken}${filterString}`;
};

const getDataUrl = (country) => {
  return `${process.env.dataApiUrl}/${country}.json`;
};

const cache = {};

async function fetchOrRetrieve(url) {
  if (!cache[url]) {
    try {
      const request = await fetch(url);
      cache[url] = await request.json();
    } catch (e) {
      console.error('Error fetching data or content', url);
      console.error(e);
    }
  }

  return cache[url];
}

/*
 * Data fetching functions
 */

const contentParsers = {
  list_textile: (raw, key, blankLinks) => {
    try {
      return JSON.parse(raw).map((values) => {
        let parsedHtml = textile(values[1] || values[0]);
        // Fix image paths and media links which come from redaxo
        let content = parsedHtml.replace(
          /(<img\s.*src=")(\/.*)(".*>)/g,
          `$1${process.env.mediaUrl}media/w600$2$3`
        );

        content = content.replace(
          /(<a\s.*href=")\/media\/(.*)(".*>)/g,
          `$1${process.env.mediaUrl}$2$3`
        );

        // add target=_blank link attribute to open in new tabs
        if (blankLinks) {
          content = content.replace(/<a(\shref[^>]*[^/])>/g, `<a$1 target="_blank">`);
        }

        return {
          id: values[0],
          name: values.length >= 2 ? values[0] : null,
          content
        };
      });
    } catch {
      //console.warn(`Error while parsing ${key} with value: "${raw}"`);
      return [];
    }
  },
  list: (raw) => raw.split(',').filter((d) => d),
  textile: textile,
  number: (raw) => parseFloat(raw),
  checkbox: (raw) => raw === 1
};

const processContent = (
  fields,
  { page, sectionIds = [], contentIds = [], bareSectionIds = [], blankLinks = false }
) => {
  return Object.entries(fields).reduce((acc, [key, value]) => {
    const idSegments = key.split('__');
    if (idSegments.length <= 1) return acc;
    const sectionId = idSegments[0];
    let contentId = idSegments[1];
    const parserId = idSegments[2];
    const matchesPage = page === sectionId;
    const matchesSection = sectionIds.includes(sectionId);
    const matchesBareSection = bareSectionIds.includes(sectionId);
    const matchesContent = contentIds.includes(contentId);
    if (!matchesPage && !matchesSection && !matchesContent && !matchesBareSection) return acc;
    const parser = contentParsers[parserId];
    const content = parser ? parser(value, key, blankLinks) : value.trim();

    if (acc[contentId] !== undefined)
      throw new Error(
        `Two pieces with same ID (${contentId}) encountered while parsing field ${key}.`
      );

    if (matchesSection) {
      acc[sectionId] = acc[sectionId]
        ? { ...acc[sectionId], [contentId]: content }
        : { [contentId]: content };
    } else {
      acc[contentId] = content;
    }

    return acc;
  }, {});
};

const processCountry = (content, data) => {
  return {
    id: content.country__code,
    name: content.country__name,
    alt_name: content.country__alt_name || content.country__name,
    href: `/countries/${data.slug}`,
    heroImage: parseInt(content.country__header_image__number) || 5,
    slug: data.slug,
    disabled: content.status === '0',
    hasDetailedAnalysis: !data.no_sector_analysis,
    published_sectors: content.country__published_sectors__list.split(',')
  };
};

/*
 * META
 */
export async function getMeta() {
  const url = getContentUrl('npe_countrylist');
  const siteContent = await getSiteContent();
  // TODO: Maybe not needed anymore to fetch country list here because we get all the relevant
  // country data from the data url already?
  const countriesContent = await fetchOrRetrieve(url);
  const meta = await fetchOrRetrieve(getDataUrl('meta'));

  const pathways = meta.pathways.map((pathway) => ({
    description: siteContent.pathways.find((p) => p.id === pathway.id)?.content || '',
    ...pathway
  }));

  const countries = meta.countries
    .map((country) => {
      const countryContent = countriesContent.data.find(
        (c) => c.attributes.country__code === country.id
      );
      if (!countryContent) return;
      return {
        ...country,
        ...processCountry(countryContent.attributes, country)
      };
    })
    .filter((d) => d)
    .sort((a, b) => (a.name > b.name ? 1 : -1))
    .sort((a, b) => {
      if (a.disabled && !b.disabled) return 1;
      if (!a.disabled && b.disabled) return -1;
      return 0;
    });

  const countryGroups = meta.country_group.map((group) => {
    return {
      ...group,
      children: countries.filter((c) => c.country_group === group.id)
    };
  });

  const references = meta.references.map((ref) => ({
    ...ref,
    description: textile(ref.description)
  }));

  const units = [...meta.units, { id: 'year' }];

  return {
    country_groups: countryGroups,
    countries,
    emission_target_types: meta.emission_target_types,
    energy_sources: meta.energy_sources,
    gases: meta.gases,
    land_covers: meta.land_covers,
    pathways,
    sectors: meta.sectors,
    units,
    references
  };
}

/*
 * SITE CONTENT
 */
async function getSiteContent(dataSpec = {}) {
  const generalUrl = getContentUrl('npe_overview');
  const response = await fetchOrRetrieve(generalUrl);
  // Make sure all tool wide content is included
  const spec = {
    ...dataSpec,
    bareSectionIds: [...(dataSpec.bareSectionIds || []), 'tool', 'site']
  };
  return processContent({ ...response.data[0].attributes, ...siteCopy }, spec);
}

/*
 * SINGLE PAGE CONTENT
 */
async function getPageContent(dataSpec) {
  const url = getContentUrl('npe_pages', [
    {
      field: 'page__name_id',
      value: dataSpec.page
    }
  ]);

  const response = await fetchOrRetrieve(url);
  return processContent(response.data[0].attributes, {
    contentIds: ['entries', 'name'],
    blankLinks: true,
  });
}

/*
 * COUNTRY
 */

async function getCountry(dataSpec) {
  const { dataIds = [], country } = dataSpec;
  const rawData = await fetchOrRetrieve(getDataUrl(country));
  const { countries } = await getMeta();
  const meta = countries.find((c) => c.slug === country);

  const allDataIds = [
    ...dataIds,
    'country_reference_year',
    'reference_years',
    'references',
    'slug',
    'id'
  ];
  const data = allDataIds.reduce((acc, id) => {
    const d = rawData[id] || null;
    return { ...acc, [id]: d };
  }, {});

  const contentUrl = getContentUrl('npe_countries', [
    { field: 'country__code', value: rawData.id }
  ]);

  const contentResponse = await fetchOrRetrieve(contentUrl);
  const rawContent = contentResponse.data[0].attributes;
  const content = processContent(rawContent, {
    ...dataSpec,
    contentIds: [...(dataSpec.contentIds || []), 'last_update']
  });

  const reference_years = [...data.reference_years, data.country_reference_year]
    .filter(uniq)
    .sort((a, b) => a - b);
  return {
    ...meta,
    country_reference_year: data.country_reference_year,
    reference_years,
    content,
    data
  };
}

/*
 * ALL COUNTRIES
 */

async function getAllCountries(dataSpec) {
  const { countries } = await getMeta();
  const dataId = dataSpec.dataIds[0];
  const dataPromises = countries.map((country) => fetchOrRetrieve(getDataUrl(country.slug)));
  const rawData = await Promise.all(dataPromises);
  return rawData.map((country) => ({
    ...country,
    ...countries.find((c) => c.id === country.id),
    data: { [dataId]: country[dataId] }
  }));
}

/*
ACTIVITIES CONTENT
*/
const getActivitiesContent = async () => {
  const contentResponse = await fetchOrRetrieve(getContentUrl('npe_activities'));

  const data = contentResponse.data
    .map((d) => d.attributes)
    .map((d) => ({
      title: d.title__text,
      date: d.date__date,
      abstract: textile(d.abstract__textile || ''),
      link: d.link__text || '',
      type: d.type__list.split(',')[0] || null,
      image: d.image__image
    }));
  return data;
};

const apiFunctions = {
  siteContent: getSiteContent,
  meta: getMeta,
  pageContent: getPageContent,
  country: getCountry,
  allCountries: getAllCountries,
  activitiesContent: getActivitiesContent
};

/* 
@param endpoints {array} an array of endpoints to query from. Available endpoints are "siteContent", 
"pageContent", "meta" and "country".

siteContent -> Endpoint for site wide redaxo content such as info boxes
pageContent -> Endpoint for static sites such as "about" or "methodology"
meta -> All the metadata such as list of countries, sectors, pathways etc
country -> Endpoint to get data as well as content for a country

siteContent and pageContent can define a couple of additional arrays of ids for fields to be included. This is mainly 
so we don't have load all the data for all the countries in order to reduce the amount of data that needs to be loaded 
on each page.

Ids of redaxo fields are more than just IDs. Since the capabilities of redaxo are limited, we encode some structure
within the IDs. They consist of three parts section__field-id__type. These parts are used here to only inlude specific
sections and to process certain types correctly. 

sectionIds -> Section ids are grouped together into a new object with the id as key. This is so they can be accessed
later on as a whole instead of having to pick individual ids from the content. This is mainly used for the infoboxes

bareSectionIds -> Same as sectioIds except that they won't be grouped together but are put directly on the content object

contentIds ->

The "country" endpoint can define a dataIds array and a page property. dataIds are an array of ids that should be included
from the data. The page property is there to only include content that belongs on this specific country page (in_brief, 
current_situation, ambition_gab, sectors)

@param page the page parameter is used in the "country" and "pageContent" endpoints to filter out required data (country) 
or query the correct page (pageContent). Alternatively the page parameter can also be defined on the endpoint directly

 */

export async function getData(dataSpec = { endpoints: [], page: null, slug: null }) {
  const endpoints = uniqBy([...(dataSpec.endpoints || []), { id: 'meta' }, { id: 'siteContent' }]);

  const promises = endpoints.map((endpoint) => {
    return apiFunctions[endpoint.id]({ ...dataSpec, ...endpoint });
  });

  // Prepare object to be passed to page
  const dataSpecProps = { ...dataSpec };
  delete dataSpecProps.endpoints;

  const responses = await Promise.all(promises);
  const props = responses.reduce((acc, response, i) => {
    const endpoint = endpoints[i].id;
    acc[endpoint] = response;
    return acc;
  }, dataSpecProps);
  return { props };
}

/*
 * Used to generate url path slugs
 */
export async function getCountryPaths(processFn) {
  const { countries } = await getMeta();

  const paths = countries
    .map((country) => ({
      params: { country: processFn ? processFn(country) : country.slug }
    }))
    .filter((path) => path.params.country);

  return {
    paths,
    fallback: false
  };
}
