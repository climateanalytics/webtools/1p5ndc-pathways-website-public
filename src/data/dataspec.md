# Dataspec

This document describes the data requirements for the data coming from climate analytics. The API exposes three main endpoints.

## `/meta`

The `/meta` endpoint is used as source of truth for all relevant metadata such as sectors, pathways, energy sources etc. Amongst other things it defines IDs and labels for entities that will be used accross the whole tool (content coming from redaxo, climate data coming from CA as well as the web application). This way you have complete control over them. If for example a pathway changes, you can make the required changes in the meta data, adjust the country data where needed and even add/remove the pathway in redaxo. They will all be linked via their ID. The same goes for the sectors or any other metadata. The ids will be used to color/highlight certain parts of text or to specify sectoral targets in the _Current situation_ section.

Notes:

**`country.no_sector_analysis`**: This is a special property used for Gambia and another country I cannot remember where there is no sectoral analysis available. In order to catch this during the build phase of the pages, I need this information here.

**`sectors`**: Since the sectors have different, structures throughout the tool that are difficult to make consistent, we will use a flat list of sectors. The required sectors in certain parts of the web application will be hard coded, assuming they won't change any time soon.

```js
{
  "pathways": [
    {
      "id": String,
      "name": String,
      "iam_name": String
    }
  ],
  "countries": [
    {
      "id": String,
      "name": String,
      "country_group": String,
      "no_sector_analysis": Boolean,
      "slug": String
    }
  ],
  "country_groups": [
    {
      "id": String,
      "name": String
    }
  ],
  "sectors": [
    {
      "id": String,
      "name": String,
    }
  ],
  "energy_sources": [
    {
      "id": String,
      "name": String
    }
  ],
  "land_covers": [
    {
      "id": String,
      "name": String
    }
  ],
  "gases": [
    {
      "id": String,
      "name": String,
      "name_short": String
    }
  ],
  "units": [
    {
      "id": String,
      "name": String,
      "name_short": String
    }
  ],
  "references": [
    {
      "id": String,
      "name": String,
      "description": String
    }
  ]
}
```

## `/data/<country_id>.json`

This is the endpoint to query all the data for a specific country. For the most part, the structure is the same as in the first phase. There are however a few important changes:

**`names`**: I tried to make as much of the texts and labels configureable from your side. This means that there will be an extra effort from your side to manage these. On the upside, we can bring much more of what will be shown on the page in one place which means you become much more independent of me and can make changes to labels without any of my help. Also it makes it much easier in case there will be the need for a second language in the future. For this reason, every data object has a `names` property. This property is a simple dictionary that links specific ids in the data to a corresponding label. Most of them should be self explanatory. Some of them might need clarification on where they will show up.

**`title`**: In the same effort to make more of the text content more configureable, each data object has a `title` and `secondary_title` property. Since the titles most of the time are dynamic and include at least the country, the JSON files only contain a template. The website uses [this](https://squirrelly.js.org/) templating library, for which a title looks like this: `Total GHG emissions for {{country.name}}`. In each template a `country` object will be available. This object contains basically the whole data object that is provided via the api. Additionally it has the following properties:

- `name` (String) e.g. "United States"

- `alt_name` (String) e.g. "The United States" which is the correct form to use in some contexts as in "The United State's GHG emissions"

**`secondary_title`**: All graphical plots can add a short text next or below the title, which is reserved for primarily showing the unit. Whether the template interpolates the `unit` variable is still under discussion.

**`caption`**: This is a more detailed description of what the graph shows. Normally this will show up when clicking on the info button. For some charts (such as the `keyfacts_` or `historic_emissions_breakdosn`) it is rendered directly under the title.

**`disclaimers`**: The keyfacts tables have some additional disclaimers which will be placed underneath (see design). For this reason we should introduce a `disclaimers` array that is simply an array of strings to be rendered. Other plots might also benefit from such a `disclaimers` entry and we found text to add to `disclaimers` for `primary_energy_mix` (the text that was previously `caption_other` for the EWG LUT special case).

**`reference_years`**: The reference years that should be used for all countries

**`country_reference_year`**: The reference year that should be used for this country specifically

The top level structure remains unchainged:

```js
  {
    "id": String,
    "reference_years": [Number],
    "country_reference_year": Number,
    "references": {...},
    "total_ghg_emissions": {...},
    "primary_energy_mix": {...},
    "historic_emissions_breakdown": {...},
    "power_sector_timeline": {...},
    "total_co2_emissions": {...},
    "keyfacts_emissions": {...},
    "sectoral_emissions": {...},
    "power_emissions_intensity": {...},
    "power_energy_mix": {...},
    "power_investments": {...},
    "power_investments_change": {...}
    "keyfacts_power": {...},
    "buildings_emissions_intensity": {...},
    "buildings_energy_mix": {...},
    "keyfacts_buildings": {...},
    "transport_emissions_intensity": {...},
    "transport_energy_mix": {...},
    "keyfacts_transport": {...},
    "industry_emissions_intensity": {...},
    "industry_energy_mix": {...},
    "keyfacts_industry": {...},
    "lulucf_forest": {...},
    "lulucf_forest": {...},
    "lulucf_land_use": {...},
    "lulucf_sector_timeline": {...}
  }
```

### Series

- `total_ghg_emissions`
- `total_co2_emissions`
- `industry_processes_emissions`
- `lulucf_emissions`
- `lulucf_forest`

`data_keys`: A list of generic ids that are present in the data to be used to create the timesseries. This will be used to generate generic timeseries where the keys are not known in advance by the code generating the graph. In addition to the `data_keys` additional, arbitrary keys/values can be set on the data (such as `cpp_min` or `range_midline`) which will be processed in a specific way by the frontend code.

`ndcs`: An array to specify the ndcs present and to indicator the order (priority) of the ndcs in case there is a conditional and unconditional ndc.

`ndc_conditional/ndc_unconditional`: This indicates from which value the emissions gap should be calculated. This will be the same value as the min of the same ndc, thus redundant. Maybe to deprecate.

`cpp`, `cpp_min`, `cpp_max`: Depending on whether the current policy pathway is given as a range or a single pathway, either `cpp_max` and `cpp_min` are provided or `cpp` only.

`names`: Quite a lot of names required for this one. Most of them should be clear. `pathways` is used in the legend to describe the 1.5° compatible pathway lines. `reference_year` is used in the ndc chart to create the label for the reference line.

```js
"total_ghg_emissions": {
  "data": [
    {
      "year": Number,
      "historical": Number,
      "range_min": Number,
      "range_midline": Number,
      "range_max": Number,
      "cpp_min": Number,
      "cpp_max": Number,
      "cpp": Number,
      "<pathway_id>": Number
    }
  ],
  "unit": <unit_id>,
  "data_keys": [<pathway_id>],
  "netzero": Number,
  "ndcs": [
    {
      "id": <ndc_id>,
      "year": 2030,
      "name": String,
      "value": Number,
      "min": Number,
      "max": Number
    }
  ]
  "names": {
    "range_min": String,
    "range_max": String,
    "range_midline": String,
    "pathways": String,
    "cpp": String,
    "cpp_min": String,
    "cpp_max": String,
    "historical": String,
    "netzero": String,
    "emissions_gap": String,
    "reference_year": Template
  },
  "title": Template,
  "secondary_title": Template,
  "caption": Template,
  "references": [
    {
      "id": <reference_id>
    }
  ],
  "methodology": [
    {
      "name": String,
      "id": <methodology_section_id>
    }
  ]
}
```

```js
"total_co2_emissions": {
  data: [
    {
      "year": Number,
      "historical": Number,
      "range_min": Number,
      "range_midline": Number,
      "range_max": Number,
      <pathway_id>: Number
    }
  ],
  "unit": <unit_id>,
  "data_keys": [<pathway_id>],
  "title": Template,
  "secondary_title": Template,
  "names": {
    "range_min": String,
    "range_max": String,
    "range_midline": String,
    "historical": String,
  },
  "caption": Template,
  "references": [
    {
      "id": String
    }
  ],
  "methodology": [{name: String, id: <methodology_section_id>}]
}
```

### Nested series

This datastructure is used for timeseries data that includes more than one selectable timeseries. This is the case for energy_mix, sectoral_emissions as well as per sector emissions graphs. In the case of energy mix, individual pathways are selectable. In the case of emissions intensity, data for two different types of units (gCO₂/kwh and MtCO₂e) are available.

- `sectoral_emissions`
- `primary_energy_mix`
- `power_energy_mix`
- `buildings_energy_mix`
- `transport_energy_mix`
- `industry_energy_mix`
- `power_emissions_intensity`
- `buildings_emissions_intensity`
- `transport_emissions_intensity`
- `industry_emissions_intensity`
- `lulucf_land_use`
- `power_investments`
- `power_investments_change`

**`<data_object_id>`**: The id for individual pathways or units

**`default`**: The `data_object_id` that be selected by default

**`data_keys`**: The actual data keys that are present for each year such as energy sources, pathways or sectors

```js
<data_id>: {
  "data": [
    {
      "id": <data_object_id>,
      unit: <unit_id>,
      "name": String,
      "data": [
        {
          "year": Number,
          <energy_source_id|sector_id|pathway_id>: Number
        }
      ]
    }
  ],
  "data_keys": [<energy_source_id|sector_id|pathway_id>],
  "default": <data_object_id>,
  "title": Template,
  "secondary_title": Template,
  "caption": Template,
  "disclaimers": [String],
  "names": {
    <data_object_id>: String
  },
  "references": [
    {
      "id": <reference_id>
    }
  ],
  "methodology": [{name: String, id: <methodology_section_id>}]
}
```

### Breakdown

- `historic_emissions_breakdown`

```js
"historic_emissions_breakdown": {
  "data": [
    {
      "sector": <sector_id>,
      "gas": <gas_id>,
      "value": Number
    }
  ],
  "year": Number,
  "unit": <unit_id>,
  "title": Template,
  "secondary_title": Template,
  "names": {
    "positive_emissions": String,
    "negative_emissions": String,
    "by_sector_title": String,
    "by_gas_title": String,
    "by_gas_by_sector_title": String,
  },
  "caption": Template,
  "disclaimers": [String],
  "references": [
    {
      "id": <reference_id>
    }
  ],
}
```

### Timeline

- `power_sector_timeline`
- `buildings_sector_timeline`
- `transport_sector_timeline`
- `industry_sector_timeline`
- `lulucf_sector_timeline`

Besides an `id` and a `unit`, each data object has one of the following combination of properties `year, values`, `year, value`, `year` or `years`.

`names.<id>`: The ids refer to the ids in the data objects. In the previous version they are `reshare|netzero|coalphaseout`, but we will be using `re_share`, `coal_consumption_share`, `netzero` and `coal_phaseout`.

```js
<data_id>: {
  "compat": [
    {
      "id": String,
      "year": Number,
      "values": [Number, Number],
      "unit": <unit_id>
    },
    {
      "id": String,
      "year": Number,
      "value": Number,
      "unit": <unit_id>
    },
    {
      "id": String,
      "year": Number
    },
    {
      "id": String,
      "years": [Number, Number]
    }
  ],
  "targets" (same as compat),
  "names": {
    <id>: String
  },
  "caption": Template,
  "references": [
    {
      "id": <reference_id>
    }
  ]
}
```

### Keyfacts

- `keyfacts_emissions`
- `keyfacts_power`
- `keyfacts_buildings`
- `keyfacts_transport`
- `keyfacts_industry`

Keyfacts data consists of multiple tables. Each table defines a number of rows each consisting of cells and a number of columns. The order of the rows as well as columns is set by the order defined in the data.

```js
<data_id>: {
  <tables>: [
    {
      id: <table_id>
      rows: [
        {
          name: String,
          unit: <unit_id>,
          cells: [
            {
              "id": <col_id>,
              "value": Number,
              "min" Number,
              "max" Number
            }
          ]
        }
      ],
      cols: [
        {
          id: <col_id>,
          name: String
        }
      ]
    }
  ],
  "title": Template,
  "secondary_title": Template,
  "disclaimers": [String],
  "caption": Template,
  "references": [
    {
      "id": String
    }
  ],
  "methodology": [{name: String, id: <methodology_section_id>}]
}
```

## `/data/<country_id>_<data_id>.json|csv`

This endpoint is used for the data download. In addition to a `country_id`, a `data_id` must be specified which limits the data downloaded. The data ids are the same top-level entries as used in the `/data/<country_id>.json` endpoint. Which formats you want to provide is up to you of course. I highly recommend to offer a well structured csv file though.
