This document keeps track of the changes made from version to version.

## v4

### Overall structure of dataspecs

I tried to make some consolidation to reduce the number of different datastructures we have but rather reuse the ones that have very similar requirements.

### New sectors

New sectors (buildings, transport, industry) have been added. This results in additional data for each of them. Specifically `*_energy_mix` and `*_emissions_intensity`.

Additionally it is planned to have an additional graph per sector in the "In brief" section called `*_aggregated_energy_mix`. This will be almost the same as the `*_energy_mix` except that it aggregates certain energy sources to achieve a simpler version.

### Additional NDC

There is the need for additional NDCs per country (so far only UK) in `total_ghg_emissions`. For this reason it is no longer practical to keep the values for individual NDCs directly in the top level data. There is now an `ndcs` array holding all relevant data for all the NDCs. This includes conditional, unconditional as well as 1.5° compatible NDC. It is still assumed that the first NDC in the list is the primary NDC.

### Multiple units per chart

One new requirement is to have the possibility to select between "total emissions" and "emissions per kwh" in the emissions intensity graph. My proposition is to simply use the same datastructure as we are using for the `*_energy_mix` data where we have multiple pathways. Instead of having multiple pathways, we now are dealing with multiple units. For this reason I made the datastructure, now called "Nested series" more generic. The main data array is no longer called `pathways` but simply `data`. Additionally there is now a `data_keys` array present that is used to pick the individual data values for each year.

### Countries without sectoral analysis

There will be at least two countries that won't have a detailed sector analysis. For these two there is now a **`no_sector_analysis`** boolean property present in the `meta.countries` array.


## v5

- Added `land_covers` to `/meta` endpoint
- Added `lulucf_sector_timeline` to follow the *Timeline* spec
- Added , `lulucf_land_use`, `power_investments` and `power_investments_change` to follow the *Nested series* spec
- Added `lulucf_emissions`, `lulucf_forest` to follow the *Series* spec
- Removed `buildings_aggregated_energy_mix`, `transport_aggregated_energy_mix` and `industry_aggregated_energy_mix` from *Series* spec as they are no longer needed
- Changed `pathways` to `data_keys` in *Series* spec to make it more generic. Besides changing this property name, no additional change should be required on the backend side.
- An additional change has to be made in the data delivery which is not reflected in the spec. In `power_emissions` under *Nested series*, in additon to *power generation* the next versio will also present *power capacities*. Since this data now will have two levels of nesting (pathways AND generation/capacities) we need to think of a new way to deal with this. My suggestion would be to simply use the `unit` as a secondary `id` from which the second set of graphs, showing the capacities will be generated. This might not be the most elegant solution but it prevents us from introducing a new datastructure. Alternatively we could intruduce sth like a `data_object_ids` top level property which will list all the nesting levels available. This would look like this: 

```
{
  "data": [
    {
      "pathway": "image_ssp1_19",
      "unit": "twh",
      "data": [...]
    },
    {
      "pathway": "image_ssp1_19",
      "unit": "twhy",
      "data": [...]
    },
    {
      "pathway": "aim_ssp1_19",
      "unit": "twh",
      "data": [...]
    },
    {
      "pathway": "aim_ssp1_19",
      "unit": "twhy",
      "data": [...]
    }
  ]
  "data_object_ids": ["pathway", "unit"]
}
```
