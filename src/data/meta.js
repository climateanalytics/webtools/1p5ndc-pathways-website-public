import { format as d3Format } from 'd3-format';
import { roundNumber, formatPercent } from 'src/utils';

export const units = {
  pc: {
    format: (val) => formatPercent(val).replace(/%$/, ''),
    formatTick: d3Format('+.0%'),
    name: '%',
    name_long: 'Percent'
  },
  gco2kwh: {
    format: function (val) {
      return `${roundNumber(Math.round(val / 10) * 10)}`;
    }
  },
  year: {
    format: (val) => val
  }
};
