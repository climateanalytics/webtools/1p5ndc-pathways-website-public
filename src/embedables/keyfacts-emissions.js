import { memo, useMemo } from 'react';
import Table from 'src/components/charts/Table';
import { getTableData } from './helpers';
import useUiReducer from 'src/utils/useUiReducer';
import { useSiteContent } from 'src/data/siteContent';
import cn from './style.module.scss';

const KeyfactsEmissions = (props) => {
  const siteContent = useSiteContent();

  const { country, id, tables, meta } = props;

  const reducer = useUiReducer({
    referenceYear: props.referenceYear || props.country.country_reference_year
  });
  const { referenceYear } = reducer.state;

  const controls = useMemo(() => {
    return [
      {
        label: siteContent.reference_year_label,
        type: 'SegmentedControl',
        id: 'referenceYear',
        segments: country.reference_years.map((y) => ({ id: y, name: y }))
      }
    ];
  }, [meta]);

  const chartProps = useMemo(() => {
    // Manually make sure that ghg emissions are on top. Should come sorted from api
    const sortedRows = tables[0].rows
      .slice(0)
      .sort((a) => (a.id === 'total_ghg_emissions' ? -1 : 1));

    const { rows, columns } = getTableData(
      { ...tables[0], rows: sortedRows },
      {
        siteContent,
        names: props.names,
        referenceYear,
        units: meta.unitsById,
        addRelativeRow: true
      }
    );

    return {
      ...props,
      rows,
      columns
    };
  }, [referenceYear, props]);

  return (
    <div className={cn.keyfactsWrapper}>
      <Table
        {...chartProps}
        showCaption={true}
        showDisclaimers={false}
        titleClassName={cn.keyfactsTitle}
        footerClassName={cn.keyfactsFooter}
        titleIcon="table-checked"
        country={country}
        controls={controls}
        reducer={reducer}
        data={chartProps.data}
        id={id}
      />
    </div>
  );
};

export default memo(KeyfactsEmissions);
