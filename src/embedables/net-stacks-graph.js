import { memo, useMemo } from 'react';
import NetStacks from 'src/components/charts/NetStacks';
import { useSiteContent } from 'src/data/siteContent';
import { useTheme } from 'src/style/ThemeProvider';
import { hexToRgba } from 'src/utils';
import useUiReducer from 'src/utils/useUiReducer';
import cn from './style.module.scss';

const NetStacksGraph = (props) => {
  const theme = useTheme();
  const { meta, data, data_keys, names } = props;
  const hasHistorical = data_keys.includes('negative_historical');
  const siteContent = useSiteContent();
  const pathways = useMemo(() => data.map((d) => d.id));

  const segments = useMemo(() =>
    [
      hasHistorical && {
        id: 'negative_historical',
        hatching: true,
        color: theme.color.category[5]
      },
      hasHistorical && {
        id: 'positive_historical',
        hatching: true,
        color: hexToRgba(theme.color.category[5], 0.5)
      },
      hasHistorical && {
        id: 'net_historical',
        color: theme.color.category[2],
        hatching: true
      },
      { id: 'negative_projection', color: theme.color.category[0] },
      { id: 'positive_projection', color: hexToRgba(theme.color.category[0], 0.5) },

      {
        id: 'net_projection',
        color: theme.color.category[2]
      }
    ]
      .filter((d) => d)
      .map((d) => ({ ...d, name: names[d.id] }))
  );

  const controls = useMemo(() => {
    const items = [
      {
        type: 'SimpleSelect',
        id: 'pathway',
        label: siteContent.pathway_label,
        segments: pathways.map((id) => meta.pathwaysById[id])
      }
    ];
    return items;
  }, [siteContent, meta, pathways]);

  const reducer = useUiReducer({
    pathway: props.pathway || pathways[0]
  });

  const unit = meta.unitsById[props.unit];

  const chartData = useMemo(
    () => data.find((d) => d.id === reducer.state.pathway).data,
    [data, reducer.state.pathway]
  );

  return (
    <>
      <NetStacks
        {...props}
        unit={unit}
        data={chartData}
        chartClassName={cn.netStackGraph}
        segments={segments}
        controls={pathways.length > 1 && controls}
        reducer={reducer}
        showDisclaimers={true}
      />
    </>
  );
};

export default memo(NetStacksGraph);
