import { memo, useMemo } from 'react';
import MixedSeries from 'src/components/charts/MixedSeries';
import cn from './style.module.scss';
import { useTheme } from 'src/style/ThemeProvider';

const IndustryProcessesesEmissions = (props) => {
  const theme = useTheme();
  const industryProcessesSpec = useMemo(
    () => [
      ...props.data_keys.map((pathway, i) => ({
        type: 'line',
        name: props.meta.pathwaysById[pathway]?.name,
        key: pathway,
        color: theme.color.pathways[pathway]
      })),
      {
        type: 'line',
        name: props.names.historical,
        key: 'historical',
        color: theme.color.text.base,
        dashed: true
      }
    ],
    [props, theme]
  );
  const unit = props.meta.unitsById[props.unit];

  return (
    <>
      <MixedSeries
        {...props}
        secondary_title={unit.name}
        chartClassName={cn.mixedChartWrapper}
        unit={unit}
        spec={industryProcessesSpec}
      />
    </>
  );
};

export default memo(IndustryProcessesesEmissions);
