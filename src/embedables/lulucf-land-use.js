import { memo, useMemo } from 'react';
import StackedArea from 'src/components/charts/StackedArea';
import MixedSeries from 'src/components/charts/MixedSeries';
import cn from './style.module.scss';
import useUiReducer from 'src/utils/useUiReducer';
import { useSiteContent } from 'src/data/siteContent';

const LulucfLandUse = (props) => {
  const { data, meta, data_keys } = props;
  const pathways = useMemo(() => data.map((d) => d.id));
  const siteContent = useSiteContent();

  const reducer = useUiReducer({
    pathway: props.pathway || pathways[0]
  });

  const chartData = useMemo(
    () => data.find((d) => d.id === reducer.state.pathway).data,
    [data, reducer.state.pathway]
  );

  const changeData = useMemo(() => {
    const baselineData = chartData.find((d) => d.year === 2020);
    return chartData.map((d) =>
      data_keys.reduce((acc, key) => ({ ...acc, [key]: d[key] - baselineData[key] }), {
        year: d.year
      })
    );
  });

  const unit = props.meta.unitsById[data[0].unit] || {};

  const segments = useMemo(
    () => data_keys.map((key) => meta.landCoversById[key] || { id: 'other', name: 'Other' }),
    [data_keys, meta]
  );

  const spec = useMemo(() => {
    return segments.map((segment) => ({
      ...segment,
      key: segment.id,
      type: 'line',
      style: {
        strokeWidth: 3.5
      }
    }));
  }, [data, meta]);

  const controls = useMemo(() => {
    const items = [
      {
        type: 'SimpleSelect',
        id: 'pathway',
        label: siteContent.pathway_label,
        segments: pathways.map((id) => meta.pathwaysById[id])
      }
    ];
    return items;
  }, [siteContent, meta, pathways]);

  return (
    <>
      <div className="margin-bottom-xxl">
        <StackedArea
          {...props}
          secondary_title={unit.name}
          chartClassName={cn.landUseAbsoluteWrapper}
          unit={unit}
          segments={segments}
          data={chartData}
          showFooter={false}
          controls={pathways.length > 1 && controls}
          reducer={reducer}
        />
      </div>
      <MixedSeries
        {...props}
        secondary_title={unit.name}
        title="{{country.alt_name|s}} land cover change relative to 2020"
        chartClassName={cn.landUseAbsoluteWrapper}
        unit={unit}
        spec={spec}
        data={changeData}
      />
    </>
  );
};

export default memo(LulucfLandUse);
