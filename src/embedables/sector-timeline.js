import { memo, useMemo } from 'react';
import Timeline from 'src/components/charts/Timeline';
import cn from './style.module.scss';
import { useSiteContent } from 'src/data/siteContent';
import { useTheme } from 'src/style/ThemeProvider';

const SectorTimeline = (props) => {
  const siteContent = useSiteContent();
  const theme = useTheme();
  const chartData = useMemo(() => {
    return [
      { id: 'targets', name: siteContent.current_targets_label, color: theme.color.emissions.ndc },
      {
        id: 'compat',
        name: siteContent.required_targets_label,
        color: theme.color.emissions.ndc_compat
      }
    ].map((side) => ({
      ...side,
      data:
        props[side.id]?.map((datum) => ({
          ...datum,
          name: props.names[datum.id],
          unit: props.meta.unitsById[datum.unit] || props.meta.unitsById.pc
        })) || []
    }));
  }, [props]);

  return (
    <Timeline
      footerClassName={cn.timelineFooter}
      showHeader={false}
      titleInFooter={true}
      data={chartData}
      {...props}
      allowDataDownload={false}
    />
  );
};

export default memo(SectorTimeline);
