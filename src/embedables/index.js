import dynamic from 'next/dynamic';

/*
 * Each entry creates a new embed that can be rendered on its own page under /countries/[country]/[embed.id]
 * Required properties:
 * `id` is an unique identifier for the embed which will also be its url slug
 * `dataId` is the id of the data object that it requires
 * `Component` a component that knows how to deal with the data and renders the embed accordingly
 */

export default {
  total_ghg_emissions: {
    id: 'total_ghg_emissions',
    dataId: 'total_ghg_emissions',
    Component: dynamic(() => import(`src/embedables/total-ghg-emissions`))
  },
  power_sector_timeline: {
    id: 'power_sector_timeline',
    dataId: 'power_sector_timeline',
    Component: dynamic(() => import(`src/embedables/sector-timeline`))
  },
  buildings_sector_timeline: {
    id: 'buildings_sector_timeline',
    dataId: 'buildings_sector_timeline',
    Component: dynamic(() => import(`src/embedables/sector-timeline`))
  },
  transport_sector_timeline: {
    id: 'transport_sector_timeline',
    dataId: 'transport_sector_timeline',
    Component: dynamic(() => import(`src/embedables/sector-timeline`))
  },
  industry_sector_timeline: {
    id: 'industry_sector_timeline',
    dataId: 'industry_sector_timeline',
    Component: dynamic(() => import(`src/embedables/sector-timeline`))
  },
  lulucf_sector_timeline: {
    id: 'lulucf_sector_timeline',
    dataId: 'lulucf_sector_timeline',
    Component: dynamic(() => import(`src/embedables/sector-timeline`))
  },
  historic_emissions_breakdown: {
    id: 'historic_emissions_breakdown',
    dataId: 'historic_emissions_breakdown',
    Component: dynamic(() => import(`src/embedables/historic-emissions-breakdown`))
  },
  total_co2_emissions: {
    id: 'total_co2_emissions',
    dataId: 'total_co2_emissions',
    Component: dynamic(() => import(`src/embedables/total-co2-emissions`))
  },
  primary_energy_mix: {
    id: 'primary_energy_mix',
    dataId: 'primary_energy_mix',
    Component: dynamic(() => import(`src/embedables/energy-mix`))
  },
  keyfacts_emissions: {
    id: 'keyfacts_emissions',
    dataId: 'keyfacts_emissions',
    Component: dynamic(() => import(`src/embedables/keyfacts-emissions`))
  },
  sectoral_emissions: {
    id: 'sectoral_emissions',
    dataId: 'sectoral_emissions',
    Component: dynamic(() => import(`src/embedables/emissions-by-sector`))
  },
  power_emissions: {
    id: 'power_emissions',
    dataId: 'power_emissions_intensity',
    Component: dynamic(() => import(`src/embedables/sector-emissions`))
  },
  power_energy_mix: {
    id: 'power_energy_mix',
    dataId: 'power_energy_mix',
    Component: dynamic(() => import(`src/embedables/energy-mix`))
  },
  power_investments: {
    id: 'power_investments',
    dataId: 'power_investments',
    Component: dynamic(() => import(`src/embedables/power-investments`))
  },
  power_cost: {
    id: 'power_cost',
    dataId: 'power_cost',
    Component: dynamic(() => import(`src/embedables/power-cost`))
  },
  power_keyfacts: {
    id: 'power_keyfacts',
    dataId: 'keyfacts_power',
    Component: dynamic(() => import(`src/embedables/sector-keyfacts`))
  },
  buildings_emissions: {
    id: 'buildings_emissions',
    dataId: 'buildings_emissions_intensity',
    Component: dynamic(() => import(`src/embedables/sector-emissions`))
  },
  buildings_energy_mix: {
    id: 'buildings_energy_mix',
    dataId: 'buildings_energy_mix',
    Component: dynamic(() => import(`src/embedables/energy-mix`))
  },
  buildings_keyfacts: {
    id: 'buildings_keyfacts',
    dataId: 'keyfacts_buildings',
    Component: dynamic(() => import(`src/embedables/sector-keyfacts`))
  },
  transport_emissions: {
    id: 'transport_emissions',
    dataId: 'transport_emissions_intensity',
    Component: dynamic(() => import(`src/embedables/sector-emissions`))
  },
  transport_energy_mix: {
    id: 'transport_energy_mix',
    dataId: 'transport_energy_mix',
    Component: dynamic(() => import(`src/embedables/energy-mix`))
  },
  transport_keyfacts: {
    id: 'transport_keyfacts',
    dataId: 'keyfacts_transport',
    Component: dynamic(() => import(`src/embedables/sector-keyfacts`))
  },
  industry_emissions: {
    id: 'industry_emissions',
    dataId: 'industry_emissions_intensity',
    Component: dynamic(() => import(`src/embedables/sector-emissions`))
  },
  industry_processes_emissions: {
    id: 'industry_processes_emissions',
    dataId: 'industry_processes_emissions',
    Component: dynamic(() => import(`src/embedables/industry-processes-emissions`))
  },
  industry_energy_mix: {
    id: 'industry_energy_mix',
    dataId: 'industry_energy_mix',
    Component: dynamic(() => import(`src/embedables/energy-mix`))
  },
  industry_keyfacts: {
    id: 'industry_keyfacts',
    dataId: 'keyfacts_industry',
    Component: dynamic(() => import(`src/embedables/sector-keyfacts`))
  },
  lulucf_land_use: {
    id: 'lulucf_land_use',
    dataId: 'lulucf_land_use',
    Component: dynamic(() => import(`src/embedables/lulucf-land-use`))
  },
  lulucf_emissions: {
    id: 'lulucf_emissions',
    dataId: 'lulucf_emissions',
    Component: dynamic(() => import(`src/embedables/net-stacks-graph`))
  },
  lulucf_forest: {
    id: 'lulucf_forest',
    dataId: 'lulucf_forest',
    Component: dynamic(() => import(`src/embedables/net-stacks-graph`))
  },
  head_image: {
    id: 'head_image',
    Component: dynamic(() => import(`src/embedables/head-image`))
  }
};
