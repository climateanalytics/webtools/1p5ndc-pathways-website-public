import { memo, useMemo } from 'react';
import { HorizontalStack, HorizontalStacks } from 'src/components/charts/HorizontalStacks';
import useUiReducer from 'src/utils/useUiReducer';
import { useSiteContent } from 'src/data/siteContent';
import { sumBy } from 'src/utils';
import cn from './style.module.scss';

const groups = [
  {
    id: 'energy',
    children: [
      'combustion',
      'power',
      'buildings',
      'transport',
      'industry_energy',
      'other_energy',
      'fugitive_emissions'
    ]
  }
];

const sectorsByGasesGroups = [
  ...groups,
  {
    id: 'agriculture'
  },
  {
    id: 'industry_processes'
  }
];

const HistoricEmissionsBreakdown = (props) => {
  const { data, meta } = props;
  const siteContent = useSiteContent();
  const reducer = useUiReducer({
    displayType: props.displayType || 'pc'
  });

  const chartData = useMemo(() => {
    // Convert to object for needed by d3-stack
    const sectorsData = sumBy(data, 'sector');
    const gasesData = sumBy(data, 'gas');

    const sectors = meta.sectors.map((sector) => ({
      ...sector,
      value: sectorsData[sector.id],
      parent: groups.find((group) => group.children.includes(sector.id))
    }));

    const gases = meta.gases.map((gas) => ({
      ...gas,
      value: gasesData[gas.id]
    }));

    const sectorsByGas = sectorsByGasesGroups.map((group) => {
      const groupIds = group.children || [group.id];
      const groupMeta = meta.sectorsById[group.id];
      const filteredData = data.filter((d) => groupIds.includes(d.sector));
      const groupData = Object.entries(sumBy(filteredData, 'gas')).map(([id, d]) => ({
        ...meta.gasesById[id],
        value: d
      }));

      return {
        ...groupMeta,
        data: groupData
      };
    });

    return { gases, sectorsByGas, sectors };
  }, [data, meta]);

  const controls = useMemo(() => {
    return [
      {
        label: siteContent.displayed_values_label,
        type: 'SegmentedControl',
        id: 'displayType',
        segments: [
          { ...meta.unitsById.pc, name: meta.unitsById.pc.name_long },
          meta.unitsById.mtco2e
        ]
      }
    ];
  }, [meta]);

  return (
    <>
      <div className={cn.horizontalStackWrapper}>
        <HorizontalStack
          {...props}
          positiveLabel={props.names.positive_emissions}
          negativeLabel={props.names.negative_emissions}
          unit={meta.unitsById[props.unit]}
          stackTitle={props.names.by_sector_title}
          chartClassName={cn.historicalSectoralEmissiosChart}
          data={chartData.sectors}
          showFooter={false}
          reducer={reducer}
          controls={controls}
        />
      </div>
      <div className={cn.horizontalStackWrapper}>
        <HorizontalStack
          {...props}
          addFrame={false}
          positiveLabel={props.names.positive_emissions}
          negativeLabel={props.names.negative_emissions}
          unit={meta.unitsById[props.unit]}
          stackTitle={props.names.by_gas_title}
          data={chartData.gases}
          reducer={reducer}
        />
      </div>
      <div className={cn.horizontalStackWrapper}>
        <HorizontalStacks
          {...props}
          unit={meta.unitsById[props.unit]}
          stackTitle={props.names.by_gas_by_sector_title}
          data={chartData.sectorsByGas}
          reducer={reducer}
          showHeader={false}
        />
      </div>
    </>
  );
};

export default memo(HistoricEmissionsBreakdown);
