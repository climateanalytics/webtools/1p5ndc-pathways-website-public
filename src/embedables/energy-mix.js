import { memo, useMemo } from 'react';
import useUiReducer from 'src/utils/useUiReducer';
import VerticalStacks from 'src/components/charts/VerticalStacks/Multiple';
import { useSiteContent } from 'src/data/siteContent';
import { uniqify } from 'src/utils';

// Groups used for sorting bars
const segmentGroups = [
  ['solids_biomass', 'gases_biomass', 'liquids_biomass'],
  ['liquids_oil_and_other', 'gases_natural_gas', 'solids_coal']
];

const scalingItems = [
  { id: 'relative', name: 'Relative' },
  { id: 'absolute', name: 'Absolute' }
];

const PrimaryEnergyMix = (props) => {
  const { meta, data, data_keys, stateReducer } = props;
  const siteContent = useSiteContent();
  const units = useMemo(() => uniqify(data.map((d) => d.unit)), [data]);
  const reducer =
    stateReducer ||
    useUiReducer({
      scaling: props.scaling || 'absolute',
      unit: props.unit || units[0]
    });

  const unit = meta.unitsById[reducer.state.unit];

  const controls = useMemo(() => {
    const items = [
      {
        type: 'SegmentedControl',
        id: 'scaling',
        label: siteContent.scaling_label,
        segments: scalingItems
      }
    ];

    if (units.length > 1) {
      items.push({
        type: 'SegmentedControl',
        id: 'unit',
        label: siteContent.power_dimension_label,
        segments: [
          { id: 'twhy', name: siteContent.power_generation_label },
          { id: 'gw', name: siteContent.power_capacities_label }
        ]
      });
    }
    return items;
  }, [units, siteContent, meta]);

  const chartData = useMemo(
    () => data.filter((d) => d.unit === reducer.state.unit),
    [data, reducer.state.unit]
  );

  const segments = useMemo(
    () => data_keys.map((key) => meta.energySourcesById[key]).filter((d) => d),
    [data_keys, meta]
  );

  return (
    <>
      <VerticalStacks
        {...props}
        secondary_title={unit.name_long}
        unit={unit}
        reducer={reducer}
        chartsMeta={meta.pathways}
        data={chartData}
        segments={segments}
        segmentGroups={segmentGroups}
        controls={controls}
      />
    </>
  );
};

export default memo(PrimaryEnergyMix);
