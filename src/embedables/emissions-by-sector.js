import { memo, useMemo } from 'react';
import Areas from 'src/components/charts/Areas';
import { useSiteContent } from 'src/data/siteContent';
import { useTheme } from 'src/style/ThemeProvider';
import useUiReducer from 'src/utils/useUiReducer';

const historicalRange = [1990, 2019];

const EmissionsBySector = (props) => {
  const theme = useTheme();
  const siteContent = useSiteContent();
  const { meta, data, default_pathway } = props;
  const defaultPathway = data.find((p) => p.id === default_pathway)?.id;

  const reducer = useUiReducer({
    pathway: props.default || defaultPathway || data.pathways[0].id
  });

  const unit = meta.unitsById[data[0].unit];

  const chartData = useMemo(() => {
    const pathway = data.find((p) => reducer.state.pathway === p.id);
    const sectorIds = Object.keys(pathway.data[0]).filter((id) => id !== 'year');
    return sectorIds.map((id) => ({
      ...meta.sectorsById[id],
      color: id === 'other' ? theme.color.category['7-50'] : meta.sectorsById[id].color,
      data: pathway.data.map((d) => ({ year: d.year, value: d[id] }))
    }));
  }, [data, meta, reducer.state.pathway]);

  const controls = useMemo(() => {
    return [
      {
        label: siteContent.pathway_label,
        type: 'SimpleSelect',
        id: 'pathway',
        items: meta.pathways.filter(({ id }) => data.find((p) => p.id === id))
      }
    ];
  }, [meta, data]);

  return (
    <>
      <Areas
        {...props}
        controls={controls}
        unit={unit}
        reducer={reducer}
        hatchingLabel={siteContent.historical_emissions_label}
        hatchingRange={historicalRange}
        data={chartData}
      />
    </>
  );
};

export default memo(EmissionsBySector);
