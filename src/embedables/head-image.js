import CountryHero from 'src/components/PageLayout/CountryHero';

const HeadImage = (props) => {
  return (
    <CountryHero profileImage={true} country={props.country} siteContent={props.siteContent} />
  );
};

export default HeadImage;
