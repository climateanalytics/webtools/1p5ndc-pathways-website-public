import { memo, useMemo } from 'react';
import MixedSeries from 'src/components/charts/MixedSeries';
import cn from './style.module.scss';
import { useTheme } from 'src/style/ThemeProvider';

const TotalCo2Emissions = (props) => {
  const theme = useTheme();

  const totalCo2Spec = useMemo(
    () => [
      ...props.data_keys.map((pathway, i) => ({
        type: 'line',
        name: props.meta.pathwaysById[pathway]?.name,
        key: pathway,
        color: theme.color.emissions.ndc_compat,
        opacity: 0.5,
        hideInLegend: i > 0,
        legendName: props.names.pathways,
        style: {
          opacity: 0.3,
          strokeWidth: 2
        }
      })),
      {
        type: 'area',
        name: props.names.range,
        key: 'range',
        color: theme.color.emissions.ndc_compat,
        style: {
          opacity: 0.5
        }
      },
      {
        type: 'line',
        name: props.names.range_midline,
        key: 'range_midline',
        color: theme.color.emissions.ndc_compat,
        style: {
          strokeWidth: 3
        }
      },
      {
        type: 'line',
        name: props.names.historical,
        key: 'historical',
        color: theme.color.text.base,
        dashed: true
      }
    ],
    [props, theme]
  );

  return (
    <>
      <MixedSeries
        {...props}
        chartClassName={cn.mixedChartWrapper}
        unit={props.meta.unitsById[props.unit]}
        spec={totalCo2Spec}
      />
    </>
  );
};

export default memo(TotalCo2Emissions);
