import { memo, useMemo } from 'react';
import useUiReducer from 'src/utils/useUiReducer';
import VerticalStacks from 'src/components/charts/VerticalStacks/Single';
import { useSiteContent } from 'src/data/siteContent';

const PowerInvestments = (props) => {
  const { meta, data, data_keys } = props;
  const siteContent = useSiteContent();

  const reducer = useUiReducer({
    pathway: data[0].id
  });

  const unit = meta.unitsById[data[0].unit];

  const controls = useMemo(
    () => [
      {
        type: 'SimpleSelect',
        id: 'pathway',
        label: siteContent.scaling_label,
        items: data.map((d) => meta.pathwaysById[d.id])
      }
    ],
    []
  );

  const segments = useMemo(
    () => data_keys.map((key) => meta.energySourcesById[key]).filter((d) => d),
    [data_keys, meta]
  );

  const chartData = data.find((d) => d.id === reducer.state.pathway).data;

  return (
    <>
      <VerticalStacks
        {...props}
        unit={unit}
        reducer={reducer}
        chartsMeta={meta.pathways}
        data={chartData}
        segments={segments}
        controls={controls}
      />
    </>
  );
};

export default memo(PowerInvestments);
