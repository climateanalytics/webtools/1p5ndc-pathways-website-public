import { memo, useMemo } from 'react';
import MixedSeries, { getYExtent } from 'src/components/charts/MixedSeries';
import NDCGraph from 'src/components/charts/NDCGraph';
import useUiReducer from 'src/utils/useUiReducer';
import { calculateRelativeValues } from './helpers';
import cn from './style.module.scss';
import cx from 'classnames';
import { renderTemplate } from 'src/components/content/Template';
import { useSiteContent } from 'src/data/siteContent';
import useMedia from 'src/utils/useMedia';
import { useTheme } from 'src/style/ThemeProvider';

const TotalGhgEmissions = ({ showNDCGraph = true, ...props }) => {
  const siteContent = useSiteContent();
  const isMobile = useMedia({ values: [false, false, false], defaultValue: true });
  const theme = useTheme();

  const reducer = useUiReducer({
    displayType: props.displayType || 'pc',
    referenceYear: props.referenceYear || props.country.country_reference_year,
    showNDCGraph: showNDCGraph,
    highlightedId: props.highlightedId
  });

  const { meta, country, id, data, hideControls } = props;
  const { referenceYear, displayType } = reducer.state;
  const showRelative = displayType == 'pc';
  const baselineLabel = renderTemplate(siteContent.baseline_year_label, { year: referenceYear });
  const referenceValue = data.find((d) => d.year === referenceYear).historical;
  const baselineValue = showRelative ? 0 : referenceValue;

  const unit = meta.unitsById[displayType];
  const cppIsLine = !!data.find((d) => d.cpp !== undefined);

  const chartProps = useMemo(() => {
    const chartData = showRelative
      ? data.map((d) => calculateRelativeValues(d, referenceValue))
      : data;

    const ndcData = showRelative
      ? props.ndcs.map((d) => calculateRelativeValues(d, referenceValue))
      : props.ndcs;

    const netzeroData = { id: 'netzero', value: 0, name: props.names.netzero, year: props.netzero };
    const netzero = showRelative
      ? calculateRelativeValues(netzeroData, referenceValue)
      : netzeroData;

    return {
      ...props,
      ndc_compat: ndcData.find((d) => d.id === 'ndc_compat'),
      ndcs: ndcData.filter((d) => d.id !== 'ndc_compat'),
      netzero,
      data: chartData
    };
  }, [data, referenceValue, props, displayType]);

  const chartData = chartProps.data;
  const primaryNdc = chartProps.ndcs[0];
  const ndcYear = primaryNdc?.year || 2030; // If there's no NDC, we still want the ndcYear to be 2030
  const showAmbitionGap = chartProps.ndcs.length && ndcYear === 2030;
  const stdNdcYear = ndcYear === 2030;

  const mixedControls = useMemo(() => {
    return (
      !hideControls && [
        {
          label: siteContent.displayed_values_label,
          type: 'SegmentedControl',
          id: 'displayType',
          showInStatic: false,
          segments: [
            { ...meta.unitsById.pc, name: meta.unitsById.pc.name_long },
            meta.unitsById.mtco2e
          ]
        },
        {
          label: siteContent.reference_year_label,
          type: 'SegmentedControl',
          id: 'referenceYear',
          segments: country.reference_years.map((y) => ({ id: y, name: y }))
        }
      ]
    );
  }, [meta, hideControls]);

  const totalGhgSpec = useMemo(
    () => [
      // LINES AND AREAS
      ...chartProps.data_keys.map((pathway, i) => ({
        type: 'line',
        name: meta.pathwaysById[pathway]?.name,
        key: pathway,
        color: theme.color.emissions.ndc_compat,
        hideInLegend: i > 0,
        legendName: chartProps.names.pathways,
        style: {
          opacity: 0.35,
          strokeWidth: 2.5
        }
      })),
      {
        type: 'line',
        name: chartProps.names.range_midline,
        key: 'range_midline',
        color: theme.color.emissions.ndc_compat,
        style: {
          strokeWidth: 3.5
        },
        tooltipIndex: 0
      },
      {
        type: 'dot',
        ...chartProps.netzero,
        color: theme.color.emissions.ndc_compat,
        marker: chartProps.netzero.year > 2060 ? 'left' : 'bottom',
        primaryKey: 'year',
        hideInLegend: true
      },
      {
        type: cppIsLine ? 'line' : 'area',
        name: chartProps.names.cpp,
        key: 'cpp',
        color: theme.color.emissions.cpp,
        renderEdges: false,
        style: {
          fillOpacity: 0.4,
          strokeOpacity: 0.75
        }
      },
      {
        type: 'area',
        name: chartProps.names.range,
        key: 'range',
        color: theme.color.emissions.ndc_compat,
        style: {
          fillOpacity: 0.2
        },
        tooltipIndex: 1
      },
      {
        type: 'line',
        name: chartProps.names.historical,
        key: 'historical',
        color: theme.color.text.base,
        dashed: true
      },

      // SPECIAL MARKS
      {
        type: 'dot',
        id: 'reference_year',
        name: siteContent.reference_year_label,
        year: reducer.state.referenceYear,
        value: baselineValue,
        color: theme.color.text.base,
        hideInLegend: true,
        marker:
          reducer.state.referenceYear === country.reference_years[0] ? 'bottomleft' : 'bottomleft',
        primaryKey: showRelative ? 'year' : 'value'
      },
      {
        type: 'dot',
        ...chartProps.ndc_compat,
        color: theme.color.emissions.ndc_compat,
        marker: 'bottom',
        showOnMobile: true,
        hideInLegend: true
      },
      ...chartProps.ndcs.map((ndc, i) => ({
        type: 'y_range',
        ...ndc,
        yMin: ndc.min,
        yMax: ndc.max,
        color: theme.color.emissions.ndc,
        marker: ndc.year === 2030 ? 'left' : 'right',
        primaryKey: 'yMin',
        markerYKey: 'yMin',
        showOnMobile: i === 0,
        hideInLegend: true
      })),
      ...(showAmbitionGap
        ? [
            {
              type: 'y_range',
              id: 'ambition_gap',
              name: 'Ambition gap',
              yMax: primaryNdc.min,
              yMin: chartProps.ndc_compat.value,
              year: 2038,
              color: theme.color.emissions.ambition_gap,
              hideInLegend: true,
              marker: 'right'
            },
            {
              type: 'y_ruler',
              value: chartProps.ndc_compat.value,
              xMin: 2030,
              xMax: 2038,
              color: theme.color.emissions.ambition_gap,
              hideInLegend: true
            },
            {
              type: 'y_ruler',
              value: primaryNdc.value,
              xMin: ndcYear,
              xMax: 2038,
              color: theme.color.emissions.ambition_gap,
              hideInLegend: true
            }
          ]
        : []),
      {
        type: 'tick',
        value: baselineValue
      }
    ],
    [props, theme]
  );

  const ndcData = useMemo(() => {
    const ndcYearData = chartData.find((d) => d.year === ndcYear);

    return [
      {
        id: 'cpp',
        name: chartProps.names.cpp,
        // Some countries have a range as cpp, some only one value
        max: ndcYearData.cpp_max ?? ndcYearData.cpp,
        min: ndcYearData.cpp_min ?? ndcYearData.cpp,
        color: theme.color.emissions.cpp
      },
      primaryNdc && {
        ...primaryNdc,
        name: stdNdcYear ? primaryNdc.name : `${ndcYear} ${primaryNdc.name}`,
        color: theme.color.emissions.ndc
      },
      {
        ...chartProps.ndc_compat,
        name: stdNdcYear ? chartProps.ndc_compat.name : `${chartProps.ndc_compat.name} (2030)`,
        mark: chartProps.ndc_compat.value,
        color: theme.color.emissions.ndc_compat
      }
    ].filter((d) => d);
  }, [chartProps, chartData, theme]);

  // We need to get the y extent from the mixed series to make sure
  // it is the same in the ndc chart
  const yExtent = useMemo(
    () => getYExtent(totalGhgSpec, chartProps.data),
    [totalGhgSpec, chartProps]
  );

  return (
    <>
      <div className={cx(cn.ghgEmissionsWrapper)}>
        <MixedSeries
          {...chartProps}
          reducer={reducer}
          controls={mixedControls}
          country={country}
          chartClassName={cn.ghgEmissionsGraph}
          frameClassName={cx(cn.ghgEmissionsFrame, !showNDCGraph && cn.full)}
          footerClassName={cn.ghgEmissionsFooter}
          unit={unit}
          spec={totalGhgSpec}
          data={chartProps.data}
          id={id}
          compactHeader={true}
        />
        {showNDCGraph && (
          <NDCGraph
            {...chartProps}
            id={id}
            country={country}
            reducer={reducer}
            chartClassName={cn.ndcGraph}
            frameClassName={cn.ndcFrame}
            unit={unit}
            baselineValue={baselineValue}
            referenceValue={referenceValue}
            baselineLabel={baselineLabel}
            showFrameButtons={false}
            showFooter={false}
            baselineUnit={meta.unitsById[props.unit]}
            data={ndcData}
            yExtent={yExtent}
            chartLabel={stdNdcYear ? props.names.ndc_title : ''}
            showDisclaimers={false}
            title={false}
            secondary_title={false}
          />
        )}
      </div>
    </>
  );
};

export default memo(TotalGhgEmissions);
