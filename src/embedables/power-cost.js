import { memo, useMemo } from 'react';
import MixedSeries from 'src/components/charts/MixedSeries';
import cn from './style.module.scss';
import { useTheme } from 'src/style/ThemeProvider';

const PowerCost = (props) => {
  const theme = useTheme();
  const { data, data_keys, meta, unit, names } = props;

  const seriesSpec = useMemo(() => {
    return [
      ...data_keys.map((pathway, i) => {
        return {
          type: 'line',
          name: meta.pathwaysById[pathway]?.name,
          key: pathway,
          color: theme.color.pathways[pathway]
        };
      })
    ];
  }, [data_keys, meta, names, theme]);

  return (
    <>
      <MixedSeries
        {...props}
        data={data}
        chartClassName={cn.mixedChartWrapper}
        unit={meta.unitsById[unit]}
        spec={seriesSpec}
      />
    </>
  );
};

export default memo(PowerCost);
