import { memo, useMemo } from 'react';
import Table from 'src/components/charts/Table';
import { getTableData } from './helpers';
import { useSiteContent } from 'src/data/siteContent';
import cn from './style.module.scss';

const KeyfactsEmissions = (props) => {
  const siteContent = useSiteContent();

  const { country, id, tables, dataId, meta } = props;
  const referenceDatum = tables[0].rows[0].cells.find((d) => d.id === 'historical');
  const referenceYear = referenceDatum.year;
  const addRelativeRow = referenceDatum.value >= 5; // TODO this should probably be coupled to the rounding settings

  const intensityProps = useMemo(() => {
    const { rows, columns } = getTableData(tables[0], {
      siteContent,
      names: props.names,
      units: meta.unitsById,
      referenceYear,
      addRelativeRow: addRelativeRow
    });

    return {
      ...props,
      rows,
      columns
    };
  }, [referenceYear, props]);

  const energySourcesProps = useMemo(() => {
    const { rows, columns } = getTableData(tables[1], {
      siteContent,
      names: props.names,
      units: meta.unitsById,
      referenceYear,
      addRelativeRow: false
    });

    return {
      ...props,
      rows,
      columns
    };
  }, [props]);

  return (
    <div className={cn.keyfactsWrapper}>
      <div className="margin-bottom-l">
        <Table
          {...intensityProps}
          showCaption={true}
          showFooter={false}
          titleClassName={cn.keyfactsTitle}
          titleIcon="table-checked"
          country={country}
          data={intensityProps.data}
          id={id}
          dataId={dataId}
        />
      </div>
      <Table
        {...energySourcesProps}
        showHeader={false}
        country={country}
        footerClassName={cn.keyfactsFooter}
        data={energySourcesProps.data}
        id={id}
        dataId={dataId}
      />
    </div>
  );
};

export default memo(KeyfactsEmissions);
