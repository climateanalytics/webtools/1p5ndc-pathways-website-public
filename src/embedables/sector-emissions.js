import { memo, useMemo } from 'react';
import MixedSeries from 'src/components/charts/MixedSeries';
import cn from './style.module.scss';
import { useSiteContent } from 'src/data/siteContent';
import useUiReducer from 'src/utils/useUiReducer';
import { useTheme } from 'src/style/ThemeProvider';

const EmissionsIntensity = (props) => {
  const siteContent = useSiteContent();
  const theme = useTheme();
  const { data, data_keys, meta, names } = props;
  const reducer = useUiReducer({
    unit: props.default
  });
  const unit = meta.unitsById[reducer.state.unit];
  const segments = data.map((d) => meta.unitsById[d.id]);

  const chartData = data.find((d) => d.id === reducer.state.unit).data;

  const seriesSpec = useMemo(() => {
    return [
      ...data_keys
        .sort((a) => (a === 'historical' ? -1 : 1))
        .map((pathway, i) => {
          const isHistorical = pathway === 'historical';
          return {
            type: 'line',
            name: isHistorical ? names.historical : meta.pathwaysById[pathway]?.name,
            key: pathway,
            color: isHistorical ? theme.color.text.base : theme.color.pathways[pathway],
            dashed: isHistorical,
            legendName: names.pathways
          };
        })
    ];
  }, [data_keys, meta, names, theme]);

  const controls = useMemo(() => {
    return [
      {
        label: siteContent.unit_label,
        type: 'SegmentedControl',
        id: 'unit',
        showInStatic: false,
        segments: segments
      }
    ];
  }, [meta, data]);

  return (
    <>
      <MixedSeries
        {...props}
        secondary_title={unit.name}
        data={chartData}
        reducer={reducer}
        controls={controls}
        chartClassName={cn.mixedChartWrapper}
        unit={unit}
        spec={seriesSpec}
      />
    </>
  );
};

export default memo(EmissionsIntensity);
