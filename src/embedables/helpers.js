/*
 * Tool specific data manipulation
 */

/*
 * Given an object of number values, calculates the relative values based on
 * the passed reference value. "year" key and string values are excuded.
 */
export function calculateRelativeValues(datum, refVal) {
  return Object.entries(datum).reduce(
    (acc, [key, value]) => {
      if (key === 'year' || typeof value === 'string') return acc;
      acc[key] = (value - refVal) / refVal;
      return acc;
    },
    { ...datum }
  );
}

export function getTableData(table, { siteContent, units, names, addRelativeRow, referenceYear }) {
  const columns = table.rows[0].cells
    .filter(({ id, year }) => id !== 'reference' || year === referenceYear)
    .map(({ year, id }) => ({
      id,
      name: year || names[id],
      secondaryName: (id === 'reference' && names[id]) || names[`${id}_caption`]
    }));

  columns.unshift({ name: siteContent.indicator_label, id: 'indicator' });

  const rows = table.rows.reduce((acc, row) => {
    const unit = units[row.unit];
    // TODO: Remove  || 0
    const refValue = row.cells.find((d) => d.year === referenceYear)?.value || 0;
    const cells = row.cells.filter(({ id, year }) => id !== 'reference' || year === referenceYear);

    acc.push([
      {
        id: 'indicator',
        name: names[row.id],
        secondaryName: unit.name_long || unit.name
      },
      ...cells.map((d) => {
        return {
          ...d,
          unit: d.year === undefined ? units.year : unit,
          addUnit: false
        };
      })
    ]);

    if (addRelativeRow) {
      acc.push([
        {
          id: 'indicator',
          secondaryName: names.relative,
          secondary: true
        },
        ...cells.map((d) => {
          // Don't calculate relative values for cells with year as value such as netzero
          if (d.id !== 'year') return {};
          return {
            ...calculateRelativeValues(d, refValue),
            unit: units.pc,
            addUnit: true
          };
        })
      ]);
    }
    return acc;
  }, []);

  return {
    columns,
    rows
  };
}
