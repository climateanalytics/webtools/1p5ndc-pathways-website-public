import designTokens from './theme/theme.json';
import { createContext, useContext, useEffect } from 'react';

const generateTheme = (id) => {
  return {
    id,
    ...designTokens,
    color: designTokens.color[id]
  };
};

const themeContext = createContext(generateTheme('light'));
export const useTheme = () => useContext(themeContext);

export default ({ children, id = 'light' }) => {
  useEffect(() => {
    ['light', 'dark'].forEach((themeId) =>
      themeId !== id
        ? document.body.classList.remove(`theme-${themeId}`)
        : document.body.classList.add(`theme-${themeId}`)
    );
  }, [id]);

  return <themeContext.Provider value={generateTheme(id)}>{children}</themeContext.Provider>;
};
