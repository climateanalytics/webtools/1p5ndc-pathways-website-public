export const base = "#ffffff";
export const weaker = "#f5f5f5";
export const weakest = "#e0e0e4";
export const base = "#222222";
export const weaker = "#777777";
export const weakest = "#d6d8d8";
export const weaker = "#777777";
export const base = "#222222";
export const base = "#3761a5";
export const stronger = "#365faa";
export const weaker = "#d5e0ef";
export const weakest = "#F4F7FA";
export const weak = "#a0c4eb";
export const accent = "#ffefad";
export const filtered-out = "#e4e1d8";
export const not-available = "#b2b2b2";
export const 0 = "#2A69D9";
export const 1 = "#F54B55";
export const 2 = "#FFB34F";
export const 3 = "#28A9B1";
export const 4 = "#5B4545";
export const 5 = "#222222";
export const 0-40 = "#699AF1";
export const 0-30 = "#ABC7FA";
export const 30 = "#2a69d94d";
export const 1-40 = "#F27575";
export const 1-30 = "#FFAF9D";
export const 60 = "#f54b5599";
export const 2-40 = "#ffc676";
export const 2-30 = "#ffdd9d";
export const 30 = "#ffb34f4d";
export const 60 = "#ffb34f99";
export const 4-40 = "#7E6F6E";
export const 4-30 = "#BBB3B2";
export const 3-40 = "#68CEC8";
export const 3-30 = "#9FE4D8";
export const 5-30 = "#d6d8d8";
export const 5-40 = "#777777";
export const positive-2 = "#1c4e94";
export const positive-1 = "#6fbedf";
export const neutral = "#d6dae5";
export const negative-1 = "#ebc26f";
export const negative-2 = "#ee8f00";
export const 0 = "#d7e6f3";
export const 1 = "#90d3fb";
export const 2 = "#008dc8";
export const 3 = "#0c2d5b";
export const 10 = "#f4f7fa";
export const 20 = "#e2eaf8";
export const 30 = "#BFCEE5";
export const 40 = "#92aacd";
export const 50 = "#3761A5";
export const 60 = "#2a5190";
export const ndc = "#FFB34F";
export const ndc_compat = "#2A69D9";
export const cpp = "#F54B55";
export const ambition_gap = "#5B4545";
export const gases_biomass = "#9FE4D8";
export const liquids_biomass = "#68CEC8";
export const beccs = "#68CEC8";
export const solids_biomass = "#28A9B1";
export const electricity = "#ABC7FA";
export const re = "#2A69D9";
export const hydrogen = "#699AF1";
export const fossil_ccs_nuclear = "#ffc676";
export const heat = "#F27575";
export const gases_natural_gas = "#d6d8d8";
export const liquids_oil_and_other = "#777777";
export const unabated_fossil = "#222222";
export const solids_coal = "#5B4545";
export const agriculture = "#68CEC8";
export const buildings = "#ABC7FA";
export const combustion = "#777777";
export const energy = "#2A69D9";
export const fugitive_emissions = "#9FE4D8";
export const industry = "#FFB34F";
export const industry_energy = "#ABC7FA";
export const industry_processes = "#FFB34F";
export const lulucf = "#28A9B1";
export const non_specified = "#d6d8d8";
export const other = "#d6d8d8";
export const other_energy = "#d6d8d8";
export const power = "#2A69D9";
export const transport = "#699AF1";
export const waste = "#7E6F6E";
export const ch4 = "#7E6F6E";
export const co2 = "#5B4545";
export const ch4-copy-copy = "#BBB3B2";
export const hfcs = "#ffc676";
export const n2o = "#ffdd9d";
export const nf3 = "#F27575";
export const non_specified = "#d6d8d8";
export const other = "#d6d8d8";
export const pfc2 = "#FFAF9D";
export const sf6 = "#9FE4D8";
export const cropland = "#FFB34F";
export const pasture = "#28A9B1";
export const forest = "#699AF1";
export const builtup = "#5B4545";
export const other_natural_area = "#d6d8d8";
export const aim_ssp1_19 = "#2A69D9";
export const ewg_lut = "#F54B55";
export const image_ssp1_19 = "#FFB34F";
export const led = "#28A9B1";
export const remind_cdr8 = "#5B4545";
export const aus_catsu = "#7E6F6E";
export const image_ssp1_19_lb = "#FFAF9D";
export const pac = "#ffc676";
export const weo_steps_2021 = "#699AF1";
export const base = "#202931";
export const weaker = "#4c5873";
export const weakest = "#636e86";
export const base = "#eaeaea";
export const weaker = "#9597ae";
export const weakest = "#696c85";
export const weaker = "#a9abbd";
export const base = "#eaeaea";
export const base = "#008dc9";
export const weaker = "#1f4c7e";
export const stronger = "#90d3fb";
export const accent = "#ffcb01";
export const not-available = "#cccccc";
export const filtered-out = "#474b69";
export const 0 = "#008dc9";
export const 1 = "#f26829";
export const 2 = "#f4a81d";
export const 3 = "#8fba51";
export const 4 = "#8a91f3";
export const 5 = "#bd53bd";
export const positive-2 = "#a1d0ff";
export const positive-1 = "#6d8ca3";
export const neutral = "#3e404f";
export const negative-1 = "#94815b";
export const negative-2 = "#f0b847";
export const 0 = "#2c3c54";
export const 1 = "#445f7e";
export const 2 = "#72a5c9";
export const 3 = "#90d3fb";
export const climate = "#ffcb01";
export const conflict = "#fa5a6d";
export const scope-conditions = "#90d3fc";
export const card = "[object Object]";
export const hover = "[object Object]";
export const modal = "[object Object]";
export const ui = "[object Object]";
export const large = "1.25";
export const small = "1.2";
export const base = "Brandon Text";
export const heading = "Brandon Text";
export const code = "Source Code Pro";
export const base = "160%";
export const tighter = "150%";
export const tightest = "120%";
export const regular = "Regular";
export const bold = "Bold";
export const regularNumber = "400";
export const boldNumber = "600";
export const regularItalic = "Regular Italic";
export const base = "0em";
export const looser = "0.15em";
export const none = "0";
export const base = "8";
export const none = "none";
export const uppercase = "uppercase";
export const none = "none";
export const xs = "9.723";
export const s = "11.667";
export const m = "14";
export const l = "16.8";
export const xl = "20.16";
export const xxl = "24.192";
export const xxxl = "29.03";
export const xxxxl = "34.836";
export const xs = "10.24";
export const s = "12.8";
export const m = "16";
export const l = "20";
export const xl = "25";
export const xxl = "31.25";
export const xxxl = "39.063";
export const xxxxl = "48.829";
export const base = "400 16px/160% 'Brandon Text'";
export const caption = "400 16px/160% 'Brandon Text'";
export const heading = "600 16px/160% 'Brandon Text'";
export const bold = "600 16px/160% 'Brandon Text'";
export const min-content-width = "320px";
export const max-content-width = "1200px";
export const xxxs = "4px";
export const xxs = "8px";
export const xs = "12px";
export const s = "16px";
export const m = "24px";
export const l = "32px";
export const xl = "48px";
export const xxl = "64px";
export const xxxl = "96px";
export const xxxs = "3.5px";
export const xxs = "7px";
export const xs = "10.5px";
export const s = "14px";
export const m = "21px";
export const l = "28px";
export const xl = "42px";
export const xxl = "56px";
export const xxxl = "84px";
export const xxxs = "4px";
export const xxs = "8px";
export const xs = "12px";
export const s = "16px";
export const m = "24px";
export const l = "32px";
export const xl = "48px";
export const xxl = "64px";
export const xxxl = "96px";
export const xs = "480px";
export const s = "640px";
export const m = "960px";
export const l = "1200px";
export const xl = "1200px";
export const max-content-width-no-unit = "1280";
export const min-content-width-no-unit = "320";
export const m = "4";
export const s = "3";