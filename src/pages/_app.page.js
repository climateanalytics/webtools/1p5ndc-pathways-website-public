import SiteLayout from 'src/components/SiteLayout';
import 'src/style/global.scss';
import { reduceBy } from 'src/utils';
import { SiteContentContext } from 'src/data/siteContent';
import { MatomoProvider, createInstance } from '@datapunt/matomo-tracker-react';
import ThemeProvider, { useTheme } from 'src/style/ThemeProvider';
import * as customMeta from 'src/data/meta';

const instance = createInstance({
  urlBase: 'https://climateanalytics.org/stats/',
  siteId: 8,
  linkTracking: false, // optional, default value: true
  configurations: {
    requireConsent: true
  }
});

const snakeToCamel = (str) =>
  str
    .split('_')
    .map((d, i) => (i > 0 ? `${d[0].toUpperCase()}${d.slice(1)}` : d))
    .join('');

/*
 * Adds colors and other config information to the metadata and adds a
 * handy restructured dictionary of all metadata by id for faster access
 */
const processMeta = (meta, theme) =>
  Object.entries(meta).reduce((acc, [snakeKey, prop]) => {
    const key = snakeToCamel(snakeKey);

    const colors = theme.color[snakeKey] || {};
    const custom = customMeta[key] || {};
    let enrichedProp = prop.map((item) => ({
      ...item,
      ...(custom[item.id] || {}),
      kebapId: item.id.replace(/_/g, '-'),
      color: colors[item.id]
    }));
    if (Array.isArray(prop)) acc[`${key}ById`] = reduceBy(enrichedProp);
    acc[key] = enrichedProp;
    return acc;
  }, {});

const withTheme = (Component) => (props) =>
  (
    <ThemeProvider>
      <Component {...props} />
    </ThemeProvider>
  );
const App = ({ Component, pageProps = {} }) => {
  const theme = useTheme();
  const meta = pageProps.meta ? processMeta(pageProps.meta, theme) : null;
  const Layout = Component.Layout || SiteLayout;
  const SubLayout = Component.SubLayout;

  return (
    <ThemeProvider>
      <MatomoProvider value={instance}>
        <SiteContentContext.Provider value={pageProps ? pageProps.siteContent : {}}>
          <Layout {...pageProps} meta={meta}>
            {SubLayout ? (
              <SubLayout {...pageProps} meta={meta}>
                <Component {...pageProps} meta={meta} />
              </SubLayout>
            ) : (
              <Component {...pageProps} meta={meta} />
            )}
          </Layout>
        </SiteContentContext.Provider>
      </MatomoProvider>
    </ThemeProvider>
  );
};

export default withTheme(App);
