import { reduceBy } from 'src/utils';
import copy from 'src/data/copy';

export const pages = [
  {
    page: 'in_brief',
    slug: '',
    name: copy.in_brief__name,
    endpoints: [
      { id: 'siteContent', sectionIds: ['info'] },
      {
        id: 'country',
        dataIds: [
          'total_ghg_emissions',
          'power_sector_timeline',
          'buildings_sector_timeline',
          'transport_sector_timeline',
          'industry_sector_timeline'
        ],
        contentIds: ['published_sectors']
      }
    ]
  },
  {
    page: 'current_situation',
    slug: 'current-situation',
    name: copy.current_situation__name,
    endpoints: [
      { id: 'siteContent', sectionIds: ['info'] },
      { id: 'country', dataIds: ['historic_emissions_breakdown'] }
    ]
  },
  {
    page: 'ambition_gap',
    slug: 'ambition-gap',
    name: copy.ambition_gap__name,
    endpoints: [
      { id: 'siteContent', sectionIds: ['info'] },
      {
        id: 'country',
        dataIds: [
          'total_ghg_emissions',
          'primary_energy_mix',
          'total_co2_emissions',
          'keyfacts_emissions'
        ]
      }
    ]
  },
  {
    page: 'sectors',
    slug: 'sectors/power',
    name: copy.sectors__name,
    endpoints: [
      {
        id: 'siteContent',
        sectionIds: ['info']
      },
      {
        id: 'country',
        dataIds: [
          'sectoral_emissions',
          'power_emissions_intensity',
          'power_energy_mix',
          'keyfacts_power',
          'power_sector_timeline',
          'power_investments',
          'power_mix',
          'power_cost',
          'buildings_emissions_intensity',
          'buildings_energy_mix',
          'keyfacts_buildings',
          'buildings_sector_timeline',
          'transport_emissions_intensity',
          'transport_energy_mix',
          'transport_sector_timeline',
          'keyfacts_transport',
          'industry_emissions_intensity',
          'industry_processes_emissions',
          'industry_sector_timeline',
          'industry_energy_mix',
          'keyfacts_industry',
          'lulucf_land_use',
          'lulucf_emissions',
          'lulucf_forest',
          'lulucf_sector_timeline'
        ],
        contentIds: ['published_sectors']
      }
    ]
  }
];

export const navItems = pages.map(({ name, slug, page }) => ({
  page,
  name,
  slug
}));

export const pagesById = reduceBy(pages, 'page');
