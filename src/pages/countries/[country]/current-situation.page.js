import { getData, getCountryPaths } from 'src/data';
import gridCn from 'src/style/grid.module.scss';
import layout from 'src/components/PageLayout';
import { navItems, pagesById } from './pages';
import { StyledHtml } from 'src/components/content/Html';
import Targets from 'src/components/Targets';
import cx from 'classnames';
import pageCn from 'src/style/page.module.scss';
import embeds from 'src/embedables';
import MethodologyLink from 'src/components/PageLayout/MethodologyLink';

function CurrentSituation({ siteContent, country, meta }) {
  const { content, data } = country;
  const historicEmissionsSpec = embeds.historic_emissions_breakdown;
  const historicEmissionsData = data[historicEmissionsSpec.dataId];

  return (
    <>
      <section
        className={cx(gridCn.grid, 'margin-bottom-xxxl', 's-margin-bottom-xl', 'margin-top-xl')}
        id="emissions-profile">
        <div className={cx(gridCn.col5, pageCn.sticky, 's-margin-bottom-xxl')}>
          <h2 className={cx(gridCn.col12, pageCn.sectionTitle)}>
            {siteContent.emissions_profile_title}
          </h2>
          <StyledHtml modifiers={['profile']} html={content.emissions_profile} />
          <MethodologyLink data={historicEmissionsData} />
        </div>
        <div className={cx(gridCn.col6, pageCn.sticky)}>
          <embeds.historic_emissions_breakdown.Component
            {...historicEmissionsData}
            {...historicEmissionsSpec}
            meta={meta}
            country={country}
          />
        </div>
      </section>
      <section className={cx(gridCn.grid, 'margin-bottom-xxxl')} id="energy-system">
        <h2 className={cx(gridCn.col10, pageCn.sectionTitle)}>{siteContent.energy_system_title}</h2>
        <StyledHtml
          modifiers={['profile']}
          html={content.energy_system}
          className={cx(gridCn.col10, gridCn.textCol2)}
        />
      </section>
      <section className={cx(gridCn.grid, 'margin-bottom-xl')} id="targets">
        <h2 className={cx(gridCn.col12, pageCn.sectionTitle)}>{siteContent.targets_title}</h2>
        <Targets country={country} siteContent={siteContent} meta={meta} />
      </section>
    </>
  );
}

CurrentSituation.SubLayout = layout({ pages: navItems, level: 2 });

export default CurrentSituation;

export async function getStaticPaths() {
  return getCountryPaths();
}

export async function getStaticProps({ params }) {
  return getData({ ...pagesById.current_situation, country: params.country });
}
