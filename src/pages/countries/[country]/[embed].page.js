import { getData } from 'src/data';
import { useRouter } from 'next/router';
import gridCn from 'src/style/grid.module.scss';
import { parseQueryObject } from 'src/utils';
import embeds from 'src/embedables';
import cx from 'classnames';
import cn from '../style.module.scss';
//import Head from 'next/head';

function Embed({ embed, isEmbed, isStatic, isBare, country, meta, siteContent }) {
  const router = useRouter();
  const parsedQuery = parseQueryObject(router.query);

  const { Component, id, dataId } = embeds[embed];

  // bla
  // isEmbed -> render embed layout
  // isStatic -> render embed layout ->  don't render any controls

  const component = router.isReady && (
    <Component
      siteContent={siteContent}
      {...country.data[dataId]} // data
      {...parsedQuery} // options
      id={id} // id for creating embed url
      dataId={dataId} // id for creating embed url
      country={country}
      isStatic={isStatic}
      meta={meta}
    />
  );

  return (
    <>
      {/* <Head>
        <meta property="og:type" content="website" />
        <meta key="og-type" property="og:type" content="website" />
        <meta key="og-url" property="og:url" content={url} />
        <meta key="og-description" property="og:description" content={description} />
        <meta key="og-image" property="og:image" content={imageUrl} />
      </Head> */}
      {isBare ? (
        component
      ) : (
        <div
          className={cx(
            gridCn.grid,
            cn.embedWrapper,
            isEmbed && cn.isEmbed,
            isStatic && cn.isStatic,
            isStatic && 'static'
          )}>
          <div className={gridCn.col12}>{component}</div>
        </div>
      )}
    </>
  );
}

export default Embed;

export async function getStaticPaths() {
  const data = await getData();

  const paths = data.props.meta.countries.reduce(
    (acc, country) => [
      ...acc,
      ...Object.values(embeds).map((embed) => ({
        params: { country: country.slug, embed: embed.id }
      }))
    ],
    []
  );

  return {
    paths: paths,
    fallback: false
  };
}

export async function getStaticProps({ params }) {
  const embed = embeds[params.embed];
  const data = await getData({
    country: params.country,
    embed: params.embed,
    endpoints: [{ id: 'country', dataIds: [embed.dataId] }]
  });
  return data;
}
