import { getData, getCountryPaths } from 'src/data';
import gridCn from 'src/style/grid.module.scss';
import layout from 'src/components/PageLayout';
import { StyledHtml } from 'src/components/content/Html';
import { pagesById, navItems } from './pages';
import cx from 'classnames';
import cn from '../style.module.scss';
import MethodologyLink from 'src/components/PageLayout/MethodologyLink';

import pageCn from 'src/style/page.module.scss';

import embedables from 'src/embedables';

function AmbitionGap({ siteContent, country, meta }) {
  const { content, data } = country;

  const totalGhgEmissionsSpec = embedables.total_ghg_emissions;
  const totalGhgEmissionsData = data[totalGhgEmissionsSpec.dataId];

  const primaryEnergyEmissionsSpec = embedables.primary_energy_mix;
  const primaryEnergyEmissionsData = data[primaryEnergyEmissionsSpec.dataId];

  const totalCo2EmissionsSpec = embedables.total_co2_emissions;
  const totalCo2EmissionsData = data[totalCo2EmissionsSpec.dataId];

  const keyfactsSpec = embedables.keyfacts_emissions;
  const keyfactsData = data[keyfactsSpec.dataId];

  return (
    <>
      <section
        className={cx(gridCn.grid, 'margin-top-m', 'margin-bottom-xxl')}
        id="compatible-pathways">
        <h2 className={cx(gridCn.col12, 'margin-bottom-xl', pageCn.sectionTitle)}>
          {siteContent.compatible_pathways_title}
        </h2>
        <div className={cx(gridCn.col12, 'margin-bottom-l')}>
          <div className={cx('margin-bottom-xl')}>
            <StyledHtml
              className={gridCn.textCol2}
              html={content.compatible_pathways}
              modifiers={['profile']}
            />
            <MethodologyLink data={totalGhgEmissionsData} />
          </div>
          <totalGhgEmissionsSpec.Component
            {...totalGhgEmissionsData}
            {...totalGhgEmissionsSpec}
            meta={meta}
            country={country}
          />
        </div>
      </section>
      <section className={cx(gridCn.grid, 'margin-bottom-xxxl')} id="energy-system">
        <h2 className={cx(gridCn.col12, pageCn.sectionTitle, 'margin-bottom-xl')}>
          {siteContent.energy_system_title}
        </h2>
        <div className={cx(gridCn.col5, pageCn.sticky)}>
          <StyledHtml modifiers={['profile']} html={content.energy_system} />
          {country.hasDetailedAnalysis && <MethodologyLink data={primaryEnergyEmissionsData} />}
        </div>
        <div className={cx(gridCn.col5, gridCn.offset6, pageCn.sticky)}>
          {country.hasDetailedAnalysis && (
            <div className={'margin-bottom-xxl'}>
              <primaryEnergyEmissionsSpec.Component
                {...primaryEnergyEmissionsData}
                {...primaryEnergyEmissionsSpec}
                meta={meta}
                country={country}
              />
            </div>
          )}
          <div>
            <totalCo2EmissionsSpec.Component
              {...totalCo2EmissionsData}
              {...totalCo2EmissionsSpec}
              meta={meta}
              country={country}
            />
          </div>
        </div>
      </section>

      <section className={cx('margin-bottom-xl')} id="benchmark-table">
        <div className={cx(gridCn.grid, cn.keyfactsTable)}>
          <div className={gridCn.col12}>
            <keyfactsSpec.Component
              {...keyfactsData}
              {...keyfactsSpec}
              meta={meta}
              country={country}
            />
          </div>
        </div>
      </section>
    </>
  );
}

AmbitionGap.SubLayout = layout({ pages: navItems, level: 2, className: cn.ambitionGap });

export default AmbitionGap;

export async function getStaticPaths() {
  return getCountryPaths();
}

export async function getStaticProps({ params }) {
  return getData({ ...pagesById.ambition_gap, country: params.country });
}
