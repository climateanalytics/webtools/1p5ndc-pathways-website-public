import { getData, getCountryPaths } from 'src/data';
import layout from 'src/components/PageLayout';
import { pagesById, navItems } from './pages';
import cx from 'classnames';
import cn from '../style.module.scss';
import { StyledHtml } from 'src/components/content/Html';
import gridCn from 'src/style/grid.module.scss';
import pageCn from 'src/style/page.module.scss';
import iconCn from 'src/style/icon.module.scss';
import buttonCn from 'src/style/button.module.scss';
import embedables from 'src/embedables';
import Link from 'next/link';

const page = pagesById.in_brief;

const InBrief = ({ siteContent, country, meta }) => {
  const { content, data } = country;

  return (
    <>
      <section className={cx('margin-bottom-l', 'margin-top-xl')}>
        <div className={cx(gridCn.grid)}>
          <div className={gridCn.col6}>
            <h2 className={pageCn.sectionTitle}>{siteContent.economy_wide_title}</h2>
            <StyledHtml
              className={cx(pageCn.sectionLead, 'margin-bottom-xxl', pageCn.enhanceContent)}
              html={country.content.ambition_gap_intro}
            />
          </div>
        </div>
        <div className={cx(gridCn.grid, gridCn.preventMobileOverflow, 'margin-bottom-xxxl')}>
          <div className={cx(gridCn.col10)}>
            <embedables.total_ghg_emissions.Component
              {...data[embedables.total_ghg_emissions.dataId]}
              {...embedables.total_ghg_emissions}
              meta={meta}
              country={country}
              showNDCGraph={false}
            />
          </div>
        </div>
        <div className={cx(gridCn.grid, 'margin-bottom-xl')}>
          {content.economy_wide_key_messages.map(({ name, content }, i) => (
            <div key={i} className={cx(gridCn.col6, cn.keyMessage)}>
              <div>
                <h3 className={cn.keyMessageTitle}>{name}</h3>
                <StyledHtml key={i} html={content} modifiers={['profile', cn.keyMessageContent]} />
              </div>
            </div>
          ))}
        </div>
        <div className={cx(gridCn.grid)}>
          <div className={cx(pageCn.sectionLinks, gridCn.col12)}>
            <Link href={`/countries/${country.slug}/current-situation`}>
              <a className={pageCn.sectionLink}>
                <span className={buttonCn.buttonText}>
                  {siteContent.read_current_situation_cta}
                </span>
              </a>
            </Link>
            <Link href={`/countries/${country.slug}/ambition-gap`}>
              <a className={pageCn.sectionLink}>{siteContent.read_full_analysis_cta}</a>
            </Link>
          </div>
        </div>
      </section>
      <section id="sectors">
        <div className={gridCn.grid}>
          <div className={gridCn.col6}>
            <h2 className={cx(pageCn.sectionTitle)}>{siteContent.sectors_title}</h2>
            <StyledHtml
              className={cx(pageCn.sectionLead, pageCn.enhanceContent)}
              html={content.sectors_intro}
            />
          </div>
        </div>
        <div className={gridCn.grid}>
          {content.sectors_key_messages.map(({ id, content }, i) => {
            const sector = meta.sectorsById[id];
            return (
              <div key={i} className={cx(gridCn.col6, cn.keyMessage)}>
                <div>
                  <h3 className={cn.keyMessageTitle}>
                    <span className={cx(iconCn.bare, iconCn.before, `icon-${id}`)} />
                    {sector.name}
                  </h3>
                  <StyledHtml html={content} modifiers={['profile', cn.keyMessageContent]} />
                </div>
                <Link href={`/countries/${country.slug}/sectors/${id}`}>
                  <a className={cx(cn.keyMessageLink, buttonCn.bare)}>
                    {siteContent.read_full_analysis_cta}
                  </a>
                </Link>
              </div>
            );
          })}
        </div>
      </section>
    </>
  );
};

InBrief.SubLayout = layout({ pages: navItems, level: 2 });

export default InBrief;

export async function getStaticPaths() {
  return getCountryPaths();
}

export async function getStaticProps({ params }) {
  return getData({ ...page, country: params.country });
}
