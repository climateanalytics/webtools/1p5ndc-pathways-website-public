import { getData, getMeta } from 'src/data';
import layout from 'src/components/PageLayout';
import { pagesById, navItems } from '../pages';
import { useRouter } from 'next/router';
import Layout from './Layout';
import Decarbonisation from './Decarbonisation.js';

function Sectors({ country, meta, siteContent }) {
  const { query } = useRouter();

  return (
    <Layout country={country} meta={meta} siteContent={siteContent}>
      <Decarbonisation country={country} meta={meta} sector={query.sector} />
    </Layout>
  );
}

Sectors.SubLayout = layout({ pages: navItems, level: 2 });

export default Sectors;

export async function getStaticPaths() {
  const { countries } = await getMeta();

  const paths = countries
    .filter((country) => country.hasDetailedAnalysis)
    .reduce((acc, country) => {
      const sectorPaths = ['industry', 'transport', 'buildings']
        .filter((sector) => country.published_sectors.includes(sector))
        .map((sector) => ({
          params: {
            country: country.slug,
            sector
          }
        }));
      return [...acc, ...sectorPaths];
    }, []);
  return { paths, fallback: false };
}

export async function getStaticProps({ params }) {
  return getData({ ...pagesById.sectors, country: params.country });
}
