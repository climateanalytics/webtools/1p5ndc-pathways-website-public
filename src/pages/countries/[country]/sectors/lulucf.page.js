import gridCn from 'src/style/grid.module.scss';
import { StyledHtml } from 'src/components/content/Html';
import layout from 'src/components/PageLayout';
import { pagesById, navItems } from '../pages';
import pageCn from 'src/style/page.module.scss';
import cx from 'classnames';
import embedables from 'src/embedables';
import { getCountryPaths, getData } from 'src/data';
import Layout from './Layout';

const Power = ({ siteContent, country, meta }) => {
  const { content, data } = country;

  const timelineSpec = embedables.lulucf_sector_timeline;
  const timelineData = data[timelineSpec.dataId];

  const emissionsSpec = embedables.lulucf_emissions;
  const emissionsData = data[emissionsSpec.dataId];

  const landuseSpec = embedables.lulucf_land_use;
  const landuseData = data[landuseSpec.dataId];

  const forestSpec = embedables.lulucf_forest;
  const forestData = data[forestSpec.dataId];

  return (
    <Layout country={country} meta={meta} siteContent={siteContent} sector="lulucf">
      <section className="margin-vertical-xxxl">
        <div className={cx(gridCn.grid, 'margin-bottom-l')}>
          <div className={cx(gridCn.col5, pageCn.sticky)}>
            <h3 className={pageCn.subSectionTitle}>{siteContent.lulucf_emissions_title}</h3>
            <StyledHtml html={content.lulucf_emissions} modifiers={['profile']} />
          </div>
          <div className={cx(gridCn.col6, gridCn.offset6, pageCn.sticky)}>
            <div className={'margin-bottom-l'}>
              <emissionsSpec.Component
                {...emissionsData}
                {...emissionsSpec}
                meta={meta}
                country={country}
              />
            </div>
          </div>
        </div>
        <div className={cx(gridCn.grid, 'margin-bottom-l')}>
          <div className={cx(gridCn.col5, pageCn.sticky)}>
            <h3 className={pageCn.subSectionTitle}>{siteContent.lulucf_forest_title}</h3>
            <StyledHtml html={content.lulucf_forest} modifiers={['profile']} />
          </div>
          <div className={cx(gridCn.col6, gridCn.offset6, pageCn.sticky)}>
            <div className={'margin-bottom-l'}>
              <forestSpec.Component {...forestData} {...forestSpec} meta={meta} country={country} />
            </div>
          </div>
        </div>
        <div className={cx(gridCn.grid, 'margin-bottom-l')}>
          <div className={cx(gridCn.col5, pageCn.sticky)}>
            <h3 className={pageCn.subSectionTitle}>{siteContent.lulucf_land_use_title}</h3>
            <StyledHtml html={content.lulucf_land_use} modifiers={['profile']} />
          </div>
          <div className={cx(gridCn.col6, gridCn.offset6, pageCn.sticky)}>
            <div className={'margin-bottom-l'}>
              <landuseSpec.Component
                {...landuseData}
                {...landuseSpec}
                meta={meta}
                country={country}
              />
            </div>
          </div>
        </div>
      </section>
    </Layout>
  );
};

Power.SubLayout = layout({ pages: navItems, level: 2 });

export async function getStaticPaths() {
  return getCountryPaths(
    (country) =>
      country.hasDetailedAnalysis && country.published_sectors.includes('lulucf') && country.slug
  );
}

export async function getStaticProps({ params }) {
  return getData({ ...pagesById.sectors, country: params.country });
}

export default Power;
