import { useMemo, useRef } from 'react';
import gridCn from 'src/style/grid.module.scss';
import iconCn from 'src/style/icon.module.scss';
import cn from '../../style.module.scss';
import cx from 'classnames';

import { useRouter } from 'next/router';
import ContentNav from 'src/components/ContentNav';

function Layout({ country, meta, siteContent, children, sector }) {
  const { query } = useRouter();
  const contentRef = useRef(null);

  const sectorPages = useMemo(() => {
    const allSectors = [
      {
        slug: 'power',
        hashLink: false,
        children: [
          {
            slug: 'decarbonisation',
            name: siteContent.power_decarbonisation_title
          },
          country.content.power_has_investments && {
            slug: 'investments',
            name: siteContent.power_investments_title
          },
          country.content.power_has_benefits && {
            slug: 'co-benefits',
            name: siteContent.power_co_benefits_title
          }
        ].filter((d) => d)
      },
      {
        slug: 'buildings',
        hashLink: false
      },
      {
        slug: 'transport',
        hashLink: false
      },
      {
        slug: 'industry',
        hashLink: false
      },
      {
        slug: 'lulucf',
        hashLink: false
      }
    ];

    const pages = allSectors
      .map(
        (sector) =>
          country.published_sectors.includes(sector.slug) && {
            ...sector,
            ...(meta.sectorsById[sector.slug] || {})
          }
      )
      .filter((d) => d);
    pages.push({
      name: siteContent.sectors_overview_title,
      //separator: true,
      slug: '',
      hashLink: false
    });
    return pages;
  }, [country, meta, siteContent]);

  const sectorId = query.sector || sector;
  const sectorName = meta.sectorsById[sector]?.name;

  return (
    <div className={cx(gridCn.grid, 'margin-top-xxl')}>
      <div className={gridCn.col2}>
        <ContentNav
          basePath={`/countries/${country.slug}/sectors/`}
          contentRef={contentRef}
          items={sectorPages}
          current={sectorId}
          openCta={siteContent.sectors_label}
          closeCta={siteContent.close_label}
        />
      </div>
      <section ref={contentRef} className={cx(gridCn.col10, gridCn.offset2)}>
        {false && (
          <h1 className={cn.sectorTitle}>
            <span className={cx(iconCn.bare, iconCn.s, iconCn.before, `icon-${sector}`)} />
            {sectorName}
          </h1>
        )}
        {children}
      </section>
    </div>
  );
}

export default Layout;
