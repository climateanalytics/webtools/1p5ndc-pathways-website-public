import embedables from 'src/embedables';
import { StyledHtml } from 'src/components/content/Html';
import pageCn from 'src/style/page.module.scss';
import gridCn from 'src/style/grid.module.scss';
import layout from 'src/components/PageLayout';
import cx from 'classnames';
import { pagesById, navItems } from '../pages';
import { getCountryPaths, getData } from 'src/data';
import Layout from './Layout';

const { sectoral_emissions } = embedables;

const Overview = ({ siteContent, country, meta }) => {
  const { content, data } = country;

  return (
    <Layout country={country} meta={meta} siteContent={siteContent} sector="">
      <section className="margin-bottom-xxxl">
        <div className={cx(gridCn.grid, 'margin-bottom-l')}>
          <div className={cx(gridCn.col6, pageCn.sticky)}>
            <h2 className={pageCn.sectionTitle}>{siteContent.sectors_overview_title}</h2>
            <StyledHtml html={content.overview} modifiers={['profile']} />
          </div>
        </div>
        <div className={cx(gridCn.grid, 'margin-bottom-l')}>
          <div className={cx(gridCn.col7, pageCn.sticky)}>
            <sectoral_emissions.Component
              {...data[sectoral_emissions.id]}
              {...sectoral_emissions}
              country={country}
              meta={meta}
            />
          </div>
        </div>
      </section>
    </Layout>
  );
};

Overview.SubLayout = layout({ pages: navItems, level: 2 });

export async function getStaticPaths() {
  return getCountryPaths((country) => country.hasDetailedAnalysis && country.slug);
}

export async function getStaticProps({ params }) {
  return getData({ ...pagesById.sectors, country: params.country });
}

export default Overview;
