import gridCn from 'src/style/grid.module.scss';
import { StyledHtml } from 'src/components/content/Html';
import pageCn from 'src/style/page.module.scss';
import btnCn from 'src/style/button.module.scss';
import cx from 'classnames';
import embedables from 'src/embedables';
import cn from '../../style.module.scss';
import MethodologyLink from 'src/components/PageLayout/MethodologyLink';
import { useSiteContent } from 'src/data/siteContent';
import Link from 'next/link';

const Decarbonisation = ({
  country,
  meta,
  sector,
  energyMixReducer,
  showCapacitiesLink,
  showInvestmentNeedsLink
}) => {
  const siteContent = useSiteContent();
  const { content, data } = country;
  const timelineSpec = embedables[`${sector}_sector_timeline`];
  const timelineData = data[timelineSpec.dataId];

  const energyMixSpec = embedables[`${sector}_energy_mix`];
  const energyMixData = data[energyMixSpec.dataId];

  const emissionsSpec = embedables[`${sector}_emissions`];
  const emissionsData = data[emissionsSpec.dataId];

  const secondaryEmissionsSpec = sector === 'industry' && embedables.industry_processes_emissions;

  const keyfactsSpec = embedables[`${sector}_keyfacts`];
  const keyfactsData = data[keyfactsSpec.dataId];

  return (
    <>
      <section className={cx(gridCn.grid, cn.timelineSection, 'margin-bottom-xxxl')}>
        {timelineData && (
          <div className={cx('margin-bottom-xxl', gridCn.col12)}>
            <timelineSpec.Component
              {...timelineData}
              {...timelineSpec}
              meta={meta}
              country={country}
            />
          </div>
        )}
      </section>

      <section id="decarbonisation">
        <div className={cx(gridCn.grid, 'margin-bottom-xxxl')}>
          <div className={cx(gridCn.col5, pageCn.sticky)} id="energy-mix" style={{ zIndex: 0 }}>
            <StyledHtml html={content[sector]} modifiers={['profile']} />
            <div className={pageCn.sectionLinks}>
              <MethodologyLink data={energyMixData} />
              {showCapacitiesLink && (
                <a
                  href="#energy-mix"
                  className={cx(btnCn.bare, btnCn.s, btnCn.iconBefore)}
                  onClick={() => energyMixReducer.dispatch('gw', 'unit')}>
                  <span className="icon-file-graph" />
                  {siteContent.power_capacities_link_label}
                </a>
              )}
              {showInvestmentNeedsLink && (
                <a
                  href="#investments"
                  onClick={() => energyMixReducer.dispatch('twhy', 'unit')}
                  className={cx(btnCn.bare, btnCn.s, btnCn.iconBefore)}>
                  <span className="icon-file-graph" />
                  {siteContent.power_investments_link_label}
                </a>
              )}
            </div>
          </div>
          <div className={cx(gridCn.col6, gridCn.offset6, pageCn.sticky)} style={{ zIndex: 1 }}>
            <div className={'margin-bottom-xxxl'}>
              <energyMixSpec.Component
                {...energyMixData}
                {...energyMixSpec}
                country={country}
                meta={meta}
                stateReducer={energyMixReducer}
              />
            </div>

            <emissionsSpec.Component
              {...emissionsData}
              {...emissionsSpec}
              country={country}
              meta={meta}
            />
            {secondaryEmissionsSpec && (
              <div className={'margin-top-xxxl'}>
                <secondaryEmissionsSpec.Component
                  {...data[secondaryEmissionsSpec.dataId]}
                  {...secondaryEmissionsSpec}
                  country={country}
                  meta={meta}
                />
              </div>
            )}
          </div>
        </div>
        <div className={cx(gridCn.grid, 'margin-bottom-l')} style={{ zIndex: 0 }}>
          <div className={cx(gridCn.col12)}>
            <keyfactsSpec.Component
              {...keyfactsData}
              {...keyfactsSpec}
              country={country}
              meta={meta}
            />
          </div>
        </div>
      </section>
    </>
  );
};

export default Decarbonisation;
