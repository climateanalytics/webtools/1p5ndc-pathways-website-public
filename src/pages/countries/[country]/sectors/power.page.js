import gridCn from 'src/style/grid.module.scss';
import { StyledHtml } from 'src/components/content/Html';
import layout from 'src/components/PageLayout';
import { pagesById, navItems } from '../pages';
import pageCn from 'src/style/page.module.scss';
import cn from '../../style.module.scss';
import btnCn from 'src/style/button.module.scss';
import embedables from 'src/embedables';
import cx from 'classnames';
import { getCountryPaths, getData } from 'src/data';
import Layout from './Layout';
import Decarbonisation from './Decarbonisation';
import useUiReducer from 'src/utils/useUiReducer';

const CoBenefitsSubSection = ({ content, images, title }) => {
  if (content == '') return null;

  return (
    <div className={cx(gridCn.grid, 'margin-bottom-l')}>
      <div className={cx(gridCn.col5, pageCn.sticky)}>
        <h3 className={pageCn.subSectionTitle}>{title}</h3>
        <StyledHtml html={content} modifiers={['profile']} />
      </div>
      <div className={cx(gridCn.col7, pageCn.sticky)}>
        <div className={'margin-bottom-l'}>
          {images.map((file) => (
            <img
              alt=""
              className={cn.coBenefitsImage}
              key={file}
              src={`${process.env.mediaUrl}w600/${file}`}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

const Power = ({ siteContent, country, meta }) => {
  const { content, data } = country;
  const investmentsSpec = embedables.power_investments;
  const investmentsData = data[investmentsSpec.dataId];

  const costSpec = embedables.power_cost;
  const costData = data[costSpec.dataId];

  const energyMixReducer = useUiReducer({
    scaling: 'absolute',
    unit: 'twhy'
  });

  return (
    <Layout country={country} meta={meta} siteContent={siteContent} sector="power">
      <Decarbonisation
        country={country}
        meta={meta}
        sector="power"
        energyMixReducer={energyMixReducer}
        showCapacitiesLink={true}
        showInvestmentNeedsLink={content.power_has_investments}
      />

      {/* example: japan */}
      {content.power_has_investments && (
        <section className="margin-vertical-xxxl" id="investments">
          <div className={cx(gridCn.grid, 'margin-bottom-l')}>
            <div className={cx(gridCn.col5, pageCn.sticky)} style={{ zIndex: 0 }}>
              <h2 className={pageCn.sectionTitle}>{siteContent.power_investments_title}</h2>
            </div>
          </div>
          <div className={cx(gridCn.grid, 'margin-bottom-l')}>
            <div className={cx(gridCn.col5, pageCn.sticky)} style={{ zIndex: 0 }}>
              <h3 className={pageCn.subSectionTitle}>
                {siteContent.power_yearly_investments_title}
              </h3>
              <StyledHtml html={content.power_investments_shift} modifiers={['profile']} />
            </div>
            <div
              className={cx(gridCn.col6, gridCn.offset6, pageCn.sticky, 'margin-bottom-m')}
              style={{ zIndex: 2 }}>
              <investmentsSpec.Component
                {...investmentsData}
                {...investmentsSpec}
                country={country}
                meta={meta}
              />
            </div>
          </div>
          <div className={cx(gridCn.grid, 'margin-bottom-l')}>
            <div className={cx(gridCn.col5, pageCn.sticky)} style={{ zIndex: 0 }}>
              <h3 className={pageCn.subSectionTitle}>
                {siteContent.power_investments_shift_title}
              </h3>
              <StyledHtml html={content.power_investments} modifiers={['profile']} />

              {/* <h3 className={pageCn.subSectionTitle}>
                {siteContent.power_investments_demand_title}
              </h3>
              <StyledHtml html={content.power_investments_demand} modifiers={['profile']} /> */}
              <div className="padding-vertical-l">
                <a
                  href="#energy-mix"
                  className={cx(btnCn.bare, btnCn.s, btnCn.iconBefore)}
                  onClick={() => energyMixReducer.dispatch('gw', 'unit')}>
                  <span className="icon-file-graph" />
                  {siteContent.power_capacities_link_label}
                </a>
              </div>
            </div>
            {/* <div className={cx(gridCn.col6, gridCn.offset6, pageCn.sticky)} style={{ zIndex: 1 }}>
              <div className={'margin-bottom-l'}>
                <costSpec.Component {...costSpec} {...costData} country={country} meta={meta} />
              </div>
            </div> */}
          </div>
        </section>
      )}
      {content.power_has_benefits === undefined ||
        (content.power_has_benefits === true && (
          <section className="margin-vertical-xxxl" id="co-benefits">
            <div className={cx(gridCn.grid, 'margin-bottom-l')}>
              <div className={cx(gridCn.col5, pageCn.sticky)}>
                <h2 className={pageCn.sectionTitle}>{siteContent.power_co_benefits_title}</h2>
              </div>
            </div>
            <CoBenefitsSubSection
              title={siteContent.power_employment_benefits_title}
              content={content.power_employment_benefits}
              images={content.power_employment_benefits_images}
            />

            <CoBenefitsSubSection
              title={siteContent.power_health_benefits_title}
              content={content.power_health_benefits}
              images={content.power_health_benefits_images}
            />

            <CoBenefitsSubSection
              title={siteContent.power_cooling_water_benefits_title}
              content={content.power_cooling_water_benefits}
              images={content.power_cooling_water_benefits_images}
            />
          </section>
        ))}
    </Layout>
  );
};

Power.SubLayout = layout({ pages: navItems, level: 2 });

export async function getStaticPaths() {
  return getCountryPaths((country) => {
    return (
      country.hasDetailedAnalysis && country.published_sectors.includes('power') && country.slug
    );
  });
}

export async function getStaticProps({ params }) {
  return getData({ ...pagesById.sectors, country: params.country });
}

export default Power;
