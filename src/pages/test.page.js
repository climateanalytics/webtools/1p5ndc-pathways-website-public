import { getData } from 'src/data';
import gridCn from 'src/style/grid.module.scss';
import Tooltip, { useTooltip } from 'src/components/Tooltip/new.js';
import { useRef } from 'react';

const dataSpec = {};

export default function Home() {
  const easyTrigger = useTooltip({ easyTrigger: true });
  const hardTrigger = useTooltip({ easyTrigger: false });

  return (
    <>
      <section className={gridCn.grid}>
        <div className={gridCn.col6}>
          The :focus pseudo-class does not discriminate based on how the element entered focus in
          the first place. So indeed, this is not possible with just CSS. At the very least you'd
          need to annotate the element on focus via an event handler. The :hover and :active
          pseudo-classes won't be of any help here since the former only applies when the mouse
          pointer is on the element and the latter only applies when the mouse button is down, i.e.
          neither state persists the way :focus does, since an element remains in focus even after
          the mouse pointer has left the element, making it indistinguishable from an element that
          received focus via tabbing. The :focus pseudo-class does not discriminate based on how the
          element entered focus in the first place. So indeed, this is not possible with just CSS.
          At the very least you'd need to annotate the element on focus via an event handler. The
          :hover and :active pseudo-classes won't be of any help here since the former only applies
          when the mouse pointer is on the element and the latter only applies when the mouse button
          is down, i.e. neither state persists the way :focus does, since an element remains in
          focus even after the mouse pointer has left the element, making it indistinguishable from
          an element that received focus via tabbing.
        </div>
      </section>
      <section className={gridCn.grid}>
        <button id="trigger-1" {...easyTrigger.triggerProps}>
          some info
        </button>
        {/* <button {...hardTrigger.triggerProps}>some info {hardTrigger.visible && 'bla'}</button> */}
        <Tooltip
          tooltipFor="trigger-1"
          orientation="s"
          alignment="center"
          visible={easyTrigger.visible}>
          :active pseudo-classes won't be of any help here since the former only applies when the
          mouse pointer is on the element and the latter only applies when the mouse button is down,
          i.e. neither state persists the way :focus does, since an element remains in focus even
          after the mouse pointer has left the element, making it indistinguishable from an element
          that received focus via tabbing.
        </Tooltip>
      </section>
    </>
  );
}

export async function getStaticProps() {
  return getData(dataSpec);
}
