import gridCn from 'src/style/grid.module.scss';
import cn from './style.module.scss';
import pageCn from 'src/style/page.module.scss';
import cx from 'classnames';

const Typography = () => {
  return (
    <section className={cx(gridCn.grid, cn.section, pageCn.content)}>
      <div className={gridCn.col12}>
        <h2>Typography</h2>
      </div>
      <div className={gridCn.col6}>
        <h3>Content text styles</h3>
        <p>
          Every element (and its child elements) that has the <code>.content</code> class from{' '}
          <code>src/style/page.module.scss</code> applied, will be styled with some default styles
          for basic html elements
        </p>
      </div>
      <div className={gridCn.col6}>
        <h1>Some h1 heading</h1>
        <h2>And a smaller h2 title</h2>
        <h3>And a h3 heading</h3>
        <h4>A followup h4 heading</h4>
        <h5>Then let's se h5</h5>
        <h6>And last but not least h6</h6>
        <p>
          Lorem ipsum dolor sit amet, <b>consectetur</b> adipiscing elit. Donec <i>est magna</i>,
          tincidunt non lectus eu, efficitur sagittis odio. Aliquam nibh orci, suscipit sit amet
          pulvinar ac, pharetra id quam. Nunc convallis accumsan urna, vel pulvinar ex vulputate sit
          amet. Nunc aliquam eleifend sapien, non consequat lacus blandit id. Suspendisse ac finibus
          nisi, eu malesuada augue. <u>Mauris sed nisi</u> ligula. Phasellus feugiat mauris at mi
          porttitor, non venenatis tortor mattis. Nam enim erat, gravida non ornare a, consequat
          eget felis. Nunc odio augue, ornare at lacus ut, mattis varius nunc. Aenean scelerisque
          tempus libero, vitae maximus quam blandit vitae. Duis vestibulum leo ac tortor gravida
          gravida. Vestibulum bibendum facilisis semper.
        </p>
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec est magna, tincidunt non
          lectus eu, efficitur sagittis odio. Aliquam nibh orci, suscipit sit amet pulvinar ac,
          pharetra id quam.
        </p>
        <p>What follows are some examples lists:</p>
        <ul>
          <li>This is a list item</li>
          <li>
            And this is another list item. This time with some longer text to see how lists look
            with line breaks within a single item. Quisque erat dolor, malesuada non ultricies in,
            elementum nec dolor. Etiam euismod tellus ut orci tincidunt scelerisque. Donec sed augue
            eu sapien suscipit tempor.
          </li>
        </ul>
        <ol>
          <li>This is a list item with a number instead of a bullet</li>
          <li>
            And this is another list item. This time with some longer text to see how lists look
            with line breaks within a single item. Quisque erat dolor, malesuada non ultricies in,
            elementum nec dolor. Etiam euismod tellus ut orci tincidunt scelerisque. Donec sed augue
            eu sapien suscipit tempor.
          </li>
        </ol>
      </div>
    </section>
  );
};

export default Typography;
