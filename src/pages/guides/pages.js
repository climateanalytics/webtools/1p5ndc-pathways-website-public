export const pages = [
  {
    slug: 'content-entry',
    name: 'Content entry'
  },
  {
    slug: 'style-guide',
    name: 'Style guide'
  },
  {
    slug: 'controls',
    name: 'Controls'
  }
];
