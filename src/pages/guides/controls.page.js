import { getData } from 'src/data';
import { Fragment } from 'react';
import gridCn from 'src/style/grid.module.scss';
import layout from 'src/components/PageLayout';
import cn from './style.module.scss';
import cx from 'classnames';
import { pages } from './pages';
import Link from 'next/link';
import buttonCn from 'src/style/button.module.scss';
import SimpleSelect from 'src/components/controls/SimpleSelect';
import SearchableSelect from 'src/components/controls/SearchableSelect';
import SegmentedControl from 'src/components/controls/SegmentedControl';
import Tabs from 'src/components/controls/Tabs';
import { reduceBy } from 'src/utils';

const buttonTypes = ['bare', 'base', 'cta', 'outline', 'ghost'];
const buttonSizes = ['s', 'm', 'l'];

const flatItems = [
  {
    id: 'one',
    name: 'India',
    href: '/countries/IND',
    modifiers: [cn.customSelectItem]
  },
  {
    id: 'two',
    name: 'Climate Analytics',
    href: 'https://climateanalytics.org/',
    external: true,
    iconAfter: 'angle-right'
  },
  {
    id: 'three',
    name: 'Do something'
  },
  {
    id: 'four',
    name: 'Download'
  },
  {
    id: 'five',
    name: 'Go somewhere',
    iconAfter: 'arrow-right'
  }
];

const groupedItems = [
  {
    id: 'group-one',
    name: 'Group one',
    children: [
      {
        id: 'group-one-child-1',
        name: 'Child one'
      },
      {
        id: 'group-one-child-2',
        name: 'Child two'
      }
    ]
  },
  {
    id: 'group-two',
    children: [
      {
        id: 'group-two-child-1',
        name: 'Child one'
      },
      {
        id: 'group-two-child-2',
        name: 'Child two'
      }
    ]
  },
  {
    id: 'ungrouped-1',
    name: 'Eeo, no group',
    iconBefore: 'download'
  },
  {
    id: 'ungrouped-2',
    name: 'Uh, also no group',
    iconBefore: 'plus'
  }
];

const customItems = [
  {
    id: 'methodology',
    name: 'Methodology'
  },
  {
    id: 'source',
    name: 'Methodology',
    text: 'Some source name'
  }
];

const segments = [
  {
    id: 'one',
    name: 'One',
    iconBefore: 'plus'
  },
  {
    id: 'two',
    name: 'Two',
    iconBefore: 'search'
  },
  {
    id: 'three',
    name: 'Three'
  }
];

const iconSegments = [
  {
    id: 'two',
    name: 'Two',
    icon: 'document'
  },
  {
    id: 'four',
    name: 'Three',
    icon: 'download'
  }
];

export default function Ctrls() {
  return (
    <div className={gridCn.grid}>
      <div className={gridCn.col12}>
        <h2>Components</h2>
      </div>
      {buttonSizes.map((size) => {
        const buttons = buttonTypes.map((type) => {
          return (
            <Fragment key={type}>
              <div className={cx(cn.row, gridCn.col1)}>{type}</div>
              <div className={cx(gridCn.col11, cn.row)}>
                <button className={cx(buttonCn[type], buttonCn[size], cn.control)}>
                  Download data
                </button>
                <button
                  className={cx(buttonCn[type], buttonCn[size], buttonCn.iconBefore, cn.control)}>
                  <span className="icon-download" />
                  Download Data
                </button>
                <button
                  className={cx(buttonCn[type], buttonCn[size], buttonCn.iconAfter, cn.control)}>
                  Visit page
                  <span className="icon-east" />
                </button>
                <button
                  className={cx(buttonCn[type], buttonCn[size], cn.buttonOverflowTest, cn.control)}>
                  <span className={buttonCn.buttonText}>Button with a very long label</span>
                </button>
                <button disabled className={cx(buttonCn[type], buttonCn[size], cn.control)}>
                  Disabled button
                </button>
                <button
                  className={cx(
                    buttonCn[type],
                    buttonCn[size],
                    buttonCn.iconAfter,
                    cn.buttonOverflowTest,
                    cn.control
                  )}>
                  <span className={buttonCn.buttonText}>Button with a very long label</span>
                  <span className="icon-east" />
                </button>
                <button className={cx(buttonCn[type], buttonCn[size], buttonCn.icon, cn.control)}>
                  <span className="icon-download" />
                </button>
                <button className={cx(buttonCn[type], buttonCn[size], buttonCn.pill, cn.control)}>
                  Pill button
                </button>
              </div>
            </Fragment>
          );
        });
        return (
          <Fragment key={size}>
            <div className={gridCn.col12}>
              <h3>Button size: {size.toUpperCase()}</h3>
            </div>
            {buttons}
          </Fragment>
        );
      })}
      <div className={gridCn.col12}>
        <h3>Simple select</h3>
      </div>
      <div className={cx(gridCn.col12, cn.row)}>
        <SimpleSelect
          id="test-0"
          label="Select label"
          buttonLabel="Select options"
          size="m"
          items={flatItems}
          buttonModifiers={['bare']}
          wrapperModifiers={[cn.control]}
        />
        <SimpleSelect
          id="test-1"
          label="Select label"
          buttonLabel="Select options"
          size="s"
          items={flatItems}
          wrapperModifiers={[cn.control]}
          description="Some description for select"
        />
        <SimpleSelect
          id="test-1a"
          label="Select label"
          buttonModifiers={['ghost']}
          size="s"
          items={groupedItems}
          wrapperModifiers={[cn.control]}
          value={groupedItems[0].children[1].id}
        />
        <SimpleSelect
          id="test-2"
          label="Select label"
          buttonLabel="Select options"
          buttonModifiers={['cta']}
          items={groupedItems}
          wrapperModifiers={[cn.control]}
        />
        <SimpleSelect
          id="test-3"
          buttonLabel="Select options"
          buttonModifiers={['outline']}
          size="m"
          buttonIcon="download"
          items={flatItems}
          wrapperModifiers={[cn.control]}
        />
        <SimpleSelect
          id="test-4"
          label="Select label"
          buttonLabel="Select options"
          buttonModifiers={['ghost']}
          size="l"
          items={flatItems}
          disabled={true}
          wrapperModifiers={[cn.control]}
        />
        <SimpleSelect
          id="test-5"
          label="Select label"
          buttonModifiers={['ghost', 'icon']}
          size="s"
          buttonIcon="download"
          items={flatItems}
          wrapperModifiers={[cn.control]}
        />
        <SimpleSelect
          id="test-6"
          label="Custom"
          buttonModifiers={['base', cn.customButton]}
          items={flatItems}
          wrapperModifiers={[cn.control, cn.customWrapper]}
        />
        <SimpleSelect
          id="test-6"
          label="Custom tray"
          buttonModifiers={['ghost', 'icon']}
          buttonIcon="info"
          items={customItems}
          renderWhenClosed={true}
          wrapperModifiers={[cn.control]}>
          {({ items, getItemProps }) => {
            const { methodology, source } = reduceBy(items);
            return (
              <>
                <p>This is some description text</p>
                <Link href={`/methodology/#${methodology.href}`}>
                  <a {...getItemProps({ methodology, index: items.indexOf(methodology) })}>
                    {methodology.name}
                  </a>
                </Link>
                <p {...getItemProps({ methodology, index: items.indexOf(methodology) })}>
                  <b>{source.name}</b> — {source.text}
                </p>
              </>
            );
          }}
        </SimpleSelect>
      </div>
      <div className={gridCn.col12}>
        <h3>Searchable select</h3>
      </div>
      <div className={cx(gridCn.col12, cn.row)}>
        <SearchableSelect
          id="test-1"
          label="Select label"
          buttonLabel="Select options"
          size="s"
          items={flatItems}
          wrapperModifiers={[cn.control]}
          value={flatItems[0].id}
        />
        <SearchableSelect
          id="test-1"
          label="Select label"
          buttonLabel="Select options"
          size="l"
          items={flatItems}
          buttonModifiers={['cta']}
          wrapperModifiers={[cn.control]}
        />
      </div>
      <div className={gridCn.col12}>
        <h3>Segmented control</h3>
      </div>
      <div className={cx(gridCn.col12, cn.row)}>
        <SegmentedControl
          id="test-1"
          label="The control"
          size="s"
          segments={segments}
          wrapperModifiers={[cn.control]}
          value={segments[0].id}
        />
        <SegmentedControl
          id="test-1"
          label="The control"
          segments={segments}
          segmentModifiers={['cta']}
          wrapperModifiers={[cn.control]}
          value={segments[0].id}
        />
        <SegmentedControl
          id="test-1"
          label="The control"
          segments={segments}
          size="l"
          segmentModifiers={['outline']}
          wrapperModifiers={[cn.control]}
          value={segments[0].id}
        />
        <SegmentedControl
          id="test-1"
          label="The control"
          size="s"
          segments={segments}
          segmentModifiers={['ghost']}
          wrapperModifiers={[cn.control, cn.customSegmentedControl]}
          value={segments[0].id}
        />
        <SegmentedControl
          id="test-1"
          label="The control"
          size="s"
          segments={iconSegments}
          segmentModifiers={['ghost']}
          wrapperModifiers={[cn.control]}
          value={segments[0].id}
        />
      </div>
      <div className={gridCn.col12}>
        <h3>Tabs</h3>
      </div>
      <div className={cx(gridCn.col12, cn.row)}>
        <Tabs
          id="test-tabs"
          tabs={[
            {
              name: 'Tab one',
              Panel: ({ panelProps }) => <div {...panelProps}>Content of panel one</div>
            },
            {
              name: 'Tab two',
              Panel: ({ panelProps }) => <div {...panelProps}>Content of panel two</div>
            },
            {
              name: 'Tab three'
            }
          ]}
        />
      </div>
    </div>
  );
}

Ctrls.SubLayout = layout({ pages });

export async function getStaticProps() {
  return getData();
}
