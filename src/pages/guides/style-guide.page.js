import { getData } from 'src/data';
import { Fragment } from 'react';
import gridCn from 'src/style/grid.module.scss';
import pageCn from 'src/style/page.module.scss';
import layout from 'src/components/PageLayout';
import Typography from './Typography';
import cn from './style.module.scss';
import cx from 'classnames';
import { pages } from './pages';
import { useTheme } from 'src/style/ThemeProvider';

export default function StyleGuide() {
  return (
    <>
      <div className={pageCn.page}>
        <section className={cx(gridCn.grid, cn.section, pageCn.content)}>
          <div className={cx(gridCn.col12)}>
            <h1>Style Guide</h1>
            <h2>Grid</h2>
          </div>
        </section>
        <section className={cx(gridCn.grid, cn.section)}>
          <div className={cx(gridCn.col1, gridCn.debug)}>.col1</div>
          <div className={cx(gridCn.col3, gridCn.debug)}>.col3</div>
          <div className={cx(gridCn.col3, gridCn.debug)}>.col3</div>
          <div className={cx(gridCn.col5, gridCn.debug)}>.col5</div>
        </section>
        <section className={cx(gridCn.grid, cn.section)}>
          <div className={cx(gridCn.col3, gridCn.offset4, gridCn.debug)}>.col3.offset4</div>
          <div className={cx(gridCn.col5, gridCn.debug)}>.col5</div>
        </section>

        <Typography />
        <div className={gridCn.grid}>
          <div className={gridCn.col12}>
            <h2>Colors</h2>
          </div>
        </div>
      </div>
    </>
  );
}

StyleGuide.SubLayout = layout({ pages });

export async function getStaticProps() {
  return getData();
}
