import { getData } from 'src/data';
import { useRef } from 'react';
import gridCn from 'src/style/grid.module.scss';
import layout from 'src/components/PageLayout';
import { StyledHtml } from 'src/components/content/Html';
import cn from './style.module.scss';
import pageCn from 'src/style/page.module.scss';
import cx from 'classnames';
import { pages } from './pages';
import ContentNav from 'src/components/ContentNav';
import useContentHierarchy from 'src/utils/useContentHierarchy';

const MetaTable = ({ items, prefix, createExample = true, name }) => {
  return (
    <table>
      <thead>
        <tr>
          <th>{name}</th>
          <th>ID</th>
        </tr>
      </thead>
      <tbody>
        {items.map((item) => (
          <tr key={item.id}>
            <td>
              <b className={cx(createExample && `${prefix}-${item.kebapId || item.id}`)}>
                {item.name}
              </b>
            </td>
            {item.id && (
              <td>
                {prefix}-{item.kebapId || item.id}
              </td>
            )}
          </tr>
        ))}
      </tbody>
    </table>
  );
};

export default function ContentEntry({ meta, siteContent, pageContent }) {
  const contentRef = useRef(null);
  const toc = useContentHierarchy(contentRef);

  const infoItems = Object.entries(siteContent.info).reduce((acc, [key, value]) => {
    const isTitle = /_title$/.test(key);
    const id = key.replace(/_title$/, '');
    if (isTitle) acc.push({ name: value.replace(/<[^>]*>/g, ''), id });
    return acc;
  }, []);

  const emissionsItems = [
    { id: 'ndc', name: 'NDC' },
    { id: 'ndc-compat', name: '1.5° Compatible pathway or range' },
    { id: 'cpp', name: 'Current policy pathway' },
    { id: 'ambition-gap', name: 'Ambition gap' }
  ];

  return (
    <div className={cx(gridCn.grid, pageCn.page, pageCn.enhanceContent)}>
      <div className={gridCn.col3}>
        <ContentNav items={toc} contentRef={contentRef} />
      </div>
      <div className={cx(gridCn.col7, pageCn.content)} ref={contentRef}>
        <h1>{pageContent.name}</h1>
        <section className={cn.section}>
          <StyledHtml html={pageContent.entries[0].content} />
          <StyledHtml html={pageContent.entries[1].content} />
          <StyledHtml html={pageContent.entries[2].content} />

          <MetaTable prefix="gas" name="Gases" items={meta.gases} />

          <MetaTable prefix="energy_source" name="Energy sources" items={meta.energySources} />

          <MetaTable prefix="sector" name="Sectors" items={meta.sectors} />

          <MetaTable
            prefix="emissions"
            name="Other emissions related colors"
            items={emissionsItems}
          />
          <div className={gridCn.col7}>
            <StyledHtml html={pageContent.entries[3].content} />
          </div>
          <div className={gridCn.col5}>
            <MetaTable name="Infoboxes" prefix="info" createExample={false} items={infoItems} />
          </div>
        </section>
      </div>
    </div>
  );
}

ContentEntry.SubLayout = layout({ pages });

export async function getStaticProps() {
  return getData({
    page: 'content-entry',
    endpoints: [{ id: 'pageContent' }, { id: 'siteContent', sectionIds: ['info'] }]
  });
}
