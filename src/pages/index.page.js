import Link from 'next/link';
import cx from 'classnames';
import cn from './style.module.scss';
import Html, { StyledHtml } from 'src/components/content/Html';
import gridCn from 'src/style/grid.module.scss';
import { getData } from 'src/data';
import buttonCn from 'src/style/button.module.scss';
import LandingHero from 'src/components/PageLayout/LandingHero';

import { useTheme } from 'src/style/ThemeProvider';

const dataSpec = {
  page: 'landing'
};

export default function Home({ siteContent, meta }) {
  const theme = useTheme();
  return (
    <>
      <main>
        <LandingHero siteContent={siteContent} countryGroups={meta.countryGroups} />
        <section
          className={cx(
            gridCn.wrapper,
            cn.landingIntro,
            'margin-bottom-xxxl',
            's-margin-bottom-xxl'
          )}>
          <div className={cx(gridCn.grid, gridCn.centerVertical)}>
            <div className={cx(gridCn.col5, gridCn.mCol9)}>
              <h1 className={cx('margin-bottom-s', cn.toolTitle)}>{siteContent.tool_name}</h1>
              <Html html={siteContent.intro} className={cn.introText} />
            </div>
            <div className={cx(gridCn.offset7, gridCn.col5, gridCn.mCol3)}>
              <a className={cx(buttonCn.outline, buttonCn.l, buttonCn.iconAfter)}>
                {siteContent.read_more_cta}
                <span className="icon-arrow-right" />
              </a>
            </div>
          </div>
        </section>
        <section className={cx(gridCn.wrapper, 'margin-bottom-xxxl', 's-margin-bottom-xxl')}>
          <div className={gridCn.grid}>
            {siteContent.about_section.map((item, i) => {
              return (
                <div key={i} className={cx(gridCn.col4, 'margin-bottom-s')}>
                  <StyledHtml html={item.content} />
                </div>
              );
            })}
          </div>
          <div className={cx(gridCn.container, gridCn.centerHorizontal)}>
            <div className="margin-bottom-m">
              <Link href="/methodology">
                <a className={cx(buttonCn.outline, buttonCn.l, buttonCn.iconBefore)}>
                  <span className="icon-file-text" />
                  {siteContent.methodology_cta}
                </a>
              </Link>
            </div>
          </div>
        </section>
        <section id="all-countries" className={cx(gridCn.wrapper)}>
          <div className={cx(cn.landingCountries, gridCn.grid)}>
            <div className={gridCn.col12}>
              <h2>{siteContent.all_countries_title}</h2>
            </div>
            <ul className={cx(gridCn.col12, cn.countryGroups)}>
              {meta.countryGroups.map((group) => {
                return (
                  <li key={group.id} className={cn.countryGroup}>
                    <p className={cx(cn.groupName)}>{group.name}</p>
                    <ul className={cn.countries}>
                      {group.children.map((country) => (
                        <li key={country.id} className={cn.country}>
                          {!country.disabled ? (
                            <Link href={`/countries/${country.slug}`}>
                              <a className={cx(cn.countryName)}>{country.name}</a>
                            </Link>
                          ) : (
                            <span className={cx(cn.countryName, cn.disabled, buttonCn.disabled)}>
                              {country.name}
                            </span>
                          )}
                        </li>
                      ))}
                    </ul>
                  </li>
                );
              })}
            </ul>
          </div>
        </section>
        <section
          className={cx(
            gridCn.wrapper,
            'margin-bottom-xxl',
            's-margin-top-m',
            's-margin-bottom-m'
          )}>
          <div className={cx(cn.landingContact, gridCn.grid)}>
            <div className={gridCn.col12}>
              <div className={cx(gridCn.grid, gridCn.centerVertical)}>
                <div className={cx(gridCn.col6, 'margin-bottom-m')}>
                  <h2>{siteContent.contact_title}</h2>
                  <Html html={siteContent.contact} />
                </div>
                <div className={cx(gridCn.col5, gridCn.colCenterHorizontal)}>
                  <Link href="/about#newsletter">
                    <a className={cx(buttonCn.outline, buttonCn.l, buttonCn.iconAfter)}>
                      <span className={buttonCn.buttonText}>{siteContent.contact_cta}</span>
                      <span className="icon-arrow-right" />
                    </a>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </section>
      </main>
    </>
  );
}

export async function getStaticProps() {
  return getData(dataSpec);
}
