import { getData } from 'src/data';
import gridCn from 'src/style/grid.module.scss';
import typoCn from 'src/style/typography.module.scss';
import pageCn from 'src/style/page.module.scss';
import cn from './style.module.scss';
import cx from 'classnames';
import { StyledHtml } from 'src/components/content/Html';

export default function Four0Four({ siteContent }) {
  return (
    <div className={cx(cn.errorPage, gridCn.container, pageCn.content)}>
      <div>
        <span className={typoCn.metaBase}>404</span>
      </div>
      <h1 className={cn.errorTitle}>{siteContent.error_title}</h1>
      <StyledHtml html={siteContent.error_text} />
    </div>
  );
}

export async function getStaticProps() {
  return getData({ page: '404' });
}
