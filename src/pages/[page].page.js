import { getData } from 'src/data';
import { useRef } from 'react';
import { StyledHtml } from 'src/components/content/Html';
import gridCn from 'src/style/grid.module.scss';
import cn from './style.module.scss';
import cx from 'classnames';
import pageCn from 'src/style/page.module.scss';
import useContentHierarchy from 'src/utils/useContentHierarchy';
import ContentNav from 'src/components/ContentNav';
import layout from 'src/components/PageLayout';
import References from 'src/components/References';
import Contact from 'src/components/Contact';
import CookieOptOut from 'src/components/utils/CookieConsent/OptOut';

const pages = [
  {
    slug: 'about',
    Appendix: Contact
  },
  {
    slug: 'methodology',
    Appendix: References
  },
  {
    slug: 'privacy-statement',
    Appendix: CookieOptOut
  }
];

const Page = ({ siteContent, pageContent, meta, slug }) => {
  const contentRef = useRef(null);
  const toc = useContentHierarchy(contentRef);
  const page = pages.find((p) => p.slug === slug);

  return (
    <div className={cx(pageCn.page, gridCn.grid)}>
      <aside className={cx(gridCn.col4)}>
        <ContentNav
          openCta={siteContent.content_label}
          closeCta={siteContent.close_label}
          label={siteContent.content_navigation_label}
          items={toc}
          contentRef={contentRef}
        />
      </aside>
      <div
        className={cx(
          gridCn.col7,
          gridCn.mCol10,
          gridCn.mOffset1,
          gridCn.sCol12,
          cn.staticContentWrapper
        )}
        ref={contentRef}>
        <div className={pageCn.content}>
          <h1>{pageContent.name}</h1>
        </div>
        {pageContent.entries.map((entry, i) => {
          return <StyledHtml key={i} html={entry.content} />;
        })}
        {page.Appendix && <page.Appendix siteContent={siteContent} meta={meta} />}
      </div>
    </div>
  );
};

Page.SubLayout = layout();

export async function getStaticPaths() {
  return {
    paths: pages.map((page) => ({ params: { page: page.slug } })),
    fallback: false
  };
}

export async function getStaticProps({ params }) {
  return getData({
    page: params.page,
    slug: params.page,
    endpoints: [{ id: 'pageContent' }]
  });
}

export default Page;
