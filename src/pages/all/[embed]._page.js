import { getData } from 'src/data';
import { useRouter } from 'next/router';
import gridCn from 'src/style/grid.module.scss';
import { parseQueryObject } from 'src/utils';
import embeds from 'src/embedables';
import cx from 'classnames';
import cn from '../style.module.scss';

function Embed({ allCountries, siteContent, isStatic, meta, embed, ...props }) {
  const router = useRouter();
  const parsedQuery = parseQueryObject(router.query);

  const { Component, id } = embeds[embed];

  // // isEmbed -> render embed layout
  // // isStatic -> render embed layout ->  don't render any controls

  const component =
    router.isReady &&
    allCountries.map((country) => (
      <Component
        key={country.id}
        siteContent={siteContent}
        {...country.data[id]} // data
        {...parsedQuery} // options
        id={id} // id for creating embed url
        country={country}
        isStatic={isStatic}
        meta={meta}
      />
    ));

  return component;

  // return (
  //   <>
  //     {isBare ? (
  //       component
  //     ) : (
  //       <div
  //         className={cx(
  //           gridCn.grid,
  //           cn.embedWrapper,
  //           isEmbed && cn.isEmbed,
  //           isStatic && cn.isStatic,
  //           isStatic && 'static'
  //         )}>
  //         <div className={gridCn.col12}>{component}</div>
  //       </div>
  //     )}
  //   </>
  // );
}

export default Embed;

export async function getStaticPaths() {
  const paths = Object.values(embeds).map((embed) => ({
    params: { embed: embed.id }
  }));

  return {
    paths: paths,
    fallback: false
  };
}

export async function getStaticProps({ params }) {
  const embed = embeds[params.embed];
  const data = await getData({
    embed: params.embed,
    endpoints: [{ id: 'allCountries', dataIds: [embed.dataId] }]
  });
  return data;
}
