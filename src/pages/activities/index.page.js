import { getData } from 'src/data';
import { useRef } from 'react';
import { StyledHtml } from 'src/components/content/Html';
import gridCn from 'src/style/grid.module.scss';
import cn from './style.module.scss';
import cx from 'classnames';
import layout from 'src/components/PageLayout';
import { timeFormat, timeParse } from 'd3-time-format';

const Page = ({ siteContent, activitiesContent }) => {
  const formatDate = timeFormat('%B %Y');
  const parseDate = timeParse('%Y-%m-%d');

  return (
    <>
      <div className={cx(gridCn.wrapper, cn.hero)}>
        <div className={cx(gridCn.grid)}>
          <div className={gridCn.col6}>
            <h2 className={cn.heroTitle}>{siteContent.activities_label}</h2>
            <StyledHtml modifiers={[cn.heroText]} html={siteContent.intro} />
          </div>
        </div>
      </div>
      <div className={cx(gridCn.grid)}>
        {activitiesContent.map((item, i) => {
          const date = formatDate(parseDate(item.date));
          return (
            <div key={i} className={cx(gridCn.col4, cn.activityItem)}>
              <img
                alt=""
                className={cn.activityImage}
                src={`${process.env.mediaUrl}w600/${item.image}`}
              />
              <div className={cn.activityMeta}>
                <span className={cn.activityType}>{item.type}</span>
                <span className={cn.activityDate}>{date}</span>
              </div>
              <h3 className={cn.activityTitle}>
                <a href={item.link} rel="noreferrer" target="_blank">
                  {item.title}
                </a>
              </h3>
              <StyledHtml modifiers={[cn.activityText]} html={item.abstract} />
            </div>
          );
        })}
      </div>
    </>
  );
};

Page.SubLayout = layout();

export async function getStaticProps() {
  return getData({
    page: 'activities',
    slug: 'activities',
    endpoints: [{ id: 'activitiesContent' }]
  });
}

export default Page;
